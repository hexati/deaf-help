package com.help.deaf.hexati.deafhelp.ui.address;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.ui.ChainedFormActivity;
import com.help.deaf.hexati.deafhelp.ui.MainActivity;
import com.help.deaf.hexati.deafhelp.ui.views.CircularImageView;
import com.help.deaf.hexati.deafhelp.utils.AutoAddTextWatcher;

import java.util.List;


public class AddressesActivity extends ChainedFormActivity implements MaterialDialog.SingleButtonCallback, AddressAdapter.OnItemClickListener, View.OnClickListener {
    private RecyclerView addressRecyclerView;
    private AddressAdapter addressAdapter;
    private List<Address> addressList;
    private AddressDBHelper addressDBHelper;
    public final static String OPEN_FOR_EDITION = "open_address_activity_for_edition";

    private EditText etName;
    private EditText etStreet;
    private EditText etNumber;
    private EditText etPostCode;
    private EditText etCity;
    private RadioGroup radioGroupAddressType;
    private MaterialDialog newAddressDialog;
    private MaterialDialog editAddressDialog;
    private Address currentEditedAddress;
    private int currentEditedAddressPosition;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addresses);
        init();
    }

    private void init() {
        loadDatabase();
        initButtons();
        initRecyclerView();
    }

    private void loadDatabase() {
        addressDBHelper = new AddressDBHelper(getApplicationContext());
        addressList = addressDBHelper.loadAllAddresses();
    }

    private void initButtons() {
        CircularImageView addAddressButton = (CircularImageView) findViewById(R.id.bt_add_address);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.floatingNext);

        addAddressButton.setOnClickListener(this);
        fab.setOnClickListener(this);
    }

    private void initRecyclerView() {
        addressAdapter = new AddressAdapter(addressList, this);
        addressRecyclerView = (RecyclerView) findViewById(R.id.addressRecycler);
        addressRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        addressRecyclerView.setItemAnimator(new DefaultItemAnimator());
        addressRecyclerView.setAdapter(addressAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        addressDBHelper.close();
    }

    private View getEditAddressView(Address address) {
        @SuppressLint("InflateParams")
        View customView = getLayoutInflater().inflate(R.layout.address_dialog_custom_view, null);
        etName = (EditText) customView.findViewById(R.id.etAddressName);
        etStreet = (EditText) customView.findViewById(R.id.etAddressStreet);
        etNumber = (EditText) customView.findViewById(R.id.etAddressNumber);
        etPostCode = (EditText) customView.findViewById(R.id.etAddressPostcode);
        etPostCode.addTextChangedListener(new AutoAddTextWatcher(etPostCode, "-", 2));
        etCity = (EditText) customView.findViewById(R.id.etAddressCity);
        radioGroupAddressType = (RadioGroup) customView.findViewById(R.id.addressTypeRadioGroup);
        if (address != null) {
            fillAddressFields(address);
        }
        return customView;
    }

    private void fillAddressFields(Address address) {
        switch (address.getIcon()) {
            case Home:
                radioGroupAddressType.check(R.id.radioHome);
                break;
            case Work:
                radioGroupAddressType.check(R.id.radioWork);
                break;
            case Other:
                radioGroupAddressType.check(R.id.radioOther);
                break;
        }
        etName.setText(address.getName());
        etStreet.setText(address.getStreet());
        etNumber.setText(address.getNumber());
        etPostCode.setText(address.getPostCode());
        etCity.setText(address.getCity());
    }


    private void showEditAddressDialog(int position) {
        currentEditedAddress = addressList.get(position);
        currentEditedAddressPosition = position;
        editAddressDialog = new MaterialDialog.Builder(this)
                .title(R.string.address_dialog_title_edit)
                .customView(getEditAddressView(addressList.get(position)), false)
                .positiveText(R.string.address_dialog_button_save_changes)
                .onPositive(this)
                .negativeText(R.string.address_dialog_button_cancel)
                .onNegative(this)
                .neutralText(R.string.address_dialog_button_delete)
                .onNeutral(this)
                .autoDismiss(false)
                .show();
    }

    private void updateAddress(Address address, int position) {
        if (position < addressList.size()) {
            addressDBHelper.updateAddress(address);
            addressAdapter.notifyItemChanged(position);
        }
    }

    private void deleteAddress(Address address, int position) {
        if (position < addressList.size()) {
            addressDBHelper.deleteAddress(address);
            addressList.remove(position);
            addressAdapter.notifyItemRemoved(position);
            addressAdapter.notifyDataSetChanged();
        }
    }

    private void showAddAddressDialog() {
        newAddressDialog = new MaterialDialog.Builder(this)
                .title(R.string.addres_add)
                .customView(getEditAddressView(null), false)
                .positiveText(R.string.address_dialog_button_add)
                .onPositive(this)
                .onNegative(this)
                .negativeText(R.string.address_dialog_button_cancel)
                .autoDismiss(false)
                .show();
    }

    private boolean checkDataCorrectness(String addressName, String addressStreet, String addressNumber, String addressPostCode, String addressCity) {
        boolean result = true;
        if (addressName.isEmpty()) {
            result = false;
            etName.setError(getString(R.string.empty_field_error));
        }
        if (addressStreet.isEmpty()) {
            result = false;
            etStreet.setError(getString(R.string.empty_field_error));
        }
        if(addressNumber.isEmpty()){
            result =false;
            etNumber.setError(getString(R.string.empty_field_error));
        }

        if (addressPostCode.isEmpty()) {
            result = false;
            etPostCode.setError(getString(R.string.empty_field_error));
        }

        if (!Utils.isZipCodeValid(addressPostCode)) {
            result = false;
            etPostCode.setError(getString(R.string.inncorect_postal_code));
        }

        if (addressCity.isEmpty()) {
            result = false;
            etCity.setError(getString(R.string.empty_field_error));
        }

        return result;
    }


    private void saveNewAddress(Address newAddress) {
        addressDBHelper.insertAddress(newAddress);
        addressList.add(newAddress);
        addressAdapter.notifyItemInserted(addressList.size() - 1);
        addressRecyclerView.scrollToPosition(addressAdapter.getItemCount() - 1);
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

        if (which == DialogAction.POSITIVE) {
            String addressName = etName.getText().toString();
            String addressStreet = etStreet.getText().toString();
            String addressNumber = etNumber.getText().toString();
            String addressPostCode = etPostCode.getText().toString();
            String addressCity = etCity.getText().toString();

            if (checkDataCorrectness(addressName, addressStreet, addressNumber, addressPostCode, addressCity)) {
//
                Address.AddressIcon selectedIcon;
                switch (radioGroupAddressType.getCheckedRadioButtonId()) {
                    case R.id.radioHome:
                        selectedIcon = Address.AddressIcon.Home;
                        break;
                    case R.id.radioWork:
                        selectedIcon = Address.AddressIcon.Work;
                        break;
                    case R.id.radioOther:
                        selectedIcon = Address.AddressIcon.Other;
                        break;

                    default:
                        selectedIcon = Address.AddressIcon.Home;
                }

                if (dialog == newAddressDialog) {
                    saveNewAddress(new Address(addressName, addressStreet, addressNumber, addressCity, addressPostCode, selectedIcon));
                } else if (dialog == editAddressDialog) {
                    currentEditedAddress.setName(addressName);
                    currentEditedAddress.setStreet(addressStreet);
                    currentEditedAddress.setNumber(addressNumber);
                    currentEditedAddress.setPostCode(addressPostCode);
                    currentEditedAddress.setCity(addressCity);
                    currentEditedAddress.setIcon(selectedIcon);
                    updateAddress(currentEditedAddress, currentEditedAddressPosition);
                }
                dialog.dismiss();
            }
        } else if (which == DialogAction.NEGATIVE) {
            dialog.dismiss();
        } else if (which == DialogAction.NEUTRAL) {
            deleteAddress(currentEditedAddress, currentEditedAddressPosition);
            dialog.dismiss();
        }
    }

    @Override
    public void onItemClick(int position) {
        showEditAddressDialog(position);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.floatingNext:
                if (getIntent().getBooleanExtra(OPEN_FOR_EDITION, false)) {
                    finish();
                } else {
                    startActivity(new Intent(AddressesActivity.this, MainActivity.class));
                    finish();
                }
                break;
            case R.id.bt_add_address:
                showAddAddressDialog();
                break;
        }
    }
}
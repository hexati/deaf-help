package com.help.deaf.hexati.deafhelp.messages;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Receives all broadcasts about SMS messages. Because this is not default SMS app, we can only receive
 * broadcasts of sent/delivered messages that were sent through {@link SmsSender}.
 * <p>Use {@link com.help.deaf.hexati.deafhelp.ui.SmsActivity} to handle receivers lifecycle, or manually register them on activity onResume(), and unregister them in onPause().</p>
 * <p>Note that local broadcast receivers prevent static one from handling the intent, and
 * registering multiple receivers within same activity will cause all of them to receive callbacks.
 * </p>
 * <p>
 */
public class SmsReceiver extends BroadcastReceiver {
    private final static String TAG = SmsReceiver.class.getSimpleName();
    /**
     * Raise this flag to prevent sending SMS but still receive callbacks.
     * <p>Use for testing purposes only.</p>
     */
    public final static boolean DEBUG = false;//false;

    /**
     * Raise this flag when you want to consider all smses sent as delivered after {@link #SENT_TIMEOUT_MS} delay. Be sure user can't spam send those messages or some input might be ignored.
     */
    private final static boolean TIMEOUT_SENT_IS_DELIVERED = true;
    /**
     * Time for {@link #TIMEOUT_SENT_IS_DELIVERED}.
     */
    public final static int SENT_TIMEOUT_MS = 2000;
    /**
     * Because there's no sendOrderedBroadcast() for local broadcast manager, we have to store result in an extra instead.
     */
    private static final String EXTRA_RESULT = "EXTRA_RESULT";

    /**
     * Sms listener attached to this instance. If this was created by system (defined in manifest) then this is <code>null</code>
     */
    private final SmsListener smsListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        //if there are any local receivers registered, let them consume the intent instead
        if (smsListener == null) {
            intent.putExtra(EXTRA_RESULT, getResultCode());
            if (LocalBroadcastManager.getInstance(context).sendBroadcast(intent)) { //static receiver, pass broadcast to local
                return;
            }
        }

        if (intent.getAction().equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            for (SmsMessage message : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                handleReceiveSms(context, message);
            }
        } else if (intent.getAction().equals(SmsSender.INTENT_ACTION_SMS_SENT)) {
            handleSentSms(intent);
        } else if (intent.getAction().equals(SmsSender.INTENT_ACTION_SMS_DELIVERED)) {
            handleDeliveredSms(intent);
        } else {

            Log.e(TAG, "onReceive: Cannot handle Intent with action " + intent.getAction());
        }
    }

    /**
     * Perform action on each received Sms message
     * <ul>
     * <li>If app is running and local broadcast receiver exist, forward callback to the listener</li>
     * <li>If app is in background, build and show notification</li>
     * </ul>
     *
     * @param context    context provided to this broadcastReceiver
     * @param smsMessage message from intent
     */
    private void handleReceiveSms(Context context, SmsMessage smsMessage) {
        String sender = smsMessage.getDisplayOriginatingAddress();
        String messageBody = smsMessage.getMessageBody();

        if (sender.equals(context.getResources().getString(SmsSender.NUMBER_REPORT))) {
            //Pass on the text to our listener if send from emergency number.
            if (smsListener != null) {
                smsListener.onMessageReceived(messageBody);
            } else {
                HelpNotificationManager.showNotification(context, smsMessage);
            }
        }

        if (sender.equals(context.getResources().getString(SmsSender.NUMBER_SYSTEM))) {
            //Pass on the text to our listener if send from emergency number.
            if (smsListener != null) {
                smsListener.onMessageReceived(messageBody);
            }//no notification for messages from system number
        }
    }

    /**
     * Action when intent is identified as sms sent.
     *
     * @param intent pending intent sent from {@link SmsSender}
     */
    private void handleSentSms(Intent intent) {
        if (smsListener != null) {
            int resultCode = intent.getIntExtra(EXTRA_RESULT, 0);
            int type = intent.getIntExtra(SmsSender.EXTRA_TYPE, SmsSender.TYPE_NORMAL);
            String message = intent.getStringExtra(SmsSender.EXTRA_MESSAGE);
            if (type == SmsSender.TYPE_NORMAL)
                smsListener.onMessageSent(resultCode, message);
            else
                smsListener.onSpecialMessageSent(type, resultCode, message);
        } else
            Log.i(TAG, "onReceive: unhandled sms sent : " + intent.getStringExtra(SmsSender.EXTRA_MESSAGE));
    }

    /**
     * Action when intent is identified as sms delivered
     *
     * @param intent pending intent sent from {@link SmsSender}
     */
    private void handleDeliveredSms(Intent intent) {
        byte[] smsPdu = (byte[]) intent.getExtras().get("pdu");
        Log.e(TAG, "handleDeliveredSms");
        SmsMessage deliveredSms;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            deliveredSms = SmsMessage.createFromPdu(smsPdu, intent.getStringExtra("format"));
        } else
            //noinspection deprecation
            deliveredSms = SmsMessage.createFromPdu(smsPdu);

        if (smsListener != null) {
            int type = intent.getIntExtra(SmsSender.EXTRA_TYPE, SmsSender.TYPE_NORMAL);
            if (type == SmsSender.TYPE_NORMAL)
                smsListener.onMessageDelivered(deliveredSms);
            else
                smsListener.onSpecialMessageDelivered(type, deliveredSms);
        } else
            Log.i(TAG, "onReceive: unhandled delivered sms : " + intent.getStringExtra(SmsSender.EXTRA_MESSAGE));
    }

    /**
     * Constructor created from static context (manifest). Do not call this manually.
     */
    public SmsReceiver() {
        smsListener = null;
    }

    /**
     * Internal constructor for {@link #createAndRegister(Context, SmsListener, IntentFilter)}.
     *
     * @param smsListener listener to attach
     */
    private SmsReceiver(SmsListener smsListener) {
        this.smsListener = smsListener;
    }

    /**
     * Create and register local broadcast receiver listening for incoming sms.
     *
     * @param context     context for receiver
     * @param smsListener UI callback
     * @return instance of SmsReceiver that will only receive sms received callbacks
     */
    public static SmsReceiver createAndRegisterReceiveInstance(Context context, @NonNull SmsListener smsListener) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Telephony.Sms.Intents.SMS_RECEIVED_ACTION);
        return createAndRegister(context, smsListener, intentFilter);
    }

    /**
     * Create and register local broadcast receiver listening for send/delivered sms.
     *
     * @param context     context for receiver
     * @param smsListener UI callback
     * @return instance of SmsReceiver that will only receive sms sent and delivered callbacks
     */
    public static SmsReceiver createAndRegisterSendingInstance(Context context, @NonNull SmsListener smsListener) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsSender.INTENT_ACTION_SMS_SENT);
        intentFilter.addAction(SmsSender.INTENT_ACTION_SMS_DELIVERED);
        return createAndRegister(context, smsListener, intentFilter);
    }

    /**
     * Create and register local broadcast receiver listening for receive/send/delivered sms.
     *
     * @param context     context for receiver
     * @param smsListener UI callback
     * @return instance of SmsReceiver that will receive all callbacks.
     */
    public static SmsReceiver createAndRegisterFullInstance(Context context, @NonNull SmsListener smsListener) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Telephony.Sms.Intents.SMS_RECEIVED_ACTION);
        intentFilter.addAction(SmsSender.INTENT_ACTION_SMS_SENT);
        intentFilter.addAction(SmsSender.INTENT_ACTION_SMS_DELIVERED);
        return createAndRegister(context, smsListener, intentFilter);
    }

    /**
     * Internal call for creating receiver with a given intentFilter.
     *
     * @param context     context for receiver
     * @param smsListener listener to attach to the receiver
     * @param filter      filter to use for the receiver
     * @return registered smsReceiver
     */
    private static SmsReceiver createAndRegister(Context context, @NonNull SmsListener smsListener, IntentFilter filter) {
        SmsReceiver localReceiver = new SmsReceiver(smsListener);
        LocalBroadcastManager.getInstance(context).registerReceiver(localReceiver, filter);
        return localReceiver;
    }

    /**
     * Interface for receiving messages from help number. Only some methods might be called depending on
     * broadcast receivers intent filter.
     */
    public interface SmsListener {
        /**
         * New message from help number was received
         *
         * @param messageText sms body
         */
        void onMessageReceived(String messageText);

        /**
         * User sent a new text message. If this results in an error, {@link
         * #onMessageDelivered(SmsMessage)} will not be called.
         *
         * @param resultCode  result code passed from intent. If success then this is {@link
         *                    android.app.Activity#RESULT_OK}. Otherwise you can use {@link
         *                    #getErrorMessage(int)} to parse it to a readable message.
         * @param messageText sms body
         */
        void onMessageSent(int resultCode, String messageText);

        /**
         * Received delivery report of message. If {@link #onMessageSent(int, String)} resulted in an error this will not be called.
         * This is also not guaranteed to be called if help number is offline (or depending on device settings?).
         *
         * @param message sms body
         */
        void onMessageDelivered(SmsMessage message);

        /**
         * User sent a new special sms request. If this results in an error, {@link
         * #onSpecialMessageDelivered(int, SmsMessage)} will not be called.
         *
         * @param type        request type, value is equal to one of the types in {@link SmsSender}
         * @param resultCode  result code passed from intent. If success then this is {@link
         *                    android.app.Activity#RESULT_OK}. Otherwise you can use {@link
         *                    #getErrorMessage(int)} to parse it to a readable message.
         * @param messageText request body
         */
        void onSpecialMessageSent(@SmsSender.SmsType int type, int resultCode, String messageText);

        /**
         * Sms request was delivered to the help. If {@link #onSpecialMessageSent(int, int, String)} returned an error this will not be called.
         * This is also not guaranteed to be called if help number is offline (or depending on device settings?).
         *
         * @param type    request type, value is equal to one of the types in {@link SmsSender}
         * @param message request body
         */
        void onSpecialMessageDelivered(@SmsSender.SmsType int type, SmsMessage message);

        /**
         * Stub for overriding selected interface methods only.
         */
        abstract class Adapter implements SmsListener {
            @Override
            public void onMessageReceived(String messageText) {

            }

            @Override
            public void onMessageSent(int resultCode, String messageText) {

            }

            @Override
            public void onMessageDelivered(SmsMessage message) {

            }

            @Override
            public void onSpecialMessageSent(@SmsSender.SmsType int type, int resultCode, String messageText) {

            }

            @Override
            public void onSpecialMessageDelivered(@SmsSender.SmsType int type, SmsMessage message) {

            }
        }

        /**
         * Handles only one type of special SMSes, implements {@link #DEBUG} and {@link #TIMEOUT_SENT_IS_DELIVERED} logic.
         * <lu>
         * <li>
         * <p>Calls {@link #onSpecialSent(String)} if sending works</li>
         * <li>Calls {@link #onSpecialSentError(String, String)} if sending fails</li>
         * <li>Calls {@link #onSpecialDelivered(SmsMessage)} when we get delivery report.</li>
         * </lu>
         */
        abstract class SpecialAdapter extends Adapter {
            @SmsSender.SmsType
            final int type;
            private boolean isAwaitingDelivery = false;
            private Handler mHandler = new Handler();

            @SuppressWarnings("ConstantConditions")
            private DeliveryRunnable mDeliveryRunnable = TIMEOUT_SENT_IS_DELIVERED ? new DeliveryRunnable() : null;

            /**
             * Special stub that filters out only one type of special sms sent/delivered, prevents receiving callbacks for sms types you don't want.
             *
             * @param type single request type handled by this listener, value should be equal to one of the types in {@link SmsSender}
             */
            protected SpecialAdapter(@SmsSender.SmsType int type) {
                this.type = type;
            }

            @Override
            final public void onMessageSent(int resultCode, String messageText) {
                //this shouldn't be called
            }

            @Override
            final public void onMessageDelivered(SmsMessage message) {
                //this shouldn't be called
            }

            @Override
            final public void onSpecialMessageSent(@SmsSender.SmsType int type, int resultCode, String messageText) {
                if (type == this.type) {
                    if (DEBUG) {
                        onSpecialSent(messageText);
                        isAwaitingDelivery = true;
                        onSpecialMessageDelivered(type, null);
                    }
                    if (resultCode != Activity.RESULT_OK)
                        onSpecialSentError(getErrorMessage(resultCode), messageText);
                    else {
                        onSpecialSent(messageText);
                        if (!DEBUG && !isAwaitingDelivery && TIMEOUT_SENT_IS_DELIVERED) {
                            mDeliveryRunnable.type = type;
                            mDeliveryRunnable.resultCode = resultCode;
                            mDeliveryRunnable.message = messageText;
                            mHandler.postDelayed(mDeliveryRunnable, SENT_TIMEOUT_MS);
                            isAwaitingDelivery = true;
                        }
                    }
                }
            }

            @Override
            final public void onSpecialMessageDelivered(@SmsSender.SmsType int type, SmsMessage message) {
                if (type == this.type && isAwaitingDelivery) {
                    isAwaitingDelivery = false;
                    mHandler.removeCallbacks(mDeliveryRunnable);
                    onSpecialDelivered(message);
                }
            }

            /**
             * Called after sms was successfully sent. {@link #onSpecialDelivered(SmsMessage)} should be called soon after this unless there's a problem with delivery report.
             *
             * @param message text that was sent
             */
            public void onSpecialSent(String message) {
            }

            /**
             * Called when sending sms failed
             *
             * @param errorMessage failure to print
             * @param messageText  text that we tried to send
             */
            public abstract void onSpecialSentError(String errorMessage, String messageText);

            /**
             * Special sms successfully delivered. This might not get called if help number is offline or device/operator settings prevent delivery reports.
             *
             * @param message delivered message <br><br>If {@link #DEBUG} or {@link #TIMEOUT_SENT_IS_DELIVERED} is true then <code>message</code> is always null
             */
            public abstract void onSpecialDelivered(SmsMessage message);

            /**
             * Runnable performing fake delivery report when real one fails.
             */
            private class DeliveryRunnable implements Runnable {
                int type;
                int resultCode;
                String message;

                @Override
                public void run() {
                    if (isAwaitingDelivery)
                        onSpecialMessageDelivered(type, null);
                }
            }
        }
    }

    /**
     * Parse result code into an error message.
     *
     * @param resultCode result code to parse
     * @return String describing the error
     */
    @NonNull
    public static String getErrorMessage(int resultCode) {
        switch (resultCode) {
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                return "Nieznany błąd";
            case SmsManager.RESULT_ERROR_NO_SERVICE:
                return "Brak zasięgu";
            case SmsManager.RESULT_ERROR_NULL_PDU:
                return "Brak PDU";
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                return "Włączony tryb samolotowy";
            default:
                return "Nieokreślony błąd";
        }
    }
}
package com.help.deaf.hexati.deafhelp.ui.history;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.help.deaf.hexati.deafhelp.R;

import java.util.ArrayList;

/**
 * Displays history entries.
 */
class HistoryAdapter extends RecyclerView.Adapter<HistoryEntry.HistoryViewHolder> {
    private ArrayList<HistoryEntry> items;
    /**
     * Host of this adapter.
     */
    private final HistoryFragment fragment;

    HistoryAdapter(HistoryFragment fragment){
        this.fragment = fragment;
        setHasStableIds(true);
    }

    /**
     * Change lsit of items shown in the adapter.
     * @param items new list
     */
    void setItems(ArrayList<HistoryEntry> items) {
        this.items = items;
    }

    @Override
    public HistoryEntry.HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new HistoryEntry.HistoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HistoryEntry.HistoryViewHolder holder, int position) {
        holder.bind(items.get(position), fragment);
    }

    @Override
    public void onViewRecycled(HistoryEntry.HistoryViewHolder holder) {
        holder.unbind();
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.recycler_history_entry;
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getTime();
    }

}

package com.help.deaf.hexati.deafhelp.ui.security;

import android.support.annotation.IdRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.utils.SharedPreferencesUtils;

public class HelpQuestionActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private final String TAG = HelpQuestionActivity.class.getSimpleName();
    private RadioGroup radioGroup;
    private EditText otherQuestionEt;
    private TextInputLayout textInputLayout;
    private EditText answerEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_question);
        answerEditText = (EditText) findViewById(R.id.activity_question_answer_editText);
        textInputLayout = (TextInputLayout) findViewById(R.id.activity_question_other_text_input);
        otherQuestionEt = (EditText) findViewById(R.id.activity_question_other_editText);
        radioGroup = (RadioGroup) findViewById(R.id.activity_question_radio_group);
        radioGroup.setOnCheckedChangeListener(this);
        FloatingActionButton saveButton = (FloatingActionButton) findViewById(R.id.activity_question_save_button);
        saveButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_question_save_button:
                saveToSharedPrefs();
                break;
        }
    }

    private void saveToSharedPrefs() {
        String answer = getAnswer();
        String question = getQuestion();
        if(checkDataCorrectness(answer,question)){
            SharedPreferencesUtils.saveUserHelpAnswer(this,answer);
            SharedPreferencesUtils.saveUserHelpQuestion(this, question);
            //if this activity was called from SetPatternActivity, return back the calling intent.
            setResult(RESULT_OK, getIntent());
            finish();
        }
    }


    private boolean checkDataCorrectness(String answer, String question){
        boolean result = true;
        if(answer.isEmpty()){
            result = false;
            answerEditText.setError(getString(R.string.field_answer_error));
        }

        if(question.isEmpty()){
            result = false;
            if(otherQuestionEt.getVisibility()==View.VISIBLE){
                otherQuestionEt.setError(getString(R.string.field_question_error));
            } else {
                Snackbar.make(answerEditText, R.string.no_question_selected, Snackbar.LENGTH_LONG);
            }
        }
        return result;
    }


    public String getQuestion(){
        int radioId = radioGroup.getCheckedRadioButtonId();
        if(radioId!=-1) {
            if (radioId == R.id.activity_question_other) {
                return otherQuestionEt.getText().toString();
            } else {
                return ((RadioButton) findViewById(radioId)).getText().toString();
            }
        } else {
            Snackbar.make(findViewById(R.id.linear_layout), "Nie wybrano żadnego pytania", Snackbar.LENGTH_LONG).show();
            return "";
        }
    }

    public String getAnswer(){
        return answerEditText.getText().toString();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.activity_question_other:
                showOtherQuestionEditText(true);
                break;
            default:
                showOtherQuestionEditText(false);
        }
    }

    private void showOtherQuestionEditText(boolean visible) {
        if (visible) {
            textInputLayout.setVisibility(View.VISIBLE);
        } else {
            textInputLayout.setVisibility(View.GONE);
        }

    }
}

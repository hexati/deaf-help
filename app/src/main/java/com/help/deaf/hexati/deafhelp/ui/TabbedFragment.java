package com.help.deaf.hexati.deafhelp.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Base class for all fragments within FragmentViewPager.<br>
 * <ul>
 * <li>Call {@link #onTabSelected()} and {@link #onTabDeselected()} when user changes pages to
 * manage lifecycle.</li>
 * <li>Pass {@link #onBackPressed()} to currently selected fragment to chain the back press.</li>
 * <li>Inflate your header layout with {@link #getHeader()} if you need it.</li>
 * <li>Implement {@link #onHeaderChangedListener} to know when to {@link #getHeader()}.</li>
 * </ul>
 */
public abstract class TabbedFragment<HEADER_TYPE> extends Fragment {
    private final static String BACKSTACK = "TabbedFragment.BackstackTAG";
    /**
     * Header to display in activity.
     *
     * @see #onCreateHeader(LayoutInflater, Bundle)
     * @see #getHeader()
     */
    private HEADER_TYPE header;

    /**
     * Currently overlaying fragment. Might be null.
     */
    private TabbedFragment<HEADER_TYPE> mCurrentOverlay;

    /**
     * How many fragments are below this one.
     */
    private int overlayLevel = 0;

    /**
     * Listener for header changed event. By default we try to see if this fragments activity is
     * implementing this.
     */
    private OnHeaderChangedListener onHeaderChangedListener;

    /**
     * Override to consume activity's back button press (called only if this fragment is the focused
     * tab).<br>
     * Super implementation removes overlaying fragments and notifies activity header change.
     *
     * @return true if it was consumed, false it not.
     */
    @CallSuper
    public boolean onBackPressed() {
        if (mCurrentOverlay != null) {
            if (mCurrentOverlay.onBackPressed())
                return true;
            if (removeOverlayFragment())
                notifyHeaderChanged();
            return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //reinitialize current overlay
        if (getOverlayContainer() == 0)
            return;
        mCurrentOverlay = (TabbedFragment<HEADER_TYPE>) getChildFragmentManager().findFragmentById(getOverlayContainer());

        //increment overlay level for the children
        if (mCurrentOverlay != null)
            mCurrentOverlay.overlayLevel += overlayLevel;
    }

    /**
     * This method is reserved for super class. Override following methods instead:<br>
     * <p>{@link #onCreateContentView(LayoutInflater, ViewGroup, Bundle)} for tabs body</p>
     * <p>{@link #onCreateHeader(LayoutInflater, Bundle)} for tabs header.</p>
     */
    @Nullable
    @Override
    public final View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        header = onCreateHeader(inflater, savedInstanceState);
        notifyHeaderChanged();
        return onCreateContentView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        header = null;
    }

    /**
     * Create header that will be used whenever this fragment is the focus of viewpager.
     *
     * @param inflater           LayoutInflater to use for inflating views
     * @param savedInstanceState saved instance if any
     * @return header, or null if fragment doesn't have one.
     * @see #getHeader()
     */
    @Nullable
    protected HEADER_TYPE onCreateHeader(LayoutInflater inflater, @Nullable Bundle savedInstanceState) {
        return null;
    }

    /**
     * Create content view to display for this fragment. <br>
     * <p>
     * Use this method instead of {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     *
     * @param inflater           LayoutInflater to use for inflating views
     * @param container          container that will be used as parent for this view
     * @param savedInstanceState saved instance if any
     * @return fragments content view
     */
    protected abstract View onCreateContentView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState);

    /**
     * Called when user changes the tab and this fragment becomes current tab.<br>
     * Super forwards the call to overlay if any.
     */
    public void onTabSelected() {
        if (mCurrentOverlay != null)
            mCurrentOverlay.onTabSelected();
    }

    /**
     * Called when user changes the tab and this fragment is no longer current tab.<br>
     * Super forwards the call to overlay if any.
     */
    public void onTabDeselected() {
        if (mCurrentOverlay != null) {
            mCurrentOverlay.onTabDeselected();
            if (!isPersistingOverlays())
                removeOverlayFragment();
        }
    }

    /**
     * Sets whether overlays should be saved or discarded as soon as {@link #onTabDeselected()} is
     * called.<br>
     * Overlay will still receive {@link #onTabDeselected()} call before being destroyed.
     *
     * @return return true to keep overlay or false to destroy them. By default true
     */
    protected boolean isPersistingOverlays() {
        return true;
    }

    /**
     * Override to provide Id of overlay container for {@link #addOverlayFragment(TabbedFragment)}.
     *
     * @return Id of ViewGroup that will host overlays
     */
    @IdRes
    protected int getOverlayContainer() {
        return 0;
    }

    /**
     * Get header this fragment created in {@link #onCreateHeader(LayoutInflater, Bundle)}.<br>
     * If overlay was set, this will return overlays header (even if null) instead.
     *
     * @return header or null if not created.
     */
    @Nullable
    public final HEADER_TYPE getHeader() {
        if (mCurrentOverlay != null) {
            return mCurrentOverlay.getHeader();
        }
        return header;
    }

    /**
     * Add a fragment that will overlay this fragment. If any current overlays exist they will be
     * replaced.
     *
     * @param overlay fragment to put as overlay for this fragment.
     * @see #getOverlayContainer()
     */
    protected final void addOverlayFragment(TabbedFragment<HEADER_TYPE> overlay) {
        if (getOverlayContainer() == 0)
            throw new IllegalStateException("Must override and provide valid container in getOverlayContainer()");
        getChildFragmentManager()
                .beginTransaction()
                .replace(getOverlayContainer(), overlay)
                .addToBackStack(BACKSTACK)
                .commitAllowingStateLoss();
        mCurrentOverlay = overlay;
    }

    /**
     * Remove currently overlaying fragment if possible.
     *
     * @return true if overlay was removed
     */
    protected final boolean removeOverlayFragment() {
        if (mCurrentOverlay == null)
            return false;
        getChildFragmentManager()
                .beginTransaction()
                .remove(mCurrentOverlay)
                .commitAllowingStateLoss();
        getChildFragmentManager().popBackStackImmediate(BACKSTACK, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        mCurrentOverlay = null;
        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnHeaderChangedListener)
            onHeaderChangedListener = (OnHeaderChangedListener) context;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnHeaderChangedListener)
            onHeaderChangedListener = (OnHeaderChangedListener) activity;
    }

    /**
     * Call this whenever listener must know about change of content in {@link #header}.
     */
    protected void notifyHeaderChanged() {
        if (onHeaderChangedListener != null)
            onHeaderChangedListener.onHeaderChanged();
    }

    /**
     * Change listener for header changes event. This is optional as activity implementing the
     * interface is already set as the listener.
     *
     * @param onHeaderChangedListener new listener
     */
    public void setOnHeaderChangedListener(OnHeaderChangedListener onHeaderChangedListener) {
        this.onHeaderChangedListener = onHeaderChangedListener;
    }

    /**
     * Describes how many fragments are below this one. If 0 then this is root of a tab, otherwise
     * it's an overlay.
     */
    protected int getOverlayLevel() {
        return overlayLevel;
    }

    /**
     * See if this fragment is currently overlaid.
     *
     * @return true if we have overlaying fragment, false if this fragment is on top.
     */
    public boolean hasOverlayingFragment() {
        return mCurrentOverlay != null;
    }

    /**
     * Callback for when header changes (due to state or overlay fragment leaving)
     */
    public interface OnHeaderChangedListener {
        void onHeaderChanged();
    }
}

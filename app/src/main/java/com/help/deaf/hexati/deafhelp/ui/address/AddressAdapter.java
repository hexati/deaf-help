package com.help.deaf.hexati.deafhelp.ui.address;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.ui.views.CircularImageView;


import java.util.List;



public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.AddressViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    private List<Address> addressList;
    private OnItemClickListener clickListener;

    public AddressAdapter(List<Address> addressesList, OnItemClickListener clickListener) {
        this.addressList = addressesList;
        this.clickListener = clickListener;
    }

    @Override
    public AddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);

        return new AddressViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AddressViewHolder holder, int position) {
        Address address = addressList.get(position);
        holder.name.setText(address.getName());
        holder.addressLine.setText(address.getAddressLine());
        holder.iconImage.setColor(address.getIconColor(holder.itemView.getContext()));
        holder.iconImage.setImageResource(address.getIconImageId());
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.address_row;
    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }

    public class AddressViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView name, addressLine;
        public CircularImageView iconImage;

        public AddressViewHolder(View view) {
            super(view);
            iconImage = (CircularImageView) view.findViewById(R.id.adressIcon);
            name = (TextView) view.findViewById(R.id.addressName);
            addressLine = (TextView) view.findViewById(R.id.addressLine);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition());
        }
    }
}

package com.help.deaf.hexati.deafhelp.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.model.LatLng;
import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.messages.HelpNotificationManager;
import com.help.deaf.hexati.deafhelp.messages.SmsReceiver;
import com.help.deaf.hexati.deafhelp.messages.SmsSender;
import com.help.deaf.hexati.deafhelp.ui.location.MyPositionTracker;
import com.help.deaf.hexati.deafhelp.ui.conversation.ConversationFragment;
import com.help.deaf.hexati.deafhelp.ui.report.ReportSummaryFragment;

import com.help.deaf.hexati.deafhelp.ui.report.ReportWarningActivity;
import com.help.deaf.hexati.deafhelp.utils.LocationServicesEnabler;
import com.help.deaf.hexati.deafhelp.utils.PermissionUtils;
import com.help.deaf.hexati.deafhelp.utils.SnackbarUtils;

public class MainActivity extends SmsActivity implements ViewPager.OnPageChangeListener, TabbedFragment.OnHeaderChangedListener {
    private final static long BACK_PRESS_QUIT_TIMEOUT_MS = 1500;
    private final static String TAG = MainActivity.class.getSimpleName();
    private final static String SAVE_LOCATION = "SAVE_PICKED_LOCATION";

    private MainActivityPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private AppBarLayout appBarLayout;
    private int mCurrentTab = 0;

    //conversation fragment
    private ConversationFragment conversationFragment;
    private LinearLayout layConversationInput;
    private TextView txtConversationInput;
    @SuppressWarnings("FieldCanBeLocal")
    private ImageButton btnConversationInputSend;

    //report summary fragment
    private ReportSummaryFragment reportSummaryFragment;
    //floating button for accepting report summary
    private FloatingActionButton fabDone;

    //shared recycled view pool between all fragments
    private RecyclerView.RecycledViewPool sharedRecycledViewPool;
    //timstamp of last back press
    private long mBackPressedTime = 0;

    //for location fragment
    public MyPositionTracker myPositionTracker;
    public LatLng savedAccidentPosition = null;
    private Toolbar toolbar;

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (SmsReceiver.DEBUG)
            setTitle(getString(R.string.app_name) + " (DEBUG)");

        toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                //What to do on back clicked
            }
        });

        sharedRecycledViewPool = new RecyclerView.RecycledViewPool();
        appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        fabDone = (FloatingActionButton) findViewById(R.id.fabActivityDone);
        fabDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFabDonePressed();
            }
        });
        bindConversationInput();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mSectionsPagerAdapter = new MainActivityPagerAdapter(this, mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(this);

        mSectionsPagerAdapter.getReportCategoryFragment().onTabSelected();

        if(savedInstanceState != null) {
            savedAccidentPosition = savedInstanceState.getParcelable(SAVE_LOCATION);
        }

        if(PermissionUtils.checkGpsPermission(this)) {
            checkLocationServicesIsOn();
        }
        myPositionTracker = new MyPositionTracker(this);
        myPositionTracker.startTracking(getApplicationContext());
        showSnackbar();

        if (savedInstanceState != null) {
            mCurrentTab = tabLayout.getSelectedTabPosition();
            mSectionsPagerAdapter.getTabAt(mCurrentTab).onTabSelected();

            if (conversationFragment != null)    //if conversationfragment wanted to initialize, recall it
                setOngoingConversationFragment(conversationFragment);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if(intent != null && intent.getAction()!=null && intent.getAction().equals(HelpNotificationManager.NOTIFICATION_ACTION))
            mViewPager.setCurrentItem(0);
    }

    private void showSnackbar() {
        if (getIntent().getAction() != null) {
            Snackbar snackbar = SnackbarUtils.getSnackbarForAction(getIntent().getAction(), appBarLayout);
            if (snackbar != null) {
                snackbar.show();
            }
        }
    }

    private void checkLocationServicesIsOn() {
        LocationServicesEnabler locationServicesEnabler = new LocationServicesEnabler();
        if (!LocationServicesEnabler.areLocationServicesEnabled(this)) {
            locationServicesEnabler.showEnableDialog(this);
        }
    }

    /**
     * Bind views for communicating with help center.
     */
    private void bindConversationInput() {
        layConversationInput = (LinearLayout) findViewById(R.id.layConversationInput);
        txtConversationInput = (TextView) findViewById(R.id.txtConversationInput);
        btnConversationInputSend = (ImageButton) findViewById(R.id.btnConversationSend);
        InputHandler inputHandler = new InputHandler();
        txtConversationInput.setOnEditorActionListener(inputHandler);
        btnConversationInputSend.setOnClickListener(inputHandler);
    }

    @Override
    protected SmsReceiver onCreateSmsReceiver() {
        return SmsReceiver.createAndRegisterFullInstance(this, new SmsReceiver.SmsListener.Adapter() {
            @Override
            public void onMessageSent(int resultCode, String messageText) {
                if (resultCode != RESULT_OK) {
                    new MaterialDialog.Builder(MainActivity.this)
                            .title(SmsReceiver.getErrorMessage(resultCode))
                            .content(messageText)
                            .negativeText(R.string.anuluj)
                            .positiveText(R.string.ok)
                            .show();
                }
            }
        });
    }

    public void promptForGoogleMapsInstalation() {
        new MaterialDialog.Builder(this)
                .title(R.string.error)
                .content(getString(R.string.map_not_installed_dialog))
                .positiveText(R.string.map_install_button)
                .negativeText(R.string.cancel_button_label)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.maps"));
                        startActivity(intent);
                    }
                })
                .show();
    }

    public MyPositionTracker getMyPositionTracker() {
        return this.myPositionTracker;
    }

    public void requestGPSPermission() {
        PermissionUtils.requestGPSPermission(this);
    }

    public void showGPSPermissionDialog() {
        new MaterialDialog.Builder(this)
                .content(R.string.gps_permission_request_message)
                .positiveText(R.string.ok_button_label)
                .negativeText(R.string.cancel_button_label)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        requestGPSPermission();
                    }
                })
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {
            case PermissionUtils.PERMISSIONS_REQUEST_LOCATION: {

                Context context = getApplicationContext();
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkLocationServicesIsOn();
                    myPositionTracker.locationPermissionRequestResult(true, context);
                } else {
                    myPositionTracker.locationPermissionRequestResult(false, context);
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        myPositionTracker.startTracking(getApplicationContext());
        HelpNotificationManager.clearNotifications(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        myPositionTracker.stopTracking();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onBackPressed() {
        //see if fragments on current tab want to consume the back press
        if (!mSectionsPagerAdapter.getTabAt(mCurrentTab).onBackPressed()) {
            //go back to ongoing conversation if possible
            if (conversationFragment != null) {
                mViewPager.setCurrentItem(0);
            } else {
                long time = System.currentTimeMillis();
                if (time - mBackPressedTime < BACK_PRESS_QUIT_TIMEOUT_MS)
                    super.onBackPressed();
                else {
                    mBackPressedTime = time;
                    Toast.makeText(this, R.string.tap_again_to_exit, Toast.LENGTH_SHORT).show();
                }
            }
        } else
            mBackPressedTime = 0;
    }

    /**
     * While scrolling away from the first tab:
     * <ul>If there's ongoing conversation, move conversation input as well to imitate it being in the tab</ul>
     * <ul>If we're in message summary fragment, hide floating action button</ul>
     */
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        boolean isConversationOngoing = conversationFragment != null && !conversationFragment.isReadOnly();

        if (position == 0 && isConversationOngoing) {
            layConversationInput.setVisibility(View.VISIBLE);
            layConversationInput.setTranslationX(-positionOffsetPixels);
        } else {
            layConversationInput.setVisibility(View.GONE);
        }
        if (position == 0 && positionOffset == 0 && reportSummaryFragment != null) {
            fabDone.show();
        } else
            fabDone.hide();
    }

    /**
     * When page is changed, trigger each tabbed fragments selected/deselected callback, then hide keyboard.
     */
    @Override
    public void onPageSelected(int position) {
        TabbedFragment deselectedFragment = mSectionsPagerAdapter.getTabAt(mCurrentTab);
        TabbedFragment selectedFragment = mSectionsPagerAdapter.getTabAt(position);
        if (deselectedFragment != null)
            deselectedFragment.onTabDeselected();
        if (selectedFragment != null)
            selectedFragment.onTabSelected();

        mCurrentTab = position;
        //always hide keyboard when changing tabs
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        onHeaderChanged();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * Set ongoing conversation fragment to pass input to.
     * <p>If this is called before activity finishes {@link #onCreate(Bundle)}, recall this at the end of it.</p>
     *
     * @param conversation ongoing conversation, or <code>null</code> if we disable input (also notifies history changed)
     */
    public void setOngoingConversationFragment(ConversationFragment conversation) {
        this.conversationFragment = conversation;
        if (layConversationInput == null)
            return;

        if (conversation != null && mCurrentTab == 0) {
            layConversationInput.setVisibility(View.VISIBLE);
        } else
            layConversationInput.setVisibility(View.GONE);
        if (conversation == null)
            mSectionsPagerAdapter.getHistoryFragment().notifyHistoryChanged();
    }

    /**
     * Set report summary fragment to show floating action button for
     *
     * @param reportSummary report summary, or <code>null</code> if we cancel the report
     */
    public void setReportSummaryFragment(ReportSummaryFragment reportSummary) {
        this.reportSummaryFragment = reportSummary;
        if (fabDone != null)
            if (reportSummary != null && mCurrentTab == 0)
                fabDone.show();
            else
                fabDone.hide();

    }

    /**
     * User accepted data in current {@link ReportSummaryFragment}, ready to move to {@link ReportWarningActivity}.
     */
    private void onFabDonePressed() {
        if (reportSummaryFragment != null) {
            reportSummaryFragment.onFabDoneClicked();
        }
    }


    /**
     * Callback when new conversation item is received.
     */
    public void onNewMessageReceived() {
        if (mViewPager.getCurrentItem() == 0) {
            appBarLayout.setExpanded(false, true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ReportWarningActivity.REQUEST_CODE && resultCode == RESULT_OK) {
            long reportId = data.getLongExtra(ReportWarningActivity.RESULT_EXTRA_REPORT_ID, -1);
            setReportSummaryFragment(null);
            ConversationFragment cf = ConversationFragment.newInstance(reportId, false);
            mSectionsPagerAdapter.getReportCategoryFragment().clearPickedCategory();
            mSectionsPagerAdapter.getReportCategoryFragment().addOverlayFragment(cf);
            mSectionsPagerAdapter.getHistoryFragment().notifyHistoryChanged();
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onHeaderChanged() {
        TabHeader h = mSectionsPagerAdapter.getTabAt(mCurrentTab).getHeader();
        if (h != null) {
            setTitle(h.headerString);
            getSupportActionBar().setDisplayHomeAsUpEnabled(h.isBackShown);
            if(!h.isBackShown){
                toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.mipmap.ic_launcher));
            }
        } else {
            setTitle(R.string.app_name);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);


        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if(savedAccidentPosition != null)
            outState.putParcelable(SAVE_LOCATION, savedAccidentPosition);
        super.onSaveInstanceState(outState);
    }

    /**
     * This handles user input and sends SMS out
     */
    private class InputHandler implements TextView.OnEditorActionListener, View.OnClickListener {
        @Override
        public void onClick(View view) {
            sendText();
        }

        @Override
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if (i == EditorInfo.IME_ACTION_SEND) {
                sendText();
                return true;
            }
            return false;
        }

        /**
         * Send sms from edittext to the help number
         */
        void sendText() {
            String textToSend = txtConversationInput.getText().toString();
            if (textToSend.length() < 1) {
                return;
            }
            SmsSender.sendSms(MainActivity.this, textToSend);
            txtConversationInput.setText(null);
        }
    }

    /**
     * Class returned as a header from each tab.
     */
    public static class TabHeader {
        private String headerString;
        private boolean isBackShown;

        public TabHeader(String headerString, boolean isBackShown) {
            this.headerString = headerString;
            this.isBackShown = isBackShown;
        }

        public TabHeader setHeaderString(String headerString) {
            this.headerString = headerString;
            return this;
        }

        public TabHeader setBackShown(boolean backShown) {
            isBackShown = backShown;
            return this;
        }
    }

    /**
     * Attach recycler to shared recycle pool so we can reuse views and viewholders between fragments.
     * <p>All adapters must override {@link RecyclerView.Adapter#getItemViewType(int)} and provide unique view types (across application) to prevent conflicts.</p>
     *
     * @param recyclerView recycler view to attach. It must exist within {@link MainActivity} context.
     */
    public static void attachRecyclerToSharedRecyclePool(RecyclerView recyclerView) {
        RecyclerView.RecycledViewPool vp = ((MainActivity) recyclerView.getContext()).sharedRecycledViewPool;
        recyclerView.setRecycledViewPool(vp);
    }
}

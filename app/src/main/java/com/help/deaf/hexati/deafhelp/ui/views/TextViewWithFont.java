package com.help.deaf.hexati.deafhelp.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.util.AttributeSet;

import com.help.deaf.hexati.deafhelp.R;


public class TextViewWithFont extends android.support.v7.widget.AppCompatTextView {

    private static final String TAG = "TextViewWithFont";
    /*
         * Caches typefaces based on their file path and name, so that they don't have to be created every time when they are referenced.
         */
    private static Typeface typeface;
    private final Paint mPaint = new Paint();

    private final Rect mBounds = new Rect();
    private boolean cropTopBottom;

    public TextViewWithFont(final Context context) {
        this(context, null);
    }

    public TextViewWithFont(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TextViewWithFont(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);

    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.RobotoTextView,
                0, 0);
        int font = a.getInt(R.styleable.RobotoTextView_font, 0);
        cropTopBottom = a.getBoolean(R.styleable.RobotoTextView_cropTopBottom, false);
        a.recycle();
        switch (font) {
            case 0: //Roboto Thin
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoThin.ttf");
                break;
            case 1:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoLight.ttf");
                break;
            case 2:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensedLight.ttf");
                break;
            case 3:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoRegular.ttf");
                break;
            case 4:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoMedium.ttf");
                break;
            case 5:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/hans.otf");
                break;
            case 6:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Befolk.ttf");
                break;
            case 7:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Thin.ttf");
                break;
            case 8:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
                break;
            case 9:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Medium.ttf");
                break;
            case 10:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Bold.ttf");
                break;
            case 11:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Black.ttf");
                break;
            case 12:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoBold.ttf");
                break;

        }
        if (typeface != null) {
            setTypeface(typeface);
        }


    }


    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        if (cropTopBottom) {
            final String text = calculateTextParams();
            final int left = mBounds.left;
            final int bottom = mBounds.bottom;
            mPaint.setTypeface(getTypeface());
            mBounds.offset(-mBounds.left, -mBounds.top);
            mPaint.setAntiAlias(true);
            mPaint.setColor(getCurrentTextColor());
            canvas.drawText(text, -left, mBounds.bottom - bottom, mPaint);
        } else super.onDraw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (cropTopBottom) {
            calculateTextParams();
            setMeasuredDimension(mBounds.width() + 1, -mBounds.top + mBounds.bottom);
        }
    }

    private String calculateTextParams() {
        final String text = getText().toString();
        final int textLength = text.length();
        mPaint.setTypeface(getTypeface());
        mPaint.setTextSize(getTextSize());
        mPaint.getTextBounds(text, 0, textLength, mBounds);
        if (textLength == 0) {
            mBounds.right = mBounds.left;
        }
        return text;
    }

}
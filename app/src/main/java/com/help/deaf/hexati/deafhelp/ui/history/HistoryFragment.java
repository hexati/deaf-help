package com.help.deaf.hexati.deafhelp.ui.history;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.util.ArraySet;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.ui.MainActivity;
import com.help.deaf.hexati.deafhelp.ui.TabbedFragment;
import com.help.deaf.hexati.deafhelp.ui.conversation.ConversationFragment;

import java.util.ArrayList;

/**
 * Fragment displaying list of reports sent.
 */
public class HistoryFragment extends TabbedFragment<MainActivity.TabHeader> {
    private RecyclerView recRecycler;
    private HistoryAdapter mAdapter;
    private TextView txtEmptyInfo;

    private ArrayList<HistoryEntry> historyEntries;
    private ArrayMap<String, XmlHistoryParser.HistoryXmlItemDetails> itemDetailsMap;

    @Nullable
    @Override
    protected MainActivity.TabHeader onCreateHeader(LayoutInflater inflater, @Nullable Bundle savedInstanceState) {
        return new MainActivity.TabHeader(getString(R.string.history_title), false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new HistoryAdapter(this);
        itemDetailsMap = new ArrayMap<>();
        notifyHistoryChanged();
    }

    @Override
    protected int getOverlayContainer() {
        return R.id.layHistoryOverlay;
    }

    @Override
    protected boolean isPersistingOverlays() {
        return false;
    }

    @Nullable
    @Override
    protected View onCreateContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_history, container, false);
        recRecycler = (RecyclerView) v.findViewById(R.id.recHistoryRecycler);
        recRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        recRecycler.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        recRecycler.setAdapter(mAdapter);
        MainActivity.attachRecyclerToSharedRecyclePool(recRecycler);
        txtEmptyInfo = (TextView) v.findViewById(R.id.txtHistoryEmpty);
        if (historyEntries.size() > 0)
            txtEmptyInfo.setVisibility(View.GONE);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        recRecycler = null;
        txtEmptyInfo = null;
    }

    /**
     * React to user selecting a history item (view all sms-es defined by this item)
     * @param position position of item in the {@link #historyEntries}
     */
    void onHistoryItemClicked(int position) {
        HistoryEntry clickedItem = historyEntries.get(position);
        ConversationFragment oldConvoFragment = ConversationFragment.newInstance(clickedItem.getTime(), true);
        addOverlayFragment(oldConvoFragment);
    }

    /**
     * Reload the list and notify adapter if needed.
     */
    public void notifyHistoryChanged() {
        //read history entries from database then parse XML to get definition of real names and icons
        historyEntries = new HistorySQL(getContext()).getAll();
        ArraySet<String> neededIds = new ArraySet<>();
        for (HistoryEntry he : historyEntries) {
            neededIds.add(he.type);
        }
        itemDetailsMap = new XmlHistoryParser(getContext(), neededIds).parse().getResult();

        mAdapter.setItems(historyEntries);
        if (recRecycler != null) {
            mAdapter.notifyDataSetChanged();
            if (historyEntries.size() > 0)
                txtEmptyInfo.setVisibility(View.GONE);
        }
    }

    /**
     * Read details (name and icon) for given report type. Underlying map is inflated during {@link #notifyHistoryChanged()}.
     * @param reportTypeId id to read details for
     * @return details for a type
     */
    XmlHistoryParser.HistoryXmlItemDetails getDetails(String reportTypeId) {
        return itemDetailsMap.get(reportTypeId);
    }
}

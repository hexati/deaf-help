package com.help.deaf.hexati.deafhelp.ui.security;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.utils.PatternLockUtils;
import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.utils.SharedPreferencesUtils;

import java.util.ArrayList;
import java.util.List;

public class SetPatternActivity extends PatternSecurityActivity {

    private static final String TAG = SetPatternActivity.class.getSimpleName();
    /**
     * Send pattern we selected to {@link HelpQuestionActivity} and get it back with result.
     */
    public final static String EXTRA_PATTERN = "SetPatternActivityExtraPattern";

    public static final int HELP_QUESTION_ACTIVITY_REQUEST_CODE = 1733;
    private List<String> patternList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fillUiWithContent();
    }

    private void fillUiWithContent() {
        getHelpText().setVisibility(View.VISIBLE);
        getMessageText().setText(getString(R.string.draw_pattern));
    }

    @Override
    public void onComplete(List<PatternLockView.Dot> pattern) {
        super.onComplete(pattern);
        String patternEntered = PatternLockUtils.patternToString(getPatternLockView(), pattern);
        if (patternLengthEligible(patternEntered)) {
            patternList.add(patternEntered);
            if (this.patternList.size() >= 2) {
                saveNewPattern();
            } else {
                updateMessage();
            }
        } else {
            patternList.clear();
            showPatternTooShortError();
        }
    }

    public boolean patternLengthEligible(String pattern) {
        return pattern.length() >= 3;
    }

    private void updateMessage() {
        getMessageText().setText(getString(R.string.draw_pattern_again));
    }

    private void showPatternTooShortError() {
        getMessageText().setText(getString(R.string.draw_pattern_error_short));
    }

    private void showPatternNotMatchError() {
        getMessageText().setText(getString(R.string.draw_pattern_error_notmatch));
    }

    private void saveNewPattern() {
        if (checkPatterns()) {
            if (SharedPreferencesUtils.getUserHelpQuestion(this).isEmpty()) {
                startHelpQuestionActivity();
            } else {
                SharedPreferencesUtils.saveUserPattern(SetPatternActivity.this, patternList.get(0));
                SharedPreferencesUtils.savePatternStatus(SetPatternActivity.this, true);
                setResult(RESULT_OK);
                finish();
            }
        } else {
            patternList.clear();
            showPatternNotMatchError();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == HELP_QUESTION_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            String pattern = data.getStringExtra(EXTRA_PATTERN);
            if (pattern == null) {
                Log.e(TAG, "onActivityResult: HelpQuestion destroyed our pattern?");
                setResult(RESULT_CANCELED);
            } else {
                SharedPreferencesUtils.saveUserPattern(SetPatternActivity.this, pattern);
                SharedPreferencesUtils.savePatternStatus(SetPatternActivity.this, true);
                setResult(RESULT_OK);
            }
            finish();

        } else if (requestCode == HELP_QUESTION_ACTIVITY_REQUEST_CODE && resultCode == RESULT_CANCELED) {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    private void startHelpQuestionActivity() {
        Intent questionIntent = new Intent(SetPatternActivity.this, HelpQuestionActivity.class);
        questionIntent.putExtra(EXTRA_PATTERN, patternList.get(0));
        startActivityForResult(questionIntent, HELP_QUESTION_ACTIVITY_REQUEST_CODE);
    }

    private boolean checkPatterns() {
        return patternList.get(0).equals(patternList.get(1));
    }

}

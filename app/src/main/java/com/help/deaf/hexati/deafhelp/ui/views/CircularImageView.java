package com.help.deaf.hexati.deafhelp.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;

import com.help.deaf.hexati.deafhelp.R;



public class CircularImageView extends android.support.v7.widget.AppCompatImageView {
    int color = Color.GRAY;
    Paint paint;


//    public CircularImageView(Context context) {
//        super(context);
//        init();
//    }

    public CircularImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);

    }

    public CircularImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);

    }

    public void setColor(int color) {
        this.color = color;
        paint.setColor(color);


    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircuralImageView);
        color = a.getColor(R.styleable.CircuralImageView_circleColor, Color.GRAY);

        paint = new Paint();
        paint.setColor(color);
        paint.setAntiAlias(true);
        a.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, getHeight() / 2, paint);
        super.onDraw(canvas);
    }
}
package com.help.deaf.hexati.deafhelp.messages;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntDef;
import android.support.annotation.StringRes;
import android.telephony.SmsManager;
import android.util.Log;

import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.ui.report.Report;
import com.help.deaf.hexati.deafhelp.utils.CommonOperations;
import com.help.deaf.hexati.deafhelp.utils.DeviceSystemUtils;
import com.help.deaf.hexati.deafhelp.utils.SharedPreferencesUtils;

import java.lang.annotation.Documented;
import java.util.ArrayList;

/**
 * Handles sending out Sms messages to the help service.
 * <br>
 * <p>Available SMS types:</p>
 * {@link #TYPE_NORMAL} <br>
 * {@link #TYPE_NEW_REPORT} <br>
 * {@link #TYPE_REGISTER} <br>
 * {@link #TYPE_UPDATE_INFO} <br>
 * {@link #TYPE_DELETE_ACCOUNT} <br>
 * {@link #TYPE_TERMINATE_REPORT} <br>
 * <p>
 */
public abstract class SmsSender {
    /**
     * Intent action used when sending SMS - its critical that this exact value is included in intent-filter for {@link SmsReceiver }in manifest.
     */
    static final String INTENT_ACTION_SMS_SENT = "SMS_SENT";
    /**
     * Intent action used when SMS delivery report comes - its critical that this exact value is included in intent-filter for {@link SmsReceiver }in manifest.
     */
    static final String INTENT_ACTION_SMS_DELIVERED = "SMS_DELIVERED";
    /**
     * Message that is sent - included as extra in pending intent.
     */
    static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    /**
     * Sms type we're sending - included as extra in pending intent.
     */
    static final String EXTRA_TYPE = "EXTRA_SMS_TYPE";

    /**
     * Sms sent directly to the help center (communication during report conversation).
     */
    public final static int TYPE_NORMAL = 0;
    public final static int TYPE_NEW_REPORT = 1;
    public final static int TYPE_REGISTER = 10;
    public final static int TYPE_UPDATE_INFO = 11;
    public final static int TYPE_DELETE_ACCOUNT = 12;
    public final static int TYPE_TERMINATE_REPORT = 20;

    /**
     * Number where emergency messages and communication is sent.
     */
    final static int NUMBER_REPORT = R.string.emergency_number;
    /**
     * Number where registration and other system messages are sent.
     */
    final static int NUMBER_SYSTEM = R.string.administrator_si_cpr_number;


    /**
     * String used before each tag in sms.
     *
     * @see Builder
     */
    private final static String DIVIDER = ", ";
    /**
     * String used between each tag and value in sms.
     *
     * @see Builder
     */
    private final static String TAG_DIVIDER = ": ";

    /**
     * Send generic sms to the help number. Use {@link SmsReceiver} to get any callbacks.
     *
     * @param context context to use
     * @param msg     message to send
     */
    public static void sendSms(Context context, String msg) {
        internalSendSms(context, msg, NUMBER_REPORT, TYPE_NORMAL);
    }

    /**
     * Send sms ot the help number.
     *
     * @param context     context for sending
     * @param msg         message to send
     * @param targetNumber {@link #NUMBER_REPORT} or {@link #NUMBER_SYSTEM}.
     * @param type        one of the types declared in {@link SmsSender}
     */
    private static void internalSendSms(Context context, String msg, @StringRes int targetNumber, @SmsType int type) {
        String number = context.getResources().getString(targetNumber);

        Log.d("smsender", "internalSendSms: " + type + " " + msg);
        Intent sentIntent = new Intent(INTENT_ACTION_SMS_SENT)
                .putExtra(EXTRA_MESSAGE, msg)
                .putExtra(EXTRA_TYPE, type);

        Intent deliveredIntent = new Intent(INTENT_ACTION_SMS_DELIVERED)
                .putExtra(EXTRA_MESSAGE, msg)
                .putExtra(EXTRA_TYPE, type);

        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, sentIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, deliveredIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        SmsManager sms = SmsManager.getDefault();

        if (SmsReceiver.DEBUG) {
            //note this will not inject sms delivered intent, receiver in debug mode uses sent intent as delivered one too.
            context.sendOrderedBroadcast(sentIntent, null, null, null, Activity.RESULT_OK, null, null);
        } else {
            ArrayList<String> parts = sms.divideMessage(msg);
            if (parts.size() > 1) {
                Log.d("SmsSender", "internalSendSms: sending multipart sms: " + parts.size());
            }
            ArrayList<PendingIntent> sentIntents = createEmptyArrayList(parts.size(), sentPI);
            ArrayList<PendingIntent> deliveryIntents = createEmptyArrayList(parts.size(), deliveredPI);

            sms.sendMultipartTextMessage(number, null, parts, sentIntents, deliveryIntents);
        }
    }

    public static void sendRegistrationMessage(Context context) {
        new Builder(TYPE_REGISTER, context)
                .addUserData()
                .buildAndSend();
    }

    public static void sendUpdateUserInfoMessage(Context context) {
        new Builder(TYPE_UPDATE_INFO, context)
                .addUserData()
                .buildAndSend();
    }

    public static void sendRemoveAccountMessage(Context context) {
        new Builder(TYPE_DELETE_ACCOUNT, context)
                .addPesel()
                .addPhone()
                .buildAndSend();
    }


    public static void sendNewReportMessage(Context context, Report report) {
        Builder smsBuilder = new Builder(TYPE_NEW_REPORT, context)
                .add(R.string.sms_tag_kategoria, CommonOperations.getReadableReportString(report.getCategory()))
                .add(R.string.sms_tag_adres, report.getLocation())
                .setSendToHelpService()
                .addPesel().addDeviceID().addPhone();
        smsBuilder.sb.append(getReportDetailsString(context, report));

        smsBuilder.buildAndSend();
    }

    public static void sendTerminateHelpConversationMessage(Context context) {
        new Builder(TYPE_TERMINATE_REPORT, context)
                .setSendToHelpService()
                .buildAndSend();
    }

    /**
     * Create empty arraylist with only first element set.
     *
     * @param size        size of list
     * @param lastElement first element of the list
     * @return list of nulls and 1 element
     */
    private static <T> ArrayList<T> createEmptyArrayList(int size, T lastElement) {
        ArrayList<T> arrayList = new ArrayList<>();
        arrayList.add(lastElement);
        for (int i = 0; i < size - 1; i++) {
            arrayList.add(null);
        }
        return arrayList;
    }

    /**
     * Build details string for report sms we're sending.
     *
     * @param context apps context to read resources from
     * @param report  report to build details string for
     * @return string of prefix + info from user, or null if user input nothing
     */
    private static String getReportDetailsString(Context context, Report report) {
        if (report.getDetails() != null) {
            String trimmed = report.getDetails().trim();
            if (trimmed.length() > 0)
                return DIVIDER + context.getString(R.string.sms_tag_info) + TAG_DIVIDER + trimmed;
        }
        return "";
    }

    @StringRes
    private static int getSmsTypeStringResource(@SmsType int type) {
        switch (type) {
            case TYPE_NEW_REPORT:
                return R.string.sms_type_nowe;
            case TYPE_REGISTER:
                return R.string.sms_type_rejestracja;
            case TYPE_UPDATE_INFO:
                return R.string.sms_type_zmien_dane;
            case TYPE_DELETE_ACCOUNT:
                return R.string.sms_type_usun_konto;
            case TYPE_TERMINATE_REPORT:
                return R.string.sms_type_zakoncz;
            default:
                return 0;
        }
    }

    /**
     * Builder for constructing SMS with common data and tags.
     */
    private static class Builder {
        private final int type;
        private final Context context;
        private final StringBuilder sb;
        private int targetNumber = NUMBER_SYSTEM;

        /**
         * New SMS builder, appends type of the report to the beginning of the message.
         *
         * @param type    type of message that will be sent
         * @param context app context to read resources from
         */
        private Builder(@SmsType int type, Context context) {
            this.type = type;
            this.context = context;
            sb = new StringBuilder();
            sb.append(context.getString(getSmsTypeStringResource(type)));
        }

        /**
         * Append given tag/value to this builder's string.
         *
         * @param tagResId id of string resource to use as a tag
         * @param value    value to pass with the tag
         * @return this builder to allow for chaining
         */
        private Builder add(@StringRes int tagResId, String value) {
            sb.append(DIVIDER)
                    .append(context.getString(tagResId))
                    .append(TAG_DIVIDER)
                    .append(value);
            return this;
        }

        /**
         * Add users pesel number to the message
         *
         * @return this builder to allow for chaining
         */
        private Builder addPesel() {
            return add(R.string.sms_tag_pesel, SharedPreferencesUtils.getUserPeselNumber(context));
        }

        /**
         * Add users phone number to the message
         *
         * @return this builder to allow for chaining
         */
        private Builder addPhone() {
            return add(R.string.sms_tag_telefon, SharedPreferencesUtils.getUserPhoneNumber(context));
        }


        /**
         * Add device ID to the message
         *
         * @return this builder to allow for chaining
         */
        private Builder addDeviceID() {
            return add(R.string.sms_tag_device_id, DeviceSystemUtils.getAndroidID(context));
        }

        /**
         * Add all user data/details stored in shared preferences.
         *
         * @return this builder to allow for chaining
         */
        private Builder addUserData() {
            return add(R.string.sms_tag_imie, SharedPreferencesUtils.getUserName(context))
                    .add(R.string.sms_tag_data_urodzenia, SharedPreferencesUtils.getUserBirthDate(context))
                    .add(R.string.sms_tag_email, SharedPreferencesUtils.getUserEmail(context))
                    .addPesel()
                    .addPhone()
                    .addDeviceID();
        }

        /**
         * Change target number from system handling to direct help contact.
         *
         * @return this builder to allow for chaining
         */
        private Builder setSendToHelpService(){
            targetNumber = NUMBER_REPORT;
            return this;
        }


        /**
         * Build and send the message.
         */
        private void buildAndSend() {
            internalSendSms(context, sb.toString(), targetNumber, type);
        }
    }

    /**
     * Available SMS types.
     */
    @Documented
    @IntDef({TYPE_NORMAL, TYPE_NEW_REPORT, TYPE_REGISTER, TYPE_UPDATE_INFO, TYPE_DELETE_ACCOUNT, TYPE_TERMINATE_REPORT})
    public @interface SmsType{}
}

package com.help.deaf.hexati.deafhelp.utils;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.ui.security.SetPatternActivity;



public class SnackbarUtils {
    public static final String ACTION_PATTERN_RESET = "pattern.reset";
    public static final String ACTION_PATTERN_CHANGED = "pattern.changed";
    public static final String ACTION_PATTERN_DISABLED = "pattern.disabled";
    public static final String ACTION_HELP_QUESTION_CHANGED = "pattern.help.changed";


    public static Snackbar getSnackbarForAction(String action, View view) {
        String message = getMessageForAction(view.getContext(),action);
        Snackbar snackBar = null;
        final Context context = view.getContext();
        if (action.equals(ACTION_PATTERN_RESET)) {
            snackBar =  Snackbar.make(view,message, BaseTransientBottomBar.LENGTH_LONG).setAction(context.getString(R.string.snackbar_pattern_set_new), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, SetPatternActivity.class));
                }
            });
        } else if (action.equals(ACTION_PATTERN_CHANGED) ||
                action.equals(ACTION_PATTERN_DISABLED) ||
                action.equals(ACTION_HELP_QUESTION_CHANGED)) {
             snackBar=  Snackbar.make(view, message, BaseTransientBottomBar.LENGTH_LONG);
        }
    return snackBar;
    }

    private static String getMessageForAction(Context context, String action) {
        String message = "";
        switch (action) {
            case ACTION_PATTERN_RESET:
                message = context.getResources().getString(R.string.snackbar_pattern_reset);
                break;
            case ACTION_PATTERN_CHANGED:
                message = context.getResources().getString(R.string.snackbar_pattern_changed);
                break;
            case ACTION_PATTERN_DISABLED:
                message = context.getResources().getString(R.string.snackbar_pattern_disabled);
                break;
            case ACTION_HELP_QUESTION_CHANGED:
                message = context.getResources().getString(R.string.snackbar_pattern_help_question_changed);
                break;
        }

        return message;
    }

}

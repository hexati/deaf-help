package com.help.deaf.hexati.deafhelp.messages;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsMessage;

import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.ui.MainActivity;
import com.help.deaf.hexati.deafhelp.utils.SharedPreferencesUtils;

/**
 * Manages this applications notifications.
 */
public abstract class HelpNotificationManager {
    public final static String NOTIFICATION_ACTION = "com.help.deaf.hexati.deafhelp.NotificationAction";
    private final static int NOTIFICATION_ID = 0;

    /**
     * Build and show notification (user must have registered).
     *
     * @param context    applications context
     * @param smsMessage message to show in notification
     */
    static void showNotification(Context context, SmsMessage smsMessage) {
        if (!SharedPreferencesUtils.getRegistrationComplete(context)) {
            return;
        }
        final NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent openApp = new Intent(context, MainActivity.class).setAction(NOTIFICATION_ACTION);
        PendingIntent openAppPending = PendingIntent.getActivity(context, 0, openApp, 0);

        final Notification.Builder nb = new Notification.Builder(context)
                .setSmallIcon(R.drawable.notiicon)
                //.setLargeIcon(getLargeIcon(context))
                .setContentTitle(context.getString(R.string.app_name))
                .setContentIntent(openAppPending)
                .setVibrate(new long[]{1000, 1000, 1000, 1000})
                .setPriority(Notification.PRIORITY_MAX);

        if (smsMessage == null || smsMessage.getDisplayMessageBody() == null) {
            nb.setContentText(context.getString(R.string.message_from_emergency));
        } else
            nb.setContentText(smsMessage.getDisplayMessageBody());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            nb.setColor(ContextCompat.getColor(context, R.color.colorAccent));
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                nm.notify(0, nb.build());
            }
        }, 1000);

    }

    /**
     * Get large icon to show in notification.
     *
     * @param context this apps context
     * @return scaled bitmap to use as an icon
     */
    private static Bitmap getLargeIcon(Context context) {
        BitmapDrawable bitmapDrawable = (BitmapDrawable) ContextCompat.getDrawable(context, R.mipmap.ic_launcher);
        Bitmap icon = bitmapDrawable.getBitmap();

        int height = (int) context.getResources().getDimension(android.R.dimen.notification_large_icon_height);
        int width = (int) context.getResources().getDimension(android.R.dimen.notification_large_icon_width);
        return Bitmap.createScaledBitmap(icon, width, height, false);
    }

    /**
     * Hide notification.
     *
     * @param context this apps context
     */
    public static void clearNotifications(Context context) {
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(NOTIFICATION_ID);
    }
}

package com.help.deaf.hexati.deafhelp.ui.security;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;
import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.ui.MainActivity;
import com.help.deaf.hexati.deafhelp.utils.SharedPreferencesUtils;
import com.help.deaf.hexati.deafhelp.utils.SnackbarUtils;

import java.util.List;

public class SecurityActivity extends PatternSecurityActivity implements PatternLockViewListener, View.OnClickListener, MaterialDialog.SingleButtonCallback {

    private EditText answerEditText;
    private TextView questionTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getForgottenPasswordButton().setVisibility(View.VISIBLE);
        getPatternLockIcon().setVisibility(View.VISIBLE);
        getHelpText().setVisibility(View.GONE);
        getMessageText().setText(getString(R.string.draw_pattern_to_unlock));
        getForgottenPasswordButton().setOnClickListener(this);
    }

    @Override
    public void onComplete(List<PatternLockView.Dot> pattern) {
        super.onComplete(pattern);
        String patternEntered = PatternLockUtils.patternToString(getPatternLockView(), pattern);
        checkPattern(patternEntered);
    }

    public void checkPattern(String passwordEntered) {
        if (passwordEntered.equals(SharedPreferencesUtils.getUserPattern(this))) {
            finish();
            startMainActivity();
        } else {
            getMessageText().setText(getString(R.string.draw_pattern_to_unlock_error));
        }
    }

    private void startMainActivity() {
        startActivity(new Intent(SecurityActivity.this, MainActivity.class));
    }

    @Override
    public void onClick(View view) {
        showQuestionDialog();
    }

    private View getCustomDialogView() {
        @SuppressLint("InflateParams")
        View customView = getLayoutInflater().inflate(R.layout.forgotten_pattern_dialog_custom_view, null);
        answerEditText = (EditText) customView.findViewById(R.id.forgotten_password_answer_editText);
        questionTextView = (TextView) customView.findViewById(R.id.forgotten_password_question_text);
        setQuestion();
        return customView;
    }

    private void setQuestion() {
        questionTextView.setText(SharedPreferencesUtils.getUserHelpQuestion(this));
    }

    private void showQuestionDialog() {
        new MaterialDialog.Builder(this)
                .title(R.string.help_question_title)
                .customView(getCustomDialogView(), false)
                .positiveText(R.string.reset)
                .onPositive(this)
                .negativeText(R.string.address_dialog_button_cancel)
                .onNegative(this)
                .autoDismiss(false)
                .show();
    }


    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        if (which == DialogAction.POSITIVE) {
            checkQuestionAnswer(dialog);
        } else if (which == DialogAction.NEGATIVE) {
            dialog.dismiss();
        }
    }

    private void checkQuestionAnswer(MaterialDialog dialog) {
        if (SharedPreferencesUtils.getUserHelpAnswer(this).equalsIgnoreCase(answerEditText.getText().toString())) {
            dialog.dismiss();
            SharedPreferencesUtils.disablePatternSecurity(this);
            Intent intent = new Intent(SecurityActivity.this, MainActivity.class);
            intent.setAction(SnackbarUtils.ACTION_PATTERN_RESET);
            finish();
            startActivity(intent);
        } else {
            answerEditText.setError(getString(R.string.help_question_error));
        }
    }
}

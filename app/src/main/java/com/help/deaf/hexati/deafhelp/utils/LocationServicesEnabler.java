package com.help.deaf.hexati.deafhelp.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.help.deaf.hexati.deafhelp.R;

import java.util.List;


public class LocationServicesEnabler  implements MaterialDialog.SingleButtonCallback{
    private Context ctx;

    public static boolean areLocationServicesEnabled (Context ctx) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                if(Settings.Secure.getInt(ctx.getContentResolver(), Settings.Secure.LOCATION_MODE) == Settings.Secure.LOCATION_MODE_OFF) {
                    return false;
                }

            } catch (Settings.SettingNotFoundException e) {     }

        } else {
            //noinspection deprecation
            String locationProviders = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

            if (TextUtils.isEmpty(locationProviders)){
                return false;
            }
        }
        return true;
    }

    public void showEnableDialog(Context ctx) {

        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        PackageManager manager = ctx.getPackageManager();
        List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
        if (infos.size() > 0) {
            // Czy przejść do location settings
            new MaterialDialog.Builder(ctx)
                    .title(R.string.dialog_enable_location_services_title)
                    .content(R.string.dialog_enable_location_services_message)
                    .positiveText(R.string.dialog_enable_location_yes_button)
                    .negativeText(R.string.dialog_enable_location_nope_button)
                    .onPositive(this)
                    .show();
            this.ctx = ctx;
        } else {
            // nie można przejść do location settings
            new MaterialDialog.Builder(ctx)
                    .title(R.string.dialog_enable_location_services_title)
                    .content(R.string.dialog_enable_location_manual)
                    .positiveText(R.string.dialog_enable_location_ok_button)
                    .show();
        }
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        if(which == DialogAction.POSITIVE) {
            try {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                ctx.startActivity(intent);
            } catch (ActivityNotFoundException e) { }
        }
    }
}

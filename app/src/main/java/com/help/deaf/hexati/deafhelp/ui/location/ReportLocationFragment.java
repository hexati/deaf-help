package com.help.deaf.hexati.deafhelp.ui.location;

import android.app.Activity;
import android.content.Intent;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.os.ResultReceiver;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.ui.address.Address;
import com.help.deaf.hexati.deafhelp.ui.address.AddressAdapter;
import com.help.deaf.hexati.deafhelp.ui.address.AddressDBHelper;
import com.help.deaf.hexati.deafhelp.ui.MainActivity;
import com.help.deaf.hexati.deafhelp.ui.TabbedFragment;
import com.help.deaf.hexati.deafhelp.ui.report.Report;
import com.help.deaf.hexati.deafhelp.ui.report.ReportSummaryFragment;
import com.help.deaf.hexati.deafhelp.ui.report.ReportWarningActivity;
import com.help.deaf.hexati.deafhelp.utils.CommonOperations;
import com.help.deaf.hexati.deafhelp.utils.GetAdressFromLocationIntentService;
import com.help.deaf.hexati.deafhelp.utils.NetworkStateChecker;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Location picker fragment used to pick accident location
 */

public class ReportLocationFragment extends TabbedFragment<MainActivity.TabHeader> implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
                                                                AddressAdapter.OnItemClickListener, MaterialDialog.InputCallback, MapPresenter.accidentLocationChangedInterface {

    private final static String SAVE_LOCATION_FRAGMENT = "SAVE_PICKED_LOCATION_IN_FRAGMENT";
    private final static String SAVE_NET_SNACBAR_SHOWED = "SAVE_SNACBAR_SHOWED";
    private AddressAdapter addressAdapter;
    private List<Address> addressList;
    private Address gpsAddress;
    private GoogleApiClient mGoogleApiClient = null;
    private AddressResultReceiver mAddressLineResultReceiver;
    private Boolean geocodeAddress = false;
    private LatLng geocodeAddressLatLng = null;
    private MapPresenter mapPresenter;
    private boolean mapAddressAddedToPanel;
    private LatLng accidentPositionToSave = null;
    private boolean netSnackBarShowed = false;

    /**
     * Location picker fragment for a report
     *
     * @param report report with category and icon set
     * @return new fragment with map
     */
    public static ReportLocationFragment newInstance(Report report) {
        Bundle args = new Bundle();
        args.putParcelable(ReportWarningActivity.EXTRA_REPORT, report);
        ReportLocationFragment fragment = new ReportLocationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("ConstantConditions")
    @Nullable
    @Override
    protected MainActivity.TabHeader onCreateHeader(LayoutInflater inflater, @Nullable Bundle savedInstanceState) {
        Report report = getArguments().getParcelable(ReportWarningActivity.EXTRA_REPORT);
        String headerText = CommonOperations.getReadableReportString(report.getCategory());
        return new MainActivity.TabHeader(headerText, true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        if (googleApiAvailability.isGooglePlayServicesAvailable(getActivity()) == ConnectionResult.SUCCESS) {

            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .build();
            }

        }

        mAddressLineResultReceiver = new AddressResultReceiver(new Handler());
        addressList = new ArrayList<>();
    }

    @Override
    protected int getOverlayContainer() {
        return R.id.layLocationOverlay;
    }

    @Override
    public View onCreateContentView(LayoutInflater inflater, ViewGroup container,
                                    Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event_location, container, false);

        Activity activity = getActivity();
        if (activity != null) {

            AddressDBHelper addressDBHelper = new AddressDBHelper(activity.getApplicationContext());
            addressList = addressDBHelper.loadAllAddresses();
            addressDBHelper.close();
            addressAdapter = new AddressAdapter(addressList, this);
        }

        initUI(rootView, savedInstanceState);
        return rootView;
    }

    /**
     * Method sets address of accident and shows user next step of report
     * @param addressLine address of accident
     */
    private void goToNextReportCard(String addressLine) {
        Report report = getArguments().getParcelable(ReportWarningActivity.EXTRA_REPORT);
        if(report != null)
            report.setLocation(addressLine);
        ReportSummaryFragment reportSummaryFragment = ReportSummaryFragment.newInstance(report);
        addOverlayFragment(reportSummaryFragment);
    }

    /**
     * Method inform user using SnackBar when device don't have Network connection turned on
     * @param rootView main view of location fragment
     * @param savedInstanceState saved instance bundle of location fragment
     */
    private void showNetworkSnackBar(View rootView, Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            netSnackBarShowed = savedInstanceState.getBoolean(SAVE_NET_SNACBAR_SHOWED, false);
        }

        if(!NetworkStateChecker.getConnectivityStatus(getActivity()) && !netSnackBarShowed) {
            Snackbar snackbar = Snackbar
                    .make(rootView, getString(R.string.no_network_snackbar_text), Snackbar.LENGTH_INDEFINITE)
                    .setAction(getString(R.string.no_network_snackbar_dismiss_button), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) { netSnackBarShowed = true;}
                    });

            View snackView = snackbar.getView();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)snackView.getLayoutParams();
            params.gravity = Gravity.TOP;
            snackView.setLayoutParams(params);
            TextView snackTextView = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);
            snackTextView.setMaxLines(3);
            snackbar.setActionTextColor(ContextCompat.getColor(getActivity(), R.color.red));
            snackbar.show();
        }
    }

    /**
     * Method init User Interface of location picking fragment
     * @param rootView main view of location fragment
     * @param savedInstanceState saved instance bundle of location fragment
     */
    private void initUI(View rootView, Bundle savedInstanceState) {

        showNetworkSnackBar(rootView, savedInstanceState);

        gpsAddress = new Address(getString(R.string.map_adress_name), null, Address.AddressIcon.MapLocation);

        mapAddressAddedToPanel = false;

        LatLng savedAccidentPosition;
        if(savedInstanceState != null){
            savedAccidentPosition = savedInstanceState.getParcelable(SAVE_LOCATION_FRAGMENT);
        } else {
            savedAccidentPosition = ((MainActivity) getActivity()).savedAccidentPosition;
        }
        if(savedAccidentPosition != null) {
            addGPSAddressToAddressPanel(savedAccidentPosition, true);
            accidentPositionToSave = savedAccidentPosition;
        }

        ImageView markerImageView = (ImageView) rootView.findViewById(R.id.imageViewMarker);
        mapPresenter = new MapPresenter(markerImageView, getActivity(), this, savedAccidentPosition);


        RecyclerView addressRecyclerView = (RecyclerView) rootView.findViewById(R.id.addressRecycler);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        addressRecyclerView.setLayoutManager(mLayoutManager);
        ((SimpleItemAnimator) addressRecyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        addressRecyclerView.setAdapter(addressAdapter);

        if (savedInstanceState == null) {
            //-----------------
            // Create map
            if (MapUtils.areGoogleMapsInstalled(getActivity())) {

                GoogleMapOptions mapOptions = new GoogleMapOptions().camera(mapPresenter.getFirstCameraPosition())
                        .compassEnabled(true)
                        .mapType(GoogleMap.MAP_TYPE_NORMAL)
                        .rotateGesturesEnabled(false)
                        .scrollGesturesEnabled(true)
                        .tiltGesturesEnabled(false)
                        .zoomControlsEnabled(true)
                        .zoomGesturesEnabled(true);

                SupportMapFragment mMapFragment = SupportMapFragment.newInstance(mapOptions);
                FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.mapFrameHolder, mMapFragment);
                fragmentTransaction.commit();

                mMapFragment.getMapAsync(mapPresenter);
            } else {
                MainActivity mainActivity = (MainActivity) getActivity();
                if (mainActivity != null) {
                    mainActivity.promptForGoogleMapsInstalation();
                }

                TextView noMapsText = (TextView) rootView.findViewById(R.id.textNoMap);
                noMapsText.setText(getActivity().getResources().getString(R.string.map_not_installed));
            }
        } else {
            SupportMapFragment mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapFrameHolder);
            if(mMapFragment != null) {
                mMapFragment.getMapAsync(mapPresenter);
            }
        }
    }

    @Override
    public void onItemClick(int position) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_confirm_adress_title)
                .content(getString(R.string.dialog_confirm_adress_message))
                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE)
                .input(getString(R.string.add_address_hint), addressList.get(position).getAddressLine(), this)
                .positiveText(R.string.dialog_confirm_adress_next_button)
                .negativeText(R.string.cancel_button_label)
                .show();
    }

    @Override
    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
        goToNextReportCard(input.toString());
    }

    /**
     * Adds Address of accident location when user picks it or current user location is known.
     * @param position accident position
     * @param byDraggingMarker boolean value that is true if user pick accident location by dragging map
     */
    private void addGPSAddressToAddressPanel(LatLng position, boolean byDraggingMarker) {
        if(!mapAddressAddedToPanel) {
            addressList.add(0, gpsAddress);
            addressAdapter.notifyDataSetChanged();
            updateGpsAddressLine(position, byDraggingMarker);
            mapAddressAddedToPanel = true;
        }
    }

    /**
     * Called when user changes accident location on map.
     */
    @Override
    public void accidentLocationChanged(LatLng position, boolean byDraggingMarker) {
        if(!mapAddressAddedToPanel) {
            addGPSAddressToAddressPanel(position, byDraggingMarker);
        } else {
            updateGpsAddressLine(position, byDraggingMarker);
        }

        if(byDraggingMarker) {
            accidentPositionToSave = position;
        }
    }

    /**
     * Receives reverse geocoded address and updates information in address panel.
     */
    private class AddressResultReceiver extends ResultReceiver {
        @SuppressWarnings("RestrictedApi")
        AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if(resultCode == GetAdressFromLocationIntentService.SUCCESS_RESULT) {
                String city = resultData.getString(GetAdressFromLocationIntentService.CITY_DATA_KEY);
                String street = resultData.getString(GetAdressFromLocationIntentService.STREET_DATA_KEY);
                String number = resultData.getString(GetAdressFromLocationIntentService.NUMBER_DATA_KEY);
                String postalCode = resultData.getString(GetAdressFromLocationIntentService.POSTALCODE_DATA_KEY);

                if(street == null || number == null || city == null) {
                    LatLng pos = resultData.getParcelable(GetAdressFromLocationIntentService.LATLNG_EXTRA_KEY);
                    if(pos != null)
                        street = "(GPS " +  String.format(Locale.getDefault(), "%.5f", pos.latitude) + " : " + String.format(Locale.getDefault(), "%.5f", pos.longitude) + ")" + ((street != null) ? " " + street : "");
                }

                gpsAddress.setCity(city);
                gpsAddress.setStreet(street);
                gpsAddress.setNumber(number);
                gpsAddress.setPostCode(postalCode);
                addressAdapter.notifyItemChanged(0);
            }
        }
    }

    private void getAddressFromPosition(LatLng position){
        Intent intent = new Intent(getActivity(), GetAdressFromLocationIntentService.class);
        intent.putExtra(GetAdressFromLocationIntentService.RECIVER_KEY,  mAddressLineResultReceiver);
        intent.putExtra(GetAdressFromLocationIntentService.LATLNG_EXTRA_KEY, position);
        getActivity().startService(intent);
    }

    /**
     * Update accident address based on location.
     * @param position picked accident position
     * @param byDraggingMarker boolean value that is true if user pick accident location by dragging map
     */
    private void updateGpsAddressLine(LatLng position, boolean byDraggingMarker) {
        if (position == null)
            return;

        gpsAddress.setStreet("GPS: " +  String.format(Locale.getDefault(), "%.5f", position.latitude) + " : " + String.format(Locale.getDefault(), "%.5f", position.longitude));
        gpsAddress.setNumber("");
        gpsAddress.setCity("");
        gpsAddress.setPostCode("");
        gpsAddress.setName(getString(R.string.map_adress_name));

        addressAdapter.notifyItemChanged(0);

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            getAddressFromPosition(position);
            geocodeAddress = false;
        } else {
            geocodeAddress = true;
            geocodeAddressLatLng = position;
        }
    }

    /**
     * Releases resources used by fragment.
     */
    public void close() {
        if (this.addressList != null) {
            if (addressList.size() > 0) {
                if (this.addressList.get(0) == gpsAddress) {
                    this.addressList.remove(0);
                }
            }
            this.addressList = null;
        }
        mapPresenter.close();
    }

    /**
     * Saves accident location when user return to accident category fragment.
     */
    private void saveLocationInActivity() {
        if(accidentPositionToSave != null) {
            Activity activity = getActivity();
            if (activity != null) {
                ((MainActivity) activity).savedAccidentPosition = accidentPositionToSave;
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(accidentPositionToSave != null)
            outState.putParcelable(SAVE_LOCATION_FRAGMENT, accidentPositionToSave);
        outState.putBoolean(SAVE_NET_SNACBAR_SHOWED, netSnackBarShowed);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        saveLocationInActivity();
        super.onDestroyView();
        close();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(geocodeAddress) {
            if(Geocoder.isPresent()) {
                getAddressFromPosition(geocodeAddressLatLng);
            }
        }
    }


    @Override
    public void onConnectionSuspended(int i) {    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {    }

    @Override
    public void onStart() {
        if(mGoogleApiClient != null)
            mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        if(mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        super.onStop();
    }
}

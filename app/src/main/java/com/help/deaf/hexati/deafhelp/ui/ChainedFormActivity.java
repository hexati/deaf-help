package com.help.deaf.hexati.deafhelp.ui;

import android.content.Intent;
import android.util.Log;

/**
 * Base Activity for screens used in registration form.<br>
 * Use {@link #moveToNextActivity(Class)} to move to next registration screen.<br>
 * Call {@link #finishRegistrationChain()} when last registration activity ends to finish entire chain.
 */
public abstract class ChainedFormActivity extends SmsActivity {
    final static int REQUEST_REGISTER = 110;

    @Override
    protected void onResume() {
        super.onResume();
        setResult(RESULT_CANCELED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //user finished registration, he can't press back to see form activities again
        Log.d("ChainedForm", "onActivityResult: closing " + requestCode + " " + resultCode + " " + getClass().getSimpleName());

        if (requestCode == REQUEST_REGISTER && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    /**
     * Move to the next screen during registration
     *
     * @param activity next activity class to move to
     * @param <T>      can only chain ChainedFormActivities
     */
    protected <T extends ChainedFormActivity> void moveToNextActivity(Class<T> activity) {
        startActivityForResult(new Intent(this, activity), REQUEST_REGISTER);
        Log.d("ChainedForm", "moveToNextActivity: moving to " + activity.getSimpleName());
        setResult(RESULT_OK);
    }

    /**
     * Move to the next screen during registration
     *
     * @param activity next activity class to move to
     * @param <T>      can only chain ChainedFormActivities
     * @param action   which activity should start
     */
    protected <T extends ChainedFormActivity> void moveToNextActivityWithAction(Class<T> activity, String action) {
        Intent intent = new Intent(this, activity);
        intent.setAction(action);
        Log.d("ChainedForm", "moveToNextActivityWithAction: moving to " + activity.getSimpleName());
        startActivityForResult(intent, REQUEST_REGISTER);
        setResult(RESULT_OK);
    }

    /**
     * Simple wrap around setting result and finishing. Calling this finishes entire chain of registration activities.
     */
    protected void finishRegistrationChain() {
        setResult(RESULT_OK);
        Log.d("ChainedForm", "finishRegistrationChain: " + getClass().getSimpleName());
        finish();
    }

}
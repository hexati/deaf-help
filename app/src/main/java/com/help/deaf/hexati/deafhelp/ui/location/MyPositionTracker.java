package com.help.deaf.hexati.deafhelp.ui.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.model.LatLng;
import com.help.deaf.hexati.deafhelp.ui.MainActivity;

/**
 * {@link MyPositionTracker} class gathers user location using GPS and Network providers
 */

public class MyPositionTracker {
    private Location myLastLocation = null;
    private LocationManager locationManager;
    private LocationListener locationListener = null;
    private boolean locationHasToBeTracked = false;
    private boolean trackingLocation = false;
    private boolean gpsEnabled = false;
    private static final int NEWER_LOCATION_TIME = 30 * 1000;
    private static final int MINIMUM_LOCATION_UPDATE_TIME = 5;
    private static final float MINIMUM_LOCATION_UPDATE_DISTANCE = 2.0f;
    private static final String TAG = "com.help.deaf.hexati.deafhelp.location";
    private positionChangedInterface positionChangedCallback = null;

    public interface positionChangedInterface{
        void gpsPositionChanged(LatLng position);
    }

    /**
     * Constructor of {@link MyPositionTracker}
     * @param mainActivity main activity of application
     */
    public MyPositionTracker(MainActivity mainActivity){

       Context context = mainActivity.getApplicationContext();

        trackingLocation = false;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);


        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(mainActivity, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                mainActivity.showGPSPermissionDialog();
            } else {
                mainActivity.requestGPSPermission();
            }
        } else {
            getMyLocationAtStart(context);
        }
    }

    public Location getMyLastLocation() {
        return myLastLocation;
    }

    public void setPositionChangedCallback(positionChangedInterface ppi) {
        this.positionChangedCallback = ppi;
    }

    public LatLng getMyPositionLatLng() {
        if(myLastLocation == null) {
            return null;
        } else {
            return new LatLng(myLastLocation.getLatitude(), myLastLocation.getLongitude());
        }
    }

    /**
     * Method getMyLastLocation is used to obtain last known location of user which is used to
     * center map and makes picking of accident location easier.
     */
    private void getMyLocationAtStart(Context ctx) {

        if(myLastLocation != null) {
            return;
        }

        Location networkLastLocation = null, gpsLastLocation = null;

        String locationProvider = LocationManager.NETWORK_PROVIDER;
        if (locationManager.isProviderEnabled(locationProvider)){

            if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                networkLastLocation = locationManager.getLastKnownLocation(locationProvider);
            }
        }

        locationProvider = LocationManager.GPS_PROVIDER;
        if (locationManager.isProviderEnabled(locationProvider)){

            if (ActivityCompat.checkSelfPermission(ctx, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                gpsLastLocation = locationManager.getLastKnownLocation(locationProvider);
            }
        }

        if((networkLastLocation != null) && (gpsLastLocation != null)) {
            myLastLocation = (networkLastLocation.getAccuracy() < gpsLastLocation.getAccuracy()) ? networkLastLocation : gpsLastLocation;
        } else if(networkLastLocation != null) {
            myLastLocation = networkLastLocation;
        } else {
            myLastLocation = gpsLastLocation;
        }

        if (myLastLocation != null) {
            if(positionChangedCallback != null){
                positionChangedCallback.gpsPositionChanged(getMyPositionLatLng());
            }
        }

    }


    public void locationPermissionRequestResult(boolean isGranted, Context ctx) {

        if(isGranted) {
            getMyLocationAtStart(ctx);

            if(locationHasToBeTracked && trackingLocation) {
                startTracking(ctx);
            }
        }

    }

    /**
     * Checks if new location obtained from location providers is more accurate than previous basing
     * on time, provider type and accuracy.
     * @param lastLocation previous location
     * @param newLocation compared location
     * @return true if new location is more accurate otherwise false
     */
    private boolean isLocationMoreAccurate(Location lastLocation, Location newLocation) {
        if(lastLocation == null) {
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = newLocation.getTime() - lastLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > NEWER_LOCATION_TIME;
        boolean isSignificantlyOlder = timeDelta < -NEWER_LOCATION_TIME;
        boolean isNewer = timeDelta > 0;

        if (isSignificantlyNewer) {
            return true;
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (newLocation.getAccuracy() - lastLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta <= 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider;
        String provider1 = newLocation.getProvider();
        String provider2 = lastLocation.getProvider();

        if((provider1 != null) && (provider2 != null)) {
            isFromSameProvider = provider1.equals(provider2);
        } else {
            isFromSameProvider = (provider1 == null) && (provider2 == null);
        }


        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Method starts tracking location of user based on GPS and Network providers and sets up
     * necessary callbacks
     * @param ctx application context
     * @return true if tracking user location is possible otherwise false
     */
    public boolean startTracking(Context ctx) {

        locationHasToBeTracked = true;

        if (trackingLocation || (ActivityCompat.checkSelfPermission(ctx, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED)) {
            return false;
        }

        trackingLocation = true;

        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {

                if(isLocationMoreAccurate(myLastLocation, location)) {
                    myLastLocation = location;

                    if(positionChangedCallback != null) {
                        positionChangedCallback.gpsPositionChanged(getMyPositionLatLng());
                    }
                }

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {
                if(provider.equals(LocationManager.GPS_PROVIDER)){
                    gpsEnabled = true;
                }
            }

            public void onProviderDisabled(String provider) {
                if(provider.equals(LocationManager.GPS_PROVIDER)){
                    gpsEnabled = false;
                }
            }
        };

        if (ActivityCompat.checkSelfPermission(ctx, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MINIMUM_LOCATION_UPDATE_TIME, MINIMUM_LOCATION_UPDATE_DISTANCE, locationListener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MINIMUM_LOCATION_UPDATE_TIME, MINIMUM_LOCATION_UPDATE_DISTANCE, locationListener);
        }

        return true;
    }

    /**
     * Method called after tracking user location is no longer needed.
     */
    public void stopTracking(){
        if(locationListener != null) {
            locationManager.removeUpdates(locationListener);
            locationListener = null;
        }
        trackingLocation = false;
        locationHasToBeTracked = false;
    }

    public boolean isGpsEnabled() {
        return gpsEnabled;
    }

}

package com.help.deaf.hexati.deafhelp.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.messages.SmsReceiver;
import com.help.deaf.hexati.deafhelp.messages.SmsSender;
import com.help.deaf.hexati.deafhelp.ui.actions.UserRegistrationDataActions;
import com.help.deaf.hexati.deafhelp.ui.address.AddressesActivity;
import com.help.deaf.hexati.deafhelp.ui.history.HistorySQL;
import com.help.deaf.hexati.deafhelp.ui.registration.RulesActivity;
import com.help.deaf.hexati.deafhelp.ui.registration.UserDataActivity;
import com.help.deaf.hexati.deafhelp.ui.security.HelpQuestionActivity;
import com.help.deaf.hexati.deafhelp.ui.security.SetPatternActivity;
import com.help.deaf.hexati.deafhelp.ui.views.TextViewWithFont;
import com.help.deaf.hexati.deafhelp.utils.SharedPreferencesUtils;
import com.help.deaf.hexati.deafhelp.utils.SnackbarUtils;

import static android.app.Activity.RESULT_OK;



@SuppressWarnings("FieldCanBeLocal")
public class SettingsFragment extends TabbedFragment<MainActivity.TabHeader> implements View.OnClickListener {
    private static final String TAG = "SettingsFragment";
    public static final int SECURITY_STATUS_REQUEST = 7910;
    public static final int CHANGE_HELP_QUESTION_REQUEST = 7911;
    private LinearLayout settingsAddAddresses;
    private LinearLayout settingsChangeData;
    private LinearLayout settingsLock;
    private LinearLayout settingsSendOpinion;
    private LinearLayout settingsAbout;
    private TextViewWithFont settingsRemoveAccount;

    private SmsReceiver mSmsReceiver;
    private LinearLayout settingsChangePassword;
    private LinearLayout settingsChangeHelpQuestion;

    private TextView patternSecurityIndicator;
    private LinearLayout moreSecurityOptions;

    @Nullable
    @Override
    protected MainActivity.TabHeader onCreateHeader(LayoutInflater inflater, @Nullable Bundle savedInstanceState) {
        return new MainActivity.TabHeader(getString(R.string.settings_title), false);
    }

    @Override
    protected View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_settings, container, false);

        settingsAddAddresses = (LinearLayout) v.findViewById(R.id.settings_add_addresses);
        settingsChangeData = (LinearLayout) v.findViewById(R.id.settings_change_data);
        settingsLock = (LinearLayout) v.findViewById(R.id.settings_lock);
        moreSecurityOptions = (LinearLayout) v.findViewById(R.id.settings_more);
        settingsChangePassword = (LinearLayout) v.findViewById(R.id.settings_pattern_change);
        settingsChangeHelpQuestion = (LinearLayout) v.findViewById(R.id.settings_help_question_change);
        settingsSendOpinion = (LinearLayout) v.findViewById(R.id.settings_sendopinion);
        settingsAbout = (LinearLayout) v.findViewById(R.id.settings_about);
        settingsRemoveAccount = (TextViewWithFont) v.findViewById(R.id.settings_remove_account);

        patternSecurityIndicator = (TextView) v.findViewById(R.id.settings_lock_indicator);

        settingsAddAddresses.setOnClickListener(this);
        settingsChangeData.setOnClickListener(this);
        settingsLock.setOnClickListener(this);
        settingsChangePassword.setOnClickListener(this);
        settingsChangeHelpQuestion.setOnClickListener(this);
        settingsSendOpinion.setOnClickListener(this);
        settingsAbout.setOnClickListener(this);
        settingsRemoveAccount.setOnClickListener(this);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateSecurityIndicatorStatus();
    }

    private void updateSecurityIndicatorStatus() {
        if (SharedPreferencesUtils.getPatternStatus(getContext())) {
            patternSecurityIndicator.setText(getString(R.string.lock_enabled));

            moreSecurityOptions.setVisibility(View.VISIBLE);

        } else {
            patternSecurityIndicator.setText(getString(R.string.lock_disabled));
            moreSecurityOptions.setVisibility(View.GONE);

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_about:
                showAbout();
                break;
            case R.id.settings_add_addresses:
                Intent intent = new Intent(getActivity(), AddressesActivity.class);
                intent.putExtra(AddressesActivity.OPEN_FOR_EDITION, true);
                startActivity(intent);
                break;
            case R.id.settings_change_data:
                changeUserInfo();
                break;
            case R.id.settings_lock:
                changePatternSecurityStatus();
                break;
            case R.id.settings_pattern_change:
                startSetPatternActivity();
                break;
            case R.id.settings_help_question_change:
                startHelpQuestionChangeActivity();
                break;
            case R.id.settings_remove_account:
                requestRemoveAccount();
                break;
            case R.id.settings_sendopinion:
                sendOpinion();
                break;
        }
    }

    private void changePatternSecurityStatus() {
        if (SharedPreferencesUtils.getPatternStatus(getContext())) {
            disablePatternSecurity();
        } else {
            startSetPatternActivity();
        }
    }

    private void disablePatternSecurity() {
        SharedPreferencesUtils.disablePatternSecurity(getContext());
        updateSecurityIndicatorStatus();
    }

    private void startHelpQuestionChangeActivity() {
        startActivityForResult(new Intent(getActivity(), HelpQuestionActivity.class), CHANGE_HELP_QUESTION_REQUEST);
    }

    public void startSetPatternActivity() {
        startActivityForResult(new Intent(getActivity(), SetPatternActivity.class), SECURITY_STATUS_REQUEST);
    }

    @Override
    public void onResume() {
        super.onResume();

        mSmsReceiver = SmsReceiver.createAndRegisterSendingInstance(getContext(), new SmsReceiver.SmsListener.SpecialAdapter(SmsSender.TYPE_DELETE_ACCOUNT) {
            @Override
            public void onSpecialSentError(String errorMessage, String messageText) {
                new MaterialDialog.Builder(getActivity())
                        .title(R.string.sending_error)
                        .content(errorMessage)
                        .positiveText(R.string.ok)
                        .show();
            }

            @Override
            public void onSpecialDelivered(SmsMessage message) {
                SharedPreferencesUtils.setRegistrationComplete(getContext(), false);
                SharedPreferencesUtils.saveVerificationComplete(getContext(), false);
                new HistorySQL(getContext()).clearDatabase();
                if(getActivity()!=null) {
                    startActivity(new Intent(getActivity(), RulesActivity.class));
                    getActivity().finish();
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mSmsReceiver);
        mSmsReceiver = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SettingsFragment.SECURITY_STATUS_REQUEST && resultCode == RESULT_OK) {
            SnackbarUtils.getSnackbarForAction(SnackbarUtils.ACTION_PATTERN_CHANGED, settingsAddAddresses).show();
        } else if (requestCode == SettingsFragment.CHANGE_HELP_QUESTION_REQUEST && resultCode == RESULT_OK) {
            SnackbarUtils.getSnackbarForAction(SnackbarUtils.ACTION_HELP_QUESTION_CHANGED, settingsAddAddresses).show();
        }
        updateSecurityIndicatorStatus();


    }

    private void sendOpinion() {
        final String emailAddress = getResources().getString(R.string.email_address);

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", emailAddress, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.email_title_opinion));
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(emailIntent, getString(R.string.send_email)));
    }

    private void requestRemoveAccount() {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.settings_delete_account)
                .content(R.string.settings_delete_acc_dialogcontent)
                .positiveText(R.string.delete)
                .negativeText(R.string.anuluj).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                settingsRemoveAccount.setEnabled(false);
                SmsSender.sendRemoveAccountMessage(getActivity());
            }
        })
                .show();

    }

    private void changeUserInfo() {
        Intent intent = new Intent(getActivity(), UserDataActivity.class);
        intent.setAction(UserRegistrationDataActions.ACTION_CHANGE_USER_DATA);
        startActivity(intent);
    }

    private void showAbout() {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.settings_about)
                .content(R.string.about_content)
                .positiveText(R.string.ok)
                .show();
    }
}

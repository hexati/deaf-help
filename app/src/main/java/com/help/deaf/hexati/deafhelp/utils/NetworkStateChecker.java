package com.help.deaf.hexati.deafhelp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Helper class consisting static method getConnectivityStatus used to determine if users device
 * have Network access turned on.
 */

public class NetworkStateChecker {

    public static boolean getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            int netType = activeNetwork.getType();
            if(netType == ConnectivityManager.TYPE_WIFI
                    || netType == ConnectivityManager.TYPE_MOBILE) {
                return true;
            }
        }
        return false;
    }
}
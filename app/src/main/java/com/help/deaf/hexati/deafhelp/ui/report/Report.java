package com.help.deaf.hexati.deafhelp.ui.report;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;

import java.util.ArrayList;

/**
 * Report that is propagated as a parcelable extra during report building chain.
 */
@SuppressWarnings("WeakerAccess")
public class Report implements Parcelable {
    /**
     * ID of report event type (from xml)
     */
    private static final String CARD_ID = "CARD_ID";
    /**
     * ArrayList defining the report event
     */
    private static final String CATEGORY = "CATEGORY";
    /**
     * Resource ID of icon to replace default one with.
     */
    private static final String ICON_RES = "ICON_RES";
    /**
     * String with location picked by the user
     */
    private static final String LOCATION = "LOCATION";
    /**
     * String with more details entered by the user
     */
    private static final String DETAILS = "DETAILS";

    private String id;
    private ArrayList<String> category;
    @DrawableRes
    private int icon;
    private String location;
    private String details;

    public Report() {

    }

    public Report setId(String id) {
        this.id = id;
        return this;
    }

    public Report setCategory(ArrayList<String> reportPath) {
        this.category = reportPath;
        return this;
    }

    public Report setIcon(int icon) {
        this.icon = icon;
        return this;
    }

    public Report setLocation(String location) {
        this.location = location;
        return this;
    }

    public Report setDetails(String details) {
        this.details = details;
        return this;
    }

    private Report(Parcel in) {
        Bundle b = in.readBundle(getClass().getClassLoader());
        id = b.getString(CARD_ID);
        category = b.getStringArrayList(CATEGORY);
        icon = b.getInt(ICON_RES, 0);
        location = b.getString(LOCATION);
        details = b.getString(DETAILS);
    }

    public String getId() {
        return id;
    }

    public ArrayList<String> getCategory() {
        return category;
    }

    @DrawableRes
    public int getIcon() {
        return icon;
    }

    public String getDetails() {
        return details;
    }

    public String getLocation() {
        return location;
    }

    public static final Creator<Report> CREATOR = new Creator<Report>() {
        @Override
        public Report createFromParcel(Parcel in) {
            return new Report(in);
        }

        @Override
        public Report[] newArray(int size) {
            return new Report[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        Bundle b = new Bundle(5);
        b.putString(CARD_ID, id);
        b.putStringArrayList(CATEGORY, category);
        b.putInt(ICON_RES, icon);
        b.putString(LOCATION, location);
        b.putString(DETAILS, details);
        parcel.writeBundle(b);
    }
}

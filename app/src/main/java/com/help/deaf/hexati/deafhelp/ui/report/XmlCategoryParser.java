package com.help.deaf.hexati.deafhelp.ui.report;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.support.annotation.NonNull;
import android.util.Log;

import com.help.deaf.hexati.deafhelp.utils.XmlFields;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Build list of cards from XMl file.
 */
class XmlCategoryParser implements XmlFields{
    private final static String TAG = XmlCategoryParser.class.getSimpleName();

    //parameters and return value of this object
    private final Context context;
    private final List<String> path;
    private ArrayList<ReportCategory> result;

    //internal attributes needed while parsing
    private XmlResourceParser mParser;
    private boolean mIsInRoot;
    private boolean mIsInTargetCard;
    private int mParentIcon = 0;

    /**
     * New parser for a given card.
     *
     * @param context context to access resources
     * @param path    path of card to find subcards for. If empty or null, build root of cards.
     */
    XmlCategoryParser(@NonNull Context context, List<String> path) {
        this.context = context;
        this.path = path;
    }

    /**
     * Perform parsing of xml file and build the result.
     *
     * @return this object
     */
    XmlCategoryParser parse() {
        if (result != null) {
            throw new IllegalStateException("This XmlParser already performed parsing, getResult() or discard the instance.");
        }
        result = new ArrayList<>();
        try {
            mParser = context.getResources().getXml(XML_RESOURCE);
            int eventType = mParser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        parseStart();
                        break;
                    case XmlPullParser.END_TAG:
                        if (mIsInTargetCard) {
                            eventType = XmlPullParser.END_DOCUMENT;
                        }
                        break;
                }
                if (eventType != XmlPullParser.END_DOCUMENT)
                    eventType = mParser.next();
            }
            if (!mIsInRoot) {
                throw new XmlPullParserException("XML Document contained no valid root element.");
            }
            mParser.close();
            mParser = null;
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        return this;
    }

    /**
     * Parse at the start tag. This actually performs most of the work:
     * <ul>
     * <li>If root element was not found, keep moving forward until it's found</li>
     * <li>If we found the root, keep looking for target card</li>
     * <li>If we're in target card, each start tag is a new card to return</li>
     * </ul>
     */
    private void parseStart() throws IOException, XmlPullParserException {
        if (mIsInTargetCard) {
            createCard();   //while inside the target card each child is added to the result
        } else if (mIsInRoot) {
            if (mParser.getName().equals(XML_CARD)) {
                //check if name / depth combination if okay and flag for target card...
                if (!isPathValid()) {
                    skip();
                } else if (path.size() == mParser.getDepth() - 1)
                    mIsInTargetCard = true; //all path/name tags match
                //store icon of current element and give it to children without icon
                mParentIcon = mParser.getAttributeResourceValue(null, XML_ICON, mParentIcon);
            } else {
                throw new XmlPullParserException("Invalid XMl tag: " + mParser.getName());
            }
        } else if (mParser.getName().equals(XML_ROOT)) {
            mIsInRoot = true;
            if (path == null || path.isEmpty())
                mIsInTargetCard = true;
        } else
            Log.w(TAG, "parseStart: tag start is not a root");
    }

    /**
     * Check if current start tag is on valid path.
     *
     * @return true if name attribute matches corresponding value from path
     */
    private boolean isPathValid() {
        return mParser.getAttributeValue(null, XML_NAME).equals(path.get(mParser.getDepth() - 2));
    }

    /**
     * Create card from current parser position. This will also skip to the end of entire tag without parsing children.
     */
    private void createCard() throws IOException, XmlPullParserException {
        String name = mParser.getAttributeValue(null, XML_NAME);
        String cardId = mParser.getAttributeValue(null, XML_ID);
        int iconResource = mParser.getAttributeResourceValue(null, XML_ICON, mParentIcon);
        //see if the card has children - this moves the parser forward
        boolean hasChildren = skip() > 0;
        if (hasChildren) {
            result.add(new ReportCategory(name, iconResource));
        } else {
            if (cardId == null)
                throw new XmlPullParserException("Cards without children must contain an ID!");
            result.add(new ReportCategory(cardId, name, iconResource));
        }
    }

    /**
     * Skip to matching end tag. <a href="https://developer.android.com/training/basics/network-ops/xml.html#skip">Source</a>
     *
     * @return amount of skipped start tags (children) for current tag (without nested starts)
     */
    private int skip() throws XmlPullParserException, IOException {
        if (mParser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int skippedStartTags = 0;
        int depth = 1;
        while (depth != 0) {
            switch (mParser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    if (depth == 1)
                        skippedStartTags++;
                    depth++;
                    break;
            }
        }
        return skippedStartTags;
    }

    /**
     * Get result from {@link #parse()}
     *
     * @return result
     */
    ArrayList<ReportCategory> getResult() {
        if (result == null) {
            throw new IllegalStateException("Call parse() before reading result.");
        }
        return result;
    }
}

package com.help.deaf.hexati.deafhelp.ui.location;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;


/**
 * MapUtils class consist static method  areGoogleMapsInstalled used to determine if user device
 * supports google maps
 */

class MapUtils {

    /**
     * areGoogleMapsInstalled method used to determine if user device supports GoogleMaps
     * @param activity needed for packageManager
     * @return boolean value representing support of GoogleMaps
     */
    @SuppressWarnings("unused")
    static boolean areGoogleMapsInstalled(Activity activity) {
        try {
            if (activity != null) {
                ApplicationInfo info = activity.getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}

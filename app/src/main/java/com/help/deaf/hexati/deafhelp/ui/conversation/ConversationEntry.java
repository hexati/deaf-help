package com.help.deaf.hexati.deafhelp.ui.conversation;

import android.support.annotation.LayoutRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;

import com.help.deaf.hexati.deafhelp.R;

/**
 * Entry for conversation with help service. Provides different layout depending on who sent the message.
 */
class ConversationEntry {
    /**
     * System message (error or end conversation)
     */
    public final static int TYPE_SYSTEM = R.layout.recycler_conversation_system;
    /**
     * Message from user
     */
    public final static int TYPE_USER = R.layout.recycler_conversation_user;
    /**
     * Reply from help service
     */
    public final static int TYPE_HELP = R.layout.recycler_conversation_help;

    /**
     * ID mapped from content provider. When injecting {@link #TYPE_SYSTEM} messages, be sure they are unique (ideally decreasing negative values);
     */
    private long _ID = -1;
    private long time;
    private int type;
    private String message;

    /**
     * _ID mapped from content provider, or manually set when using {@link #TYPE_SYSTEM}.
     *
     * @return unique ID for this message.
     */
    long get_ID() {
        return _ID;
    }

    /**
     * Get type of this message.
     *
     * @return message type (resource ID of layout for this item)
     */
    @LayoutRes
    public int getType() {
        return type;
    }

    public ConversationEntry(long time, int type, String message) {
        this.time = time;
        this.type = type;
        this.message = message;
    }

    /**
     * New conversation entry.
     *
     * @param _ID     If _ID is negative then it's not final.
     * @param time    time when message was received. If 0 then it's not shown.
     * @param type    type of message
     * @param message text of message
     */
    public ConversationEntry(long _ID, long time, int type, String message) {
        this._ID = _ID;
        this.time = time;
        this.type = type;
        this.message = message;
    }

    /**
     * ViewHolder displaying conversation entries.
     */
    static class ConversationEntryViewHolder extends RecyclerView.ViewHolder {
        TextView txtTime;
        TextView txtMessage;

        /**
         * New viewholder
         *
         * @param itemView itemview to find views of
         */
        ConversationEntryViewHolder(View itemView) {
            super(itemView);
            txtTime = (TextView) itemView.findViewById(R.id.txtRecyclerConversationTime);
            txtMessage = (TextView) itemView.findViewById(R.id.txtRecyclerConversationMessage);
        }

        /**
         * Show a given conversation entry. If this is a system message with time = 0, text turns red.
         *
         * @param data entry to show
         */
        void bind(ConversationEntry data) {
            updateTime(data);
            if (data.type == TYPE_SYSTEM) {
                if (data.time == 0)
                    txtMessage.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.colorAccent));
                else
                    txtMessage.setTextColor(ContextCompat.getColor(itemView.getContext(), android.R.color.tertiary_text_light));
            }
            txtMessage.setText(data.message);
        }

        /**
         * Update value of time text view to show relative time passed since message was created.<br>
         * We use systems timestamp to determine current time.
         *
         * @param data conversation entry to read time from
         */
        private void updateTime(ConversationEntry data) {
            if (data.time < 1) {
                txtTime.setVisibility(View.GONE);
            } else {
                txtTime.setVisibility(View.VISIBLE);
                if (System.currentTimeMillis() - data.time < DateUtils.MINUTE_IN_MILLIS) {
                    txtTime.setText(R.string.now);
                } else
                    txtTime.setText(DateUtils.getRelativeTimeSpanString(data.time));
            }
        }

        /**
         * Partially update ViewHolder
         * @param data object this ViewHolder is currently bound to
         * @param what data to update in this ViewHolder, one of {@link Payload}
         */
        void update(ConversationEntry data, Object what) {
            if (what == Payload.Time) {
                updateTime(data);
            }
        }
    }

    /**
     * Objects that can be used as a payload for {@link ConversationEntryViewHolder#update(ConversationEntry, Object)},
     */
    enum Payload {
        Time, Message
    }
}

package com.help.deaf.hexati.deafhelp.ui.location;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.ui.MainActivity;
import com.help.deaf.hexati.deafhelp.utils.LocationServicesEnabler;
import com.help.deaf.hexati.deafhelp.utils.PermissionUtils;

/**
 * MapPresenter class manages GoogleMap fragment and Accident Location Marker
 * When user change zoom to street lever {@link MapPresenter} changes map mode to hybrid view which
 * is better for picking location. After accident location is picked it notifies {@link ReportLocationFragment}
 * using accidentLocationChangedInterface
 */

class MapPresenter implements LocationSource, OnMapReadyCallback, MyPositionTracker.positionChangedInterface, GoogleMap.OnCameraMoveListener,
                                                GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraMoveCanceledListener,
                                                GoogleMap.OnCameraIdleListener,  GoogleMap.OnMyLocationButtonClickListener {

    private static final float MAP_DEFAULT_ZOOM = 17.0f;
    private static final float MAP_HYBRID_ZOOM = 19.0f;
    private static final LatLng DEFAULT_MAP_CENTER = new LatLng(51.759, 19.457);
    private static final String TAG = "com.help.deaf.hexati";
    private GoogleMap googleMap = null;
    private Boolean hybridMapIsOn = false;
    private Marker positionMarker = null;
    private Boolean sendLocationToMaps = false;
    private Boolean homeButtonWasClicked = false;
    private ImageView markerImageView;
    private OnLocationChangedListener onLocationChangedListener;
    private Activity mActivity;
    private MyPositionTracker myPositionTracker;
    private LatLng accidentPosition = null;
    private accidentLocationChangedInterface accidentLocationChangedCallback;
    private Boolean markerWasUserDragged = false;
    private boolean markerWasDraggedUp = false;

    /**
     * accidentLocationChangedInterface  is used to notify {@link ReportLocationFragment} about
     * accident location
     */
    interface accidentLocationChangedInterface{
        void accidentLocationChanged(LatLng position, boolean byDraggingMarker);
    }

    /**
     * Constructor of {@link MapPresenter} class
     * @param markerImageView is View with marker image in center displayed when user is picking location
     * @param mActivity Activity that consist MapFragment
     * @param callback accidentLocationChangedInterface to notify accident location
     * @param savedAccidentPosition accident position to put marker pin n, or <code>null</code> if not known yet
     */
    MapPresenter(ImageView markerImageView, Activity mActivity, accidentLocationChangedInterface callback, LatLng savedAccidentPosition) {
        this.markerImageView = markerImageView;
        this.mActivity = mActivity;
        this.accidentLocationChangedCallback = callback;
        markerImageView.setVisibility(View.INVISIBLE);
        myPositionTracker = ((MainActivity) mActivity).getMyPositionTracker();
        myPositionTracker.setPositionChangedCallback(this);

        if(savedAccidentPosition != null){
            accidentPosition = savedAccidentPosition;
            markerWasUserDragged = true;
        } else {
            accidentPosition = myPositionTracker.getMyPositionLatLng();
        }
    }

    /**
     * Method used to setup map
     * @param googleMap map to be controlled by {@link MapPresenter}
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setLocationSource(this);
        setupMap();
    }

    /**
     * Method returns {@link CameraPosition} based on user location if known or default zoom and
     * camera position which centers Map.
     * @return CameraPosition for map
     */
    CameraPosition getFirstCameraPosition() {
        float mapZoom = MAP_DEFAULT_ZOOM;
        LatLng firstPosition = null;
        if (myPositionTracker != null) {
            firstPosition = myPositionTracker.getMyPositionLatLng();
        }
        if (firstPosition == null) {
            firstPosition = DEFAULT_MAP_CENTER;
            mapZoom = 5.5f;
        }
        return CameraPosition.fromLatLngZoom(firstPosition, mapZoom);
    }

    /**
     * Enables Map "home button" if user position could be obtained
     */
    private void tryEnableHomeButton () {
        if(mActivity != null && googleMap != null) {
            if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Boolean enableHomeLocation = LocationServicesEnabler.areLocationServicesEnabled(mActivity);

                googleMap.setMyLocationEnabled(enableHomeLocation);
                googleMap.getUiSettings().setMyLocationButtonEnabled(enableHomeLocation);
            }
        }
    }

    /**
     * Configures Google Map for better user experience and sets needed callback to {@link MapPresenter}
     */
    private void setupMap() {
        if(googleMap == null)
            return;

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        tryEnableHomeButton();
        googleMap.getUiSettings().setMapToolbarEnabled(false);

        googleMap.setIndoorEnabled(true);
        googleMap.setBuildingsEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        CameraPosition cameraPosition;

        if(myPositionTracker != null && accidentPosition == null) {
            accidentPosition = myPositionTracker.getMyPositionLatLng();
        }

        float mapZoom = MAP_DEFAULT_ZOOM;
        if(accidentPosition == null) {
            accidentPosition = DEFAULT_MAP_CENTER;
            mapZoom = 5.5f;

            // Dummy location marker
            if(mActivity != null) {
                if(!PermissionUtils.checkGpsPermission(mActivity)) {
                    createPositionMarker();
                } else if(!LocationServicesEnabler.areLocationServicesEnabled(mActivity)) {
                    createPositionMarker();
                }
            }

        } else {
            if(accidentLocationChangedCallback != null)
                accidentLocationChangedCallback.accidentLocationChanged(accidentPosition, false);

            // Valid Location marker
            createPositionMarker();

            if (sendLocationToMaps) {
                Location loc = myPositionTracker.getMyLastLocation();
                if (onLocationChangedListener != null && loc != null)  {
                    onLocationChangedListener.onLocationChanged(loc);
                }
            }
        }

        cameraPosition = new CameraPosition.Builder().target(accidentPosition).zoom(mapZoom).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        googleMap.moveCamera(cameraUpdate);

        googleMap.setOnCameraMoveListener(this);
        googleMap.setOnCameraMoveStartedListener(this);
        googleMap.setOnCameraMoveCanceledListener(this);
        googleMap.setOnCameraIdleListener(this);
        googleMap.setOnMyLocationButtonClickListener(this);

    }

    /**
     * Creates Marker which pin accident location on map.
     */
    private void createPositionMarker() {
        if (googleMap != null && mActivity !=null) {
            if (accidentPosition != null && positionMarker == null) {

                MarkerOptions markerOptions = new MarkerOptions().position(accidentPosition)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin))
                        .title(mActivity.getResources().getString(R.string.map_marker_title));

                positionMarker = googleMap.addMarker(markerOptions);

                markerImageView.setVisibility(View.INVISIBLE);
            } else {
                Log.e(TAG, "can't create marker - position unknown!!!");
            }
        }
    }

    /**
     * Move accident position marker to newPosition
     * @param newPosition new position of marker
     */
    private void movePositionMarker(LatLng newPosition) {
        if (positionMarker != null) {
            positionMarker.setPosition(newPosition);

            CameraPosition cameraPosition = new CameraPosition.Builder().target(newPosition).zoom(googleMap.getCameraPosition().zoom).build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            googleMap.moveCamera(cameraUpdate);
        }
    }

    /**
     * Method starts picking accident location by dragging
     * @param reason cause of camera move
     */
    @Override
    public void onCameraMoveStarted(int reason) {
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            markerImageUp();
        } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION) {
            markerImageUp();
        }
    }

    /**
     * changes map mode to hybrid view when zoom is near street level which
     * is better for picking location
     */
    @Override
    public void onCameraMove() {
        if (googleMap == null)
            return;

        float currentZoom = this.googleMap.getCameraPosition().zoom;

        if (!hybridMapIsOn && (currentZoom >= MAP_HYBRID_ZOOM)) {
            hybridMapIsOn = true;
            this.googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        } else if (hybridMapIsOn && (currentZoom < MAP_HYBRID_ZOOM)) {
            hybridMapIsOn = false;
            this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
    }

    @Override
    public void onCameraMoveCanceled() {
        markerImageDown(accidentPosition);
    }

    @Override
    public void onCameraIdle() {
        markerImageDown(null);
    }

    /**
     * Picks up marker from map and display marker overlay when user starts to pick accident location
     */
    private void markerImageUp() {

        markerWasDraggedUp = true;
        markerImageView.setVisibility(View.VISIBLE);
        if(positionMarker != null) {
            positionMarker.setVisible(false);

            markerWasUserDragged = !homeButtonWasClicked;
            homeButtonWasClicked = false;
        }
    }

    /**
     * Hides marker overlay when user pick accident location by dragging map and pins up accident
     * location on map
     */
    private void markerImageDown(LatLng position) {
        markerImageView.setVisibility(View.INVISIBLE);

        if(!markerWasDraggedUp)
            return;

        if(position == null) {
            position = googleMap.getCameraPosition().target;

            if(position == null)
                return;
        }

        accidentPosition = position;

        if(positionMarker == null) {
            createPositionMarker();
        }

        if(positionMarker != null) {

            positionMarker.setPosition(position);
            positionMarker.setVisible(true);

            if(accidentLocationChangedCallback != null)
                accidentLocationChangedCallback.accidentLocationChanged(accidentPosition, true);
        }

        markerWasDraggedUp = false;
    }

    @Override
    public boolean onMyLocationButtonClick() {
        homeButtonWasClicked = true;
        return false;
    }

    /**
     * Releases resources used by {@link MapPresenter}
     */
    void close() {
        if (myPositionTracker != null) {
            myPositionTracker.setPositionChangedCallback(null);
            this.myPositionTracker = null;
            this.accidentLocationChangedCallback = null;
        }
        mActivity = null;
    }

    /**
     * Updates user position on map and set accident Marker on that position if user didn't set it
     * manually by dragging map
     * @param position new user position
     */
    @Override
    public void gpsPositionChanged(LatLng position) {
        if (accidentPosition == null) {
            setupMap();
            if(accidentLocationChangedCallback != null)
                accidentLocationChangedCallback.accidentLocationChanged(position, false);
            accidentPosition = position;
        }

        if (!markerWasUserDragged) {
            accidentPosition = position;

            if (googleMap != null) {
                if (positionMarker == null) {
                    createPositionMarker();
                }
                movePositionMarker(accidentPosition);
            }
            if(accidentLocationChangedCallback != null)
                accidentLocationChangedCallback.accidentLocationChanged(accidentPosition, false);
        }

        if (sendLocationToMaps) {
            Location location = myPositionTracker.getMyLastLocation();
            if (onLocationChangedListener != null && location != null) {
                onLocationChangedListener.onLocationChanged(location);
            }
        }
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        sendLocationToMaps = true;
        this.onLocationChangedListener = onLocationChangedListener;
    }

    @Override
    public void deactivate() {
        sendLocationToMaps = false;
    }

}

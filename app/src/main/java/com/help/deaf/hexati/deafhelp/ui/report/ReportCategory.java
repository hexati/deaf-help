package com.help.deaf.hexati.deafhelp.ui.report;

import android.support.annotation.DrawableRes;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.help.deaf.hexati.deafhelp.R;

/**
 * Clickable report category
 */
class ReportCategory {
    final String id;
    final String categoryName;
    final int icon;

    /**
     * New report category (with children)
     *
     * @param categoryName category name
     * @param icon         resource id of icon to display
     */
    ReportCategory(String categoryName, @DrawableRes int icon) {
        id = null;
        this.categoryName = categoryName;
        this.icon = icon;
    }

    /**
     * New report card (no children)
     *
     * @param id           card id
     * @param categoryName card name
     * @param icon         resource id of icon to display
     */
    ReportCategory(String id, String categoryName, @DrawableRes int icon) {
        this.id = id;
        this.categoryName = categoryName;
        this.icon = icon;
    }

    /**
     * See if this has no children (this has no id).
     * @return true if this is a category, false it it's a final card
     */
    boolean hasChildren(){
        return id == null;
    }

    static class ReportViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgIcon;
        TextView txtTitle;
        ViewGroup layCardLayout;

        private ReportCategoryFragment hostFragment;

        ReportViewHolder(View itemView) {
            super(itemView);
            imgIcon = (ImageView) itemView.findViewById(R.id.imgRecyclerReportIcon);
            layCardLayout = (ViewGroup) itemView.findViewById(R.id.layReportCardInnerLayout);
            txtTitle = (TextView) itemView.findViewById(R.id.txtRecyclerReportTitle);
            layCardLayout.setOnClickListener(this);
        }

        void bind(ReportCategoryFragment hostFragment, ReportCategory data) {
            this.hostFragment = hostFragment;
            txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f);
            imgIcon.setImageResource(data.icon);
            txtTitle.setText(data.categoryName);
            txtTitle.requestLayout();
        }

        void unbind() {
            hostFragment = null;
        }

        @Override
        public void onClick(View view) {
            if (hostFragment != null)
                hostFragment.onReportCategorySelected(getAdapterPosition());
        }
    }
}

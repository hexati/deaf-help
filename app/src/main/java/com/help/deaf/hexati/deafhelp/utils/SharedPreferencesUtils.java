package com.help.deaf.hexati.deafhelp.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class SharedPreferencesUtils {

    private static final String SHARED_PREFERENCES_KEY = SharedPreferencesUtils.class.getName() + ".preferences_key";
    private static final String USER_NAME_KEY =  SHARED_PREFERENCES_KEY + ".user_name";
    private static final String USER_BIRTH_DATE_KEY =  SHARED_PREFERENCES_KEY + ".user_date";
    private static final String USER_EMAIL_KEY =  SHARED_PREFERENCES_KEY + ".user_email";
    private static final String USER_PHONE_NUMBER_KEY = SHARED_PREFERENCES_KEY + ".user_phone_number";
    private static final String REGISTRATION_KEY =  SHARED_PREFERENCES_KEY + ".registration_complete";
    private static final String VERIFICATION_KEY =  SHARED_PREFERENCES_KEY + ".verification_complete";
    private static final String USER_PATTERN_KEY = SHARED_PREFERENCES_KEY + ".user_pattern";
    private static final String USER_HELP_QUESTION = SHARED_PREFERENCES_KEY + ".user_help_question";
    private static final String USER_HELP_ANSWER = SHARED_PREFERENCES_KEY + ".user_help_answer";
    private static final String PATTERN_ENABLED_KEY = SHARED_PREFERENCES_KEY + "pattern_enabled";
    private static final String USER_PESEL_NUMBER = SHARED_PREFERENCES_KEY + "user_pesel_number";


    private static String getDefaultKey() {
        return SHARED_PREFERENCES_KEY;
    }


    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        return getSharedPreferences(context).edit();
    }

    private static void saveBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    private static boolean getBoolean(Context context, String key, boolean defValue) {
        return getSharedPreferences(context).getBoolean(key, defValue);
    }

    private static void saveString(Context context, String key, String value) {
        getEditor(context).putString(key, value).apply();
    }

    private static void remove(Context context, String key) {
        getEditor(context.getApplicationContext()).remove(key).apply();
    }

    private static void saveFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).apply();
    }

    private static float getFloat(Context context, String key, float defValue) {
        return getSharedPreferences(context).getFloat(key, defValue);
    }

    private static String getString(Context context, String key, String defValue) {
        return getSharedPreferences(context).getString(key, defValue);
    }

    private static String getString(Context context, String key) {
        return getString(context, key, "");
    }

    private static void saveInt(Context context, String key, int value) {
        getEditor(context).putInt(key, value).apply();
    }

    private static int getInt(Context context, String key, int defValue) {
        return getSharedPreferences(context).getInt(key, defValue);
    }

    private static int getInt(Context context, String key) {
        return getInt(context, key, 0);
    }

    private static void saveLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).apply();
    }

    private static long getLong(Context context, String key) {
        return getSharedPreferences(context).getLong(key, 0L);
    }




    ///////////////////////////


    public static void saveUserName(Context context, String name) {
        saveString(context.getApplicationContext(), USER_NAME_KEY, name);
    }

    public static String getUserName(Context context){
        return getString(context, USER_NAME_KEY, "");
    }

    public static void saveUserBirthDate(Context context, String date) {
        saveString(context.getApplicationContext(), USER_BIRTH_DATE_KEY, date);
    }

    public static String getUserBirthDate(Context context){
        return getString(context, USER_BIRTH_DATE_KEY, "");
    }

    public static void saveVerificationComplete(Context context, Boolean isComplete) {
        saveBoolean(context.getApplicationContext(), VERIFICATION_KEY, isComplete);
    }
    public static Boolean getVerificationComplete(Context context){
        return getBoolean(context, VERIFICATION_KEY, false);
    }

    public static void saveUserEmail(Context context, String userEmail) {
        saveString(context.getApplicationContext(), USER_EMAIL_KEY, userEmail);
    }

    public static String getUserEmail(Context context){
        return getString(context, USER_EMAIL_KEY, "");
    }

    public static void saveUserPattern(Context context, String pattern){
        saveString(context.getApplicationContext(), USER_PATTERN_KEY, pattern);
    }

    public static String getUserPattern(Context context){
        return getString(context, USER_PATTERN_KEY,"");
    }

    public static void saveUserHelpQuestion(Context context, String question){
        saveString(context, USER_HELP_QUESTION, question);
    }

    public static String getUserHelpQuestion(Context context){
        return getString(context,USER_HELP_QUESTION );
    }

    public static void saveUserHelpAnswer(Context context, String answer){
        saveString(context, USER_HELP_ANSWER, answer);
    }

    public static String getUserHelpAnswer(Context context){
        return getString(context,USER_HELP_ANSWER );
    }

    public static void savePatternStatus(Context context, boolean enabled){
        saveBoolean(context.getApplicationContext(), PATTERN_ENABLED_KEY, enabled);
    }

    public static boolean getPatternStatus(Context context){
        return getBoolean(context, PATTERN_ENABLED_KEY, false);
    }

    public static void disablePatternSecurity(Context context){
        savePatternStatus(context, false);
        saveUserPattern(context, "");
    }

    public static void saveUserPhoneNumber(Context context, String phoneNumber) {
        saveString(context, USER_PHONE_NUMBER_KEY, phoneNumber);
    }

    public static void saveUserPeselNumber(Context context, String peselNumber) {
        saveString(context, USER_PESEL_NUMBER, peselNumber);
    }

    public static String getUserPeselNumber(Context context){
        return getString(context,USER_PESEL_NUMBER);
    }

    public static String getUserPhoneNumber(Context context){
        return getString(context, USER_PHONE_NUMBER_KEY);
    }

    public static boolean getRegistrationComplete(Context context) {
        return getBoolean(context, REGISTRATION_KEY, false);
    }

    public static void  setRegistrationComplete(Context context, boolean completed){
        saveBoolean(context, REGISTRATION_KEY, completed);
    }
}

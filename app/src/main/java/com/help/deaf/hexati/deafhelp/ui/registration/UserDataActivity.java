package com.help.deaf.hexati.deafhelp.ui.registration;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.telephony.SmsMessage;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.messages.SmsReceiver;
import com.help.deaf.hexati.deafhelp.messages.SmsSender;
import com.help.deaf.hexati.deafhelp.ui.ChainedFormActivity;
import com.help.deaf.hexati.deafhelp.ui.actions.UserRegistrationDataActions;
import com.help.deaf.hexati.deafhelp.utils.AutoAddTextWatcher;
import com.help.deaf.hexati.deafhelp.utils.CommonOperations;
import com.help.deaf.hexati.deafhelp.utils.DateUtils;
import com.help.deaf.hexati.deafhelp.utils.PermissionUtils;
import com.help.deaf.hexati.deafhelp.utils.PeselValidator;
import com.help.deaf.hexati.deafhelp.utils.SharedPreferencesUtils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;


public class UserDataActivity extends ChainedFormActivity implements DatePickerDialog.OnDateSetListener, View.OnClickListener, View.OnFocusChangeListener, TextWatcher {

    private static final String DATE_PICKER_DIALOG_TAG = "datePickerTag";
    private static final String TAG = UserDataActivity.class.getSimpleName();
    private String registrationAction;
    private String mPhoneNumberPrefix;
    private int mSmsType;
    private EditText etName;
    private EditText etEmail;
    private EditText etBirthDate;
    private EditText etPesel;
    private EditText etPhoneNumber;
    private FloatingActionButton btnSend;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        init();
        PermissionUtils.requestReceiveSmsPermission(this);

    }

    private void init() {
        registrationAction = getIntent().getAction();
        mPhoneNumberPrefix = getString(R.string.number_country_prefix)+ " ";
        initUI();
        if (getIntent().getAction() != null) {
            if (registrationAction.equals(UserRegistrationDataActions.ACTION_NEW_USER)) {
                uiNewUser();
                mSmsType = SmsSender.TYPE_REGISTER;
            } else if (registrationAction.equals(UserRegistrationDataActions.ACTION_CHANGE_USER_DATA)) {
                uiChangeUserData();
                mSmsType = SmsSender.TYPE_UPDATE_INFO;
            }
        } else {
            throw new RuntimeException("Registration action is null");
        }
    }

    @Override
    protected SmsReceiver onCreateSmsReceiver() {
        return SmsReceiver.createAndRegisterSendingInstance(this, new SmsReceiver.SmsListener.SpecialAdapter(mSmsType) {
            @Override
            public void onSpecialSentError(String errorMessage, String messageText) {
                sendError(errorMessage);
            }

            @Override
            public void onSpecialDelivered(SmsMessage message) {
                dataSend();
            }
        });
    }

    private void uiNewUser() {
        initNewUserTitle();
    }

    private void uiChangeUserData() {
        initChangeUserDataTitle();
        fillUserData();
    }

    private void fillUserData() {
        etName.setText(SharedPreferencesUtils.getUserName(this));
        etEmail.setText(SharedPreferencesUtils.getUserEmail(this));
        etBirthDate.setText(SharedPreferencesUtils.getUserBirthDate(this));
        etPesel.setText(SharedPreferencesUtils.getUserPeselNumber(this));
        etPhoneNumber.setText(SharedPreferencesUtils.getUserPhoneNumber(this));
    }

    private void initNewUserTitle() {
        TextView textView = (TextView) findViewById(R.id.tvTitle);
        textView.setText(getString(R.string.register_title));
    }

    private void initChangeUserDataTitle() {
        TextView textView = (TextView) findViewById(R.id.tvTitle);
        textView.setText(getString(R.string.settings_change_data));
    }

    private void initUI() {
        initEditTextViews();
        initFabButton();
    }

    private void initEditTextViews() {
        etName = (EditText) findViewById(R.id.etUserName);
        etBirthDate = (EditText) findViewById(R.id.etDateOfBirth);
        etEmail = (EditText) findViewById(R.id.etUserEmail);
        etPesel = (EditText) findViewById(R.id.etPeselNumber);
        etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        etBirthDate.setOnClickListener(this);
        etBirthDate.setFocusable(false);
        etPhoneNumber.setOnFocusChangeListener(this);
        etPhoneNumber.addTextChangedListener(this);
        etPhoneNumber.addTextChangedListener(new AutoAddTextWatcher(etPhoneNumber, " ", 7, 10));
    }



    private void initFabButton() {
        btnSend = (FloatingActionButton) findViewById(R.id.floatingNext);
        btnSend.setOnClickListener(this);
        btnSend.show();
    }

    private boolean checkDataCorrectness() {
        if(SmsReceiver.DEBUG)
            return true;

        boolean dataCorrect = true;
        if (etName.getText().toString().isEmpty()) {
            etName.setError(getString(R.string.empty_field_error));
            dataCorrect = false;
        }
        if (etEmail.getText().toString().isEmpty() || !CommonOperations.isValidEmail(etEmail.getText())) {
            etEmail.setError(getString(R.string.error_wrong_email));
            dataCorrect = false;
        } else {
            etEmail.setError(null);
        }

        if(!validatePhoneNumber()){
            dataCorrect = false;
        }

        if (!validateBirthDate()) {
            dataCorrect = false;
        }

        if(!validatePeselNumber()){
            dataCorrect = false;
        }

        return dataCorrect;
    }

    private boolean validatePhoneNumber() {
        if(!etPhoneNumber.getText().toString().isEmpty()){
            if(etPhoneNumber.getText().toString().length()<15){
                etPhoneNumber.setError(getString(R.string.error_phone_nr_short));
                return false;
            }
        } else {
            etPhoneNumber.setError(getString(R.string.empty_field_error));
            return false;
        }
        return true;
    }

    private boolean validateBirthDate() {
        boolean dataCorrect = true;
        if (etBirthDate.getText().toString().isEmpty()) {
            etBirthDate.setError(getString(R.string.error_required_field));
            dataCorrect = false;
        } else if (!(DateUtils.checkBirthDate(etBirthDate.getText().toString(), DateUtils.MIN_USER_AGE))) {
            etBirthDate.setFocusable(true);
            etBirthDate.setFocusableInTouchMode(true);
            etBirthDate.requestFocus();
            etBirthDate.setError(getString(R.string.error_wrong_birthdate));
            etBirthDate.setFocusableInTouchMode(false);
            dataCorrect = false;
        } else {
            etBirthDate.setError(null);
        }
        return dataCorrect;
    }

    private boolean validatePeselNumber() {
        boolean dataCorrect = true;
        if (!etPesel.getText().toString().isEmpty()) {
            PeselValidator peselValidator = new PeselValidator(etPesel.getText().toString());
            if (peselValidator.isValid()) {
                if (!peselValidator.isBirthDateValid(etBirthDate.getText().toString())) {
                    dataCorrect = false;
                    etPesel.setError(getString(R.string.error_birthdate));
                }

            } else {
                etPesel.setError(getString(R.string.error_pesel_number));
                dataCorrect = false;
            }
        } else {
            etPesel.setError(getString(R.string.error_required_field));
            dataCorrect = false;
        }
        return dataCorrect;
    }

    private void dispatchData() {
        String userName = etName.getText().toString();
        String userDateOfBirth = etBirthDate.getText().toString();
        String userEmail = etEmail.getText().toString();
        String userPesel = etPesel.getText().toString();
        String userPhone = etPhoneNumber.getText().toString();
        saveInSharedPrefs(userName, userEmail, userDateOfBirth, userPesel, userPhone);
        sendRegistrationData();
        btnSend.hide();
    }

    private void saveInSharedPrefs(String userName, String userEmail, String userDateOfBirth, String peselNumber, String phoneNumber) {
        SharedPreferencesUtils.saveUserName(this, userName);
        SharedPreferencesUtils.saveUserBirthDate(this, userDateOfBirth);
        SharedPreferencesUtils.saveUserEmail(this, userEmail);
        SharedPreferencesUtils.saveUserPhoneNumber(this, phoneNumber);
        SharedPreferencesUtils.saveUserPeselNumber(this, peselNumber);
    }

    private void sendRegistrationData() {
        if (registrationAction.equals(UserRegistrationDataActions.ACTION_NEW_USER)) {
            SmsSender.sendRegistrationMessage(this);
        } else if (registrationAction.equals(UserRegistrationDataActions.ACTION_CHANGE_USER_DATA)) {
            SmsSender.sendUpdateUserInfoMessage(this);
        }
    }

    private void sendError(String errorReason) {
        new MaterialDialog.Builder(this)
                .title(R.string.error)
                .content(getString(R.string.sending_error) + " " + errorReason)
                .positiveText(R.string.ok)
                .show();
        btnSend.show();
    }

    private void dataSend() {
        if (registrationAction.equals(UserRegistrationDataActions.ACTION_NEW_USER)) {
            SharedPreferencesUtils.setRegistrationComplete(this, true);
            btnSend.show();
            moveToNextActivity(VerificationActivity.class);
        } else if (registrationAction.equals(UserRegistrationDataActions.ACTION_CHANGE_USER_DATA)) {
            finish();
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        etBirthDate.setText(DateUtils.formatToDate(year, monthOfYear, dayOfMonth));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.floatingNext:
                tryToSendData(view);
                break;
            case R.id.etDateOfBirth:
                if (etBirthDate.getError() != null) {
                    etBirthDate.setError(null);
                }
                showDatePicker();
                break;
        }
    }

    private void tryToSendData(View parentView) {
        showSendingSnackBar(parentView);
        if (checkDataCorrectness()) {
            if (PermissionUtils.checkSendSmsPermission(UserDataActivity.this)) {
                dispatchData();
            } else
                showNoSmsPermissionSnackBar(parentView);
        } else
            showWrongDataSnackBar(parentView);
    }

    private void showSendingSnackBar(View parentView) {
        Snackbar.make(parentView, getResources().getString(R.string.sending_data), Snackbar.LENGTH_LONG)
                .show();
    }

    private void showNoSmsPermissionSnackBar(View parentView) {
        Snackbar.make(parentView, getResources().getString(R.string.sending_no_permission), Snackbar.LENGTH_LONG)
                .show();
    }

    private void showWrongDataSnackBar(View parentView) {
        Snackbar.make(parentView, getResources().getString(R.string.sending_wrong_data), Snackbar.LENGTH_LONG)
                .show();
    }


    private void showDatePicker() {
        DatePickerDialog dpd = DatePickerDialog.newInstance(UserDataActivity.this);
        dpd.showYearPickerFirst(true);
        dpd.dismissOnPause(true);
        dpd.setVersion(DatePickerDialog.Version.VERSION_1);
        dpd.show(getFragmentManager(), DATE_PICKER_DIALOG_TAG);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(etPhoneNumber!=null && !etPhoneNumber.getText().toString().contains("+48 "))
        etPhoneNumber.setText(mPhoneNumberPrefix, TextView.BufferType.EDITABLE);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(!s.toString().contains(mPhoneNumberPrefix)){
            etPhoneNumber.setText(mPhoneNumberPrefix);
            Selection.setSelection(etPhoneNumber.getText(), etPhoneNumber.getText().length());
        }
    }
}

package com.help.deaf.hexati.deafhelp.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings;


public class DeviceSystemUtils {
    @SuppressLint("HardwareIds")
    public static String getAndroidID(Context context){
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

}

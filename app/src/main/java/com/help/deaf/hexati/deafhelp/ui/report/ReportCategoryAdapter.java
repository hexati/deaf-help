package com.help.deaf.hexati.deafhelp.ui.report;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.help.deaf.hexati.deafhelp.R;

import java.util.ArrayList;

/**
 * Display clickable cards for the user.
 */
class ReportCategoryAdapter extends RecyclerView.Adapter<ReportCategory.ReportViewHolder> {
    private ArrayList<ReportCategory> items;
    private ReportCategoryFragment hostFragment;

    ReportCategoryAdapter(ReportCategoryFragment hostFragment) {
        this.hostFragment = hostFragment;
    }

    void setItems(ArrayList<ReportCategory> items) {
        this.items = items;
    }

    @Override
    public ReportCategory.ReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_report_card, parent, false);
        return new ReportCategory.ReportViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ReportCategory.ReportViewHolder holder, int position) {
        holder.bind(hostFragment, items.get(position));
    }

    @Override
    public void onViewRecycled(ReportCategory.ReportViewHolder holder) {
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }
}

package com.help.deaf.hexati.deafhelp.ui;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.ui.history.HistoryFragment;
import com.help.deaf.hexati.deafhelp.ui.report.ReportCategoryFragment;

/**
 * Pager adapter that allows direct access to the fragments, even after activity gets recreated.
 */
class MainActivityPagerAdapter extends FragmentPagerAdapter {
    private ReportCategoryFragment reportCategoryFragment;
    private HistoryFragment historyFragment;
    private SettingsFragment settingsFragment;

    private MainActivity mainActivity;

    private static final int REPORT_CATEGORY_FRAGMENT_POSITION = 0;
    private static final int HISTORY_FRAGMENT_POSITION = 1;
    private static final int SETTINGS_FRAGMENT_POSITION = 2;

    /**
     * Create and attach this adapter to a given viewpager
     *
     * @param mainActivity activity hosting this adapter
     * @param viewPager    viewpager using this adapter
     */
    MainActivityPagerAdapter(MainActivity mainActivity, ViewPager viewPager) {
        super(mainActivity.getSupportFragmentManager());
        this.mainActivity = mainActivity;
        viewPager.setAdapter(this);
        reinstateTabs(viewPager);
    }

    /**
     * Force creation/find existing tabs in fragment manager.
     *
     * @param viewPager viewpager this adapter is attached to
     */
    private void reinstateTabs(ViewPager viewPager) {
        startUpdate(viewPager);
        reportCategoryFragment = (ReportCategoryFragment) instantiateItem(viewPager, REPORT_CATEGORY_FRAGMENT_POSITION);
        historyFragment = (HistoryFragment) instantiateItem(viewPager, HISTORY_FRAGMENT_POSITION);
        settingsFragment = (SettingsFragment) instantiateItem(viewPager, SETTINGS_FRAGMENT_POSITION);
        finishUpdate(viewPager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case REPORT_CATEGORY_FRAGMENT_POSITION:
                return ReportCategoryFragment.newInstance();
            case HISTORY_FRAGMENT_POSITION:
                return new HistoryFragment();
            case SETTINGS_FRAGMENT_POSITION:
                return new SettingsFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case REPORT_CATEGORY_FRAGMENT_POSITION:
                return mainActivity.getResources().getString(R.string.tab1_raport);
            case HISTORY_FRAGMENT_POSITION:
                return mainActivity.getResources().getString(R.string.tab2_history);
            case SETTINGS_FRAGMENT_POSITION:
                return mainActivity.getResources().getString(R.string.tab3_settings);
        }
        return null;
    }

    /**
     * Get fragment at position. Note this might be null if a tab was not instantiated yet (not probable for tab 0 and 1).
     *
     * @param position position to get fragment from
     * @return fragment at position
     */
    TabbedFragment<MainActivity.TabHeader> getTabAt(int position) {
        switch (position) {
            case REPORT_CATEGORY_FRAGMENT_POSITION:
                return reportCategoryFragment;
            case HISTORY_FRAGMENT_POSITION:
                return historyFragment;
            case SETTINGS_FRAGMENT_POSITION:
                return settingsFragment;
            default:
                return null;
        }
    }

    /**
     * Get report category picker fragment
     *
     * @return fragment
     */
    @NonNull
    public ReportCategoryFragment getReportCategoryFragment() {
        return reportCategoryFragment;
    }

    /**
     * Get settings fragment
     *
     * @return settings fragment
     */
    @NonNull
    public SettingsFragment getSettingsFragment() {
        return settingsFragment;
    }

    /**
     * Get history fragment
     *
     * @return fragment
     */
    @NonNull
    public HistoryFragment getHistoryFragment() {
        return historyFragment;
    }
}

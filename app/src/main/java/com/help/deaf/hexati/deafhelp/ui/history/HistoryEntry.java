package com.help.deaf.hexati.deafhelp.ui.history;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.help.deaf.hexati.deafhelp.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * History entry (type and time when it happened)
 */
public class HistoryEntry {
    long time;
    String type;
    /**
     * This is smsId of message that terminated the report conversation or next report id-1.
     */
    long lastSmsTime;

    /**
     * History entry from the database
     *
     * @param time        timestamp when user requested help
     * @param type        type defined as an ID in the xml file
     * @param lastSmsTime timestamp of sms that terminated the report, or next report time -1. If 0 then conversation is still ongoing.
     */
    public HistoryEntry(long time, String type, long lastSmsTime) {
        this.type = type;
        this.time = time;
        this.lastSmsTime = lastSmsTime;
    }

    /**
     * Timestamp of report start
     */
    public long getTime() {
        return time;
    }

    /**
     * Timestamp of report end. If 0 then this is still ongoing.
     */
    public long getEndTime() {
        return lastSmsTime;
    }

    /**
     * Represents {@link HistoryEntry} in the adapter.
     */
    static class HistoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgIcon;
        TextView txtTitle;
        TextView txtTime;
        private HistoryFragment hostFragment;

        /**
         * New instance of this viewholder
         *
         * @param itemView itemview to find views in
         */
        HistoryViewHolder(View itemView) {
            super(itemView);
            imgIcon = (ImageView) itemView.findViewById(R.id.imgRecyclerHistoryIcon);
            txtTitle = (TextView) itemView.findViewById(R.id.txtRecyclerHistoryTitle);
            txtTime = (TextView) itemView.findViewById(R.id.txtRecyclerHistoryTime);
            itemView.setOnClickListener(this);
        }

        /**
         * Bind history entry to this viewholder
         *
         * @param data         data to bind
         * @param hostFragment fragment this viewholder will use as callback
         */
        void bind(HistoryEntry data, HistoryFragment hostFragment) {
            this.hostFragment = hostFragment;
            XmlHistoryParser.HistoryXmlItemDetails itemDetails = hostFragment.getDetails(data.type);
            txtTitle.setText(itemDetails.name);
            imgIcon.setImageResource(itemDetails.iconId);
            //change date format?
            String dateString = new SimpleDateFormat("d MMM yyyy [HH:mm:ss]", Locale.getDefault()).format(new Date(data.time));
            txtTime.setText(dateString);
        }

        /**
         * Forget about host fragment.
         */
        void unbind() {
            hostFragment = null;
        }

        @Override
        public void onClick(View view) {
            if (hostFragment != null)
                hostFragment.onHistoryItemClicked(getAdapterPosition());
        }
    }
}

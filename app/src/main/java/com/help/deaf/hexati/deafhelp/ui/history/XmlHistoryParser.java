package com.help.deaf.hexati.deafhelp.ui.history;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.support.annotation.DrawableRes;
import android.support.v4.util.ArraySet;
import android.util.ArrayMap;

import com.help.deaf.hexati.deafhelp.utils.CommonOperations;
import com.help.deaf.hexati.deafhelp.utils.XmlFields;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

/**
 * Parse definition of name + icon for given IDs.
 */
class XmlHistoryParser implements XmlFields {
    private final Context context;
    private final ArraySet<String> reportIds;

    private XmlResourceParser mParser;
    private Stack<Integer> mIconsStack;
    private ArrayList<String> mReportCategoryStack;

    private ArrayMap<String, HistoryXmlItemDetails> result;

    /**
     * XML parser to find name + icon for all IDs in the history.
     *
     * @param context   context to access resources with
     * @param reportIds set of IDs to find definitions for.
     */
    XmlHistoryParser(Context context, ArraySet<String> reportIds) {
        this.context = context;
        this.reportIds = reportIds;
    }

    /**
     * Parse Xml file to build the result.
     *
     * @return this
     */
    XmlHistoryParser parse() {
        if (result != null)
            throw new IllegalStateException("This XmlParser already performed parsing, getResult() or discard the instance.");
        result = new ArrayMap<>();
        mIconsStack = new Stack<>();
        mReportCategoryStack = new ArrayList<>();

        try {
            mParser = context.getResources().getXml(XML_RESOURCE);
            int eventType = mParser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        parseStartTag();
                        break;
                    case XmlPullParser.END_TAG:
                        parseEndTag();
                        break;
                }

                //read more if we still need to parse some ids
                if (result.size() >= reportIds.size())
                    eventType = XmlPullParser.END_DOCUMENT;
                else
                    eventType = mParser.next();
            }

            mParser.close();
            mParser = null;
            if (result.size() < reportIds.size())
                throw new XmlPullParserException("Failed to resolve all IDs - check parameters and XML.");
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        return this;
    }

    /**
     * Parse start tag:
     * <ul>
     * <li>If we're in card (with children) push name and icon on stack</li>
     * <li>If we're in card (no children) compare id with ones we look for and save it</li>
     * </ul>
     */
    private void parseStartTag() throws IOException, XmlPullParserException {
        if (mParser.getName().equals(XML_CARD)) {

            //push the icon on the stack (if missing then copy previous icon)
            String elementName = mParser.getAttributeValue(null, XML_NAME);
            int fallbackIcon = mIconsStack.isEmpty() ? 0 : mIconsStack.peek();
            int elementIcon = mParser.getAttributeResourceValue(null, XML_ICON, fallbackIcon);

            //push the icon and name...
            mIconsStack.push(elementIcon);
            mReportCategoryStack.add(elementName);

            //no children, parse and see if we need data of this element.
            String elementId = mParser.getAttributeValue(null, XML_ID);
            //we've got an ID we want defined!
            if (reportIds.contains(elementId)) {
                String resolvedName = CommonOperations.getReadableReportString(mReportCategoryStack);
                result.put(elementId, new HistoryXmlItemDetails(resolvedName, elementIcon));
            }
        }
    }

    /**
     * Parse end tag: pop report names and icons from stacks as needed.
     */
    private void parseEndTag() throws IOException, XmlPullParserException {
        if (mParser.getName().equals(XML_CARD)) {
            mIconsStack.pop();
            mReportCategoryStack.remove(mReportCategoryStack.size() - 1);
        }
    }

    /**
     * Read parsed results.
     * @return map from parsing.
     */
    public ArrayMap<String, HistoryXmlItemDetails> getResult() {
        if (result == null) {
            throw new IllegalStateException("Call parse() before reading result.");
        }
        return result;
    }

    /**
     * Details of an item.
     */
    static class HistoryXmlItemDetails {
        final String name;
        @DrawableRes
        final int iconId;

        /**
         * Create details
         * @param name name to print for the user
         * @param iconId drawable resource ID for icon
         */
        private HistoryXmlItemDetails(String name, int iconId) {
            this.name = name;
            this.iconId = iconId;
        }
    }
}

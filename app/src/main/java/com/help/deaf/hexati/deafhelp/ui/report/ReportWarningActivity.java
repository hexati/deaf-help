package com.help.deaf.hexati.deafhelp.ui.report;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.messages.SmsReceiver;
import com.help.deaf.hexati.deafhelp.messages.SmsSender;
import com.help.deaf.hexati.deafhelp.ui.SmsActivity;
import com.help.deaf.hexati.deafhelp.ui.history.HistorySQL;

/**
 * Final fullscreen activity that shows when report is ready to be sent.
 */
public class ReportWarningActivity extends SmsActivity implements View.OnClickListener {
    private final static String TAG = "ReportWarningActivity";
    /**
     * Time before cancel button unlocks after sending (must be larger than sent timeout)
     */
    private final static int TIMEOUT_MS = SmsReceiver.SENT_TIMEOUT_MS + 2000;
    /**
     * Time for icon animation (ms).
     */
    private final static int ANIMATION_TIME = 1000;
    /**
     * User entered warning screen but haven't send the report yet.
     */
    private final static int STATE_NOT_SEND = 0;
    /**
     * User pressed the send button.
     */
    private final static int STATE_SEND_REQUESTED = 1;
    /**
     * Device reported successful report sent.
     */
    private final static int STATE_SEND_DONE = 2;
    /**
     * Device reported failure while sending the report;
     */
    private final static int STATE_SEND_FAILED = -2;
    /**
     * Device received successful delivery report.
     */
    private final static int STATE_DELIVERY_DONE = 3;

    public static final int REQUEST_CODE = 222;
    public static final String EXTRA_REPORT = "EXTRA_REPORT";

    /**
     * Int value included in result Intent, contains Id of new report returned from help.
     */
    public static String RESULT_EXTRA_REPORT_ID = "RESULT_EXTRA_REPORT_ID";

    private TextView btnSend;
    private TextView btnCancel;
    private ViewGroup layLoadingContainer;
    private TextView txtLoadingInfo;
    private TextView txtMessage;
    private ImageView imgSendDone;
    private ProgressBar prgLoading;

    private Report report;

    private int mReportState = STATE_NOT_SEND;
    private Handler mTimeoutHandler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_warning);
        btnCancel = (TextView) findViewById(R.id.btnActivityReportWarningCancel);
        btnSend = (TextView) findViewById(R.id.btnActivityReportWarningSend);
        btnSend.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        layLoadingContainer = (ViewGroup) findViewById(R.id.layActivityReportWarningLoadingContainer);
        txtLoadingInfo = (TextView) findViewById(R.id.txtActivityReportWarningLoadingInfo);
        txtMessage = (TextView) findViewById(R.id.txtActivityReportWarningMessage);
        prgLoading = (ProgressBar) findViewById(R.id.prgActivityReportWarningLoading);
        imgSendDone = (ImageView) findViewById(R.id.imgActivityReportWarningLoadingDone);

        report = getIntent().getParcelableExtra(EXTRA_REPORT);

        if (report.getIcon() > 0) {
            ((ImageView) findViewById(R.id.imgActivityReportWarningImage)).setImageResource(report.getIcon());
        }
    }

    @Override
    protected SmsReceiver onCreateSmsReceiver() {
        return SmsReceiver.createAndRegisterSendingInstance(this, new SmsReceiver.SmsListener.SpecialAdapter(SmsSender.TYPE_NEW_REPORT) {
            @Override
            public void onSpecialSent(String message) {
                onSmsSent();
            }

            @Override
            public void onSpecialDelivered(SmsMessage message) {
                onSmsDelivered(message);
            }

            @Override
            public void onSpecialSentError(String errorMessage, String messageText) {
                onSmsSendFailure(errorMessage);
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == btnSend) {
            sendReportSms();
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    /**
     * Core method - prepares and sends the report SMS to the help service.
     */
    private void sendReportSms() {
        if (mReportState == STATE_SEND_FAILED) {
            //user is retrying failed sms sending
            transitionProgressIntoIcon(ANIMATION_TIME, true);
            txtLoadingInfo.setText(R.string.report_sending);
        }
        btnSend.setVisibility(View.INVISIBLE);
        btnCancel.setVisibility(View.INVISIBLE);
        txtMessage.setVisibility(View.INVISIBLE);
        layLoadingContainer.setVisibility(View.VISIBLE);
        mReportState = STATE_SEND_REQUESTED;
        mTimeoutHandler.postDelayed(mTimeoutRunnable, TIMEOUT_MS);

        SmsSender.sendNewReportMessage(this, report);
    }

    /**
     * Triggered after sms was successfully sent out from the device.<br>
     * Hide progress bar and start waiting for delivery report.
     */
    private void onSmsSent() {
        if(mReportState > STATE_SEND_REQUESTED){
            Log.e(TAG, "onSmsSent: skipped - state is "+mReportState);
            return;
        }
        txtLoadingInfo.setText(R.string.report_send);
        mReportState = STATE_SEND_DONE;
        mTimeoutHandler.removeCallbacks(mTimeoutRunnable);
        mTimeoutHandler.postDelayed(mTimeoutRunnable, TIMEOUT_MS);
        imgSendDone.setImageResource(R.drawable.ic_check);
        transitionProgressIntoIcon(ANIMATION_TIME * 2, false);
    }

    /**
     * Triggered when delivery report is received back by the device.<br>
     * Insert new report into history, set result and finish this activity.
     *
     * @param message sms that we were sending out
     */
    private void onSmsDelivered(SmsMessage message) {
        if(mReportState > STATE_SEND_DONE){
            Log.e(TAG, "onSmsDelivered: skipped - state is "+mReportState);
            return;
        }
        long timestamp = message != null ? message.getTimestampMillis() : System.currentTimeMillis() - SmsReceiver.SENT_TIMEOUT_MS;
        mReportState = STATE_DELIVERY_DONE;
        mTimeoutHandler.removeCallbacks(mTimeoutRunnable);
        txtLoadingInfo.setText(R.string.report_received);
        new HistorySQL(this).saveNewReport(report.getId(), timestamp);
        setResult(RESULT_OK, new Intent().putExtra(RESULT_EXTRA_REPORT_ID, timestamp));
        finish();
    }

    /**
     * Failed to send an sms
     *
     * @param message message to display as an error
     */
    private void onSmsSendFailure(String message) {
        mReportState = STATE_SEND_FAILED;
        txtLoadingInfo.setText(message);
        mTimeoutHandler.removeCallbacks(mTimeoutRunnable);
        imgSendDone.setImageResource(R.drawable.ic_action_cross);
        btnSend.setVisibility(View.VISIBLE);
        btnCancel.setVisibility(View.VISIBLE);
        transitionProgressIntoIcon(ANIMATION_TIME, false);
    }

    /**
     * Transition progress bar into image.
     *
     * @param duration animation duration in milliseconds.
     * @param reverse  true to run the animation in reverse (icon into progress)
     */
    private void transitionProgressIntoIcon(int duration, boolean reverse) {
        float fromAlpha = reverse ? 1f : 0f;
        float toAlpha = reverse ? 0f : 1f;
        float fromAngle = reverse ? 0f : -120f;
        float toAngle = reverse ? 120f : 0f;


        imgSendDone.setVisibility(View.VISIBLE);
        prgLoading.setVisibility(View.VISIBLE);
        imgSendDone.setAlpha(fromAlpha);
        imgSendDone.setRotation(fromAngle);
        imgSendDone.animate()
                .rotation(toAngle)
                .alpha(toAlpha)
                .setInterpolator(new LinearOutSlowInInterpolator())
                .setDuration(duration)
                .withEndAction(reverse ? new Runnable() {
                    @Override
                    public void run() {
                        imgSendDone.setVisibility(View.INVISIBLE);
                    }
                } : null);

        prgLoading.setAlpha(toAlpha);
        prgLoading.animate()
                .alpha(fromAlpha)
                .setDuration(duration)
                .withEndAction(reverse ? null : new Runnable() {
                    @Override
                    public void run() {
                        prgLoading.setVisibility(View.INVISIBLE);
                    }
                });
    }

    /**
     * Runnable triggered when sms sending is timing out, allowing user for dirty cancel of sending report.
     */
    private Runnable mTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            switch (mReportState) {
                case STATE_SEND_DONE:
                    txtLoadingInfo.setText(R.string.report_no_recived);
                case STATE_SEND_REQUESTED:
                    btnCancel.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        //prevent user from leaving activity while report is being sent
        switch (mReportState) {
            case STATE_SEND_REQUESTED:
            case STATE_SEND_DONE:
                return;
        }
        super.onBackPressed();
    }
}

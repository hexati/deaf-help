package com.help.deaf.hexati.deafhelp.ui;

import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import com.help.deaf.hexati.deafhelp.messages.SmsReceiver;

/**
 * Activity that handles SmsReceiver during its lifecycle.
 */
public abstract class SmsActivity extends AppCompatActivity {
    private SmsReceiver mSmsReceiver;

    /**
     * Create SmsReceiver and attach any listener you need.
     * @return sms receiver or null if not needed
     */
    protected SmsReceiver onCreateSmsReceiver() {
        return null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSmsReceiver = onCreateSmsReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mSmsReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mSmsReceiver);
            mSmsReceiver = null;
        }
    }
}

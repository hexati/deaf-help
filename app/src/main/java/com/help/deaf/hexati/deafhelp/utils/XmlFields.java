package com.help.deaf.hexati.deafhelp.utils;

import android.support.annotation.XmlRes;

import com.help.deaf.hexati.deafhelp.R;

/**
 * Contains static names of fields in {@link #XML_RESOURCE} document.
 */
public interface XmlFields {
    /**
     * XML resource holding data about all cards.
     */
    @XmlRes
    int XML_RESOURCE = R.xml.kategorie;
    /**
     * XML tag name of root element.
     */
    String XML_ROOT = "zgloszenie";
    /**
     * XML tag name of Card (basic xml element). This can be inside {@link #XML_ROOT} or nested inside another card.
     */
    String XML_CARD = "card";
    /**
     * XML attribute of a {@link #XML_CARD}, this is a user readable {@link String}. This is never null, but not guaranteed to be unique across document.
     */
    String XML_NAME = "name";
    /**
     * XML attribute of a {@link #XML_CARD}, this is a drawable resource id. This might be a null.
     */
    String XML_ICON = "icon";
    /**
     * XML attribute of a {@link #XML_CARD}, this is a unique (String) ID for each type of a report, stored in history and never shown directly to the user. This might be a null if this element has children.
     */
    String XML_ID = "id";
}

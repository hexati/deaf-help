package com.help.deaf.hexati.deafhelp.utils;

import android.text.TextUtils;

import java.util.ArrayList;



public class CommonOperations {
    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /**
     * Return report type as readable string (all but first path of report category).
     *
     * @param reportPath path of report - first item is category and is ignored
     * @param more       more items appended after arraylist ends
     * @return readable report type (eg. "Reakcja alergiczna - przytomy")
     */
    public static String getReadableReportString(ArrayList<String> reportPath, String... more) {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < reportPath.size(); i++) {
            sb.append(reportPath.get(i))
                    .append(" - ");
        }
        for (String aMore : more) {
            sb.append(aMore)
                    .append(" - ");
        }
        sb.delete(sb.length() - 3, sb.length() - 1);
        return sb.toString();
    }
}

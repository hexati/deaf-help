package com.help.deaf.hexati.deafhelp.ui.report;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.ui.MainActivity;
import com.help.deaf.hexati.deafhelp.ui.TabbedFragment;
import com.help.deaf.hexati.deafhelp.ui.conversation.ConversationFragment;
import com.help.deaf.hexati.deafhelp.ui.history.HistorySQL;
import com.help.deaf.hexati.deafhelp.ui.location.ReportLocationFragment;
import com.help.deaf.hexati.deafhelp.utils.CommonOperations;

import java.util.ArrayList;

/**
 * Fragment hosting clickable grid of cards for picking a report.
 */
public class ReportCategoryFragment extends TabbedFragment<MainActivity.TabHeader> {
    private final static String SAVE_PATH = "SAVE_PICKED_PATH";
    private RecyclerView mRecycler;
    /**
     * Path of selected categories. If empty then this is the root.
     */
    private ArrayList<String> mCategoryPath;

    private ReportCategoryAdapter mAdapter;
    private ArrayList<ReportCategory> categoryItems;
    //keep reference to header we will be updating
    private MainActivity.TabHeader header;

    public static ReportCategoryFragment newInstance() {
        return new ReportCategoryFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new ReportCategoryAdapter(this);
        mCategoryPath = new ArrayList<>();
        if (savedInstanceState == null) {
            long ongoingId = new HistorySQL(getContext()).getOngoingReportId();
            if (ongoingId > 0) {
                ConversationFragment cf = ConversationFragment.newInstance(ongoingId, false);
                addOverlayFragment(cf);
                Toast.makeText(getContext(), R.string.chat_continue, Toast.LENGTH_SHORT).show();
            }
        } else {
            mCategoryPath = savedInstanceState.getStringArrayList(SAVE_PATH);
        }
        header = new MainActivity.TabHeader(null, false);
        notifyCategoryPathChanged();
    }

    @Nullable
    @Override
    protected MainActivity.TabHeader onCreateHeader(LayoutInflater inflater, @Nullable Bundle savedInstanceState) {
        return header;  //we update headers content dynamically as user selects categories
    }

    @Override
    protected View onCreateContentView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_report, container, false);
        mRecycler = (RecyclerView) v.findViewById(R.id.recReportRecycler);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new GridLayoutManager(getContext(), getResources().getInteger(R.integer.categories_recycler_grid_column_count)));
        ((SimpleItemAnimator) mRecycler.getItemAnimator()).setSupportsChangeAnimations(false);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mRecycler = null;
    }

    @Override
    protected int getOverlayContainer() {
        return R.id.layReportOverlay;
    }

    @Override
    public boolean onBackPressed() {
        if (!super.onBackPressed()) {
            if (mCategoryPath.size() == 0) {
                return false;
            }
            mCategoryPath.remove(mCategoryPath.size() - 1);
            notifyCategoryPathChanged();
            notifyHeaderChanged();
        }
        return true;
    }

    /**
     * Report category was clicked
     *
     * @param position current position of category in the list
     */
    void onReportCategorySelected(int position) {
        ReportCategory category = categoryItems.get(position);
        if (category.hasChildren()) {
            //open children cards
            mCategoryPath.add(category.categoryName);
            notifyCategoryPathChanged();
            notifyHeaderChanged();
        } else if (!hasOverlayingFragment()) {
            //start the map
            ArrayList<String> path = new ArrayList<>(mCategoryPath);
            path.add(category.categoryName);
            Report r = new Report()
                    .setId(category.id)
                    .setCategory(path)
                    .setIcon(category.icon);
            ReportLocationFragment locationFragment = ReportLocationFragment.newInstance(r);
            addOverlayFragment(locationFragment);
        }
    }

    /**
     * Return to the categories root. Usually called after new report is submitted.
     */
    public void clearPickedCategory() {
        mCategoryPath.clear();
        notifyCategoryPathChanged();
        updateHeaderContent();
    }

    /**
     * Load category items for {@link #mCategoryPath}
     */
    private void notifyCategoryPathChanged() {
        ArrayList<ReportCategory> newItems = new XmlCategoryParser(getContext(), mCategoryPath)
                .parse()
                .getResult();

        if (mRecycler != null) {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtilCallback(newItems));
            categoryItems = newItems;
            mAdapter.setItems(categoryItems);
            diffResult.dispatchUpdatesTo(mAdapter);
        } else {
            categoryItems = newItems;
            mAdapter.setItems(categoryItems);
        }
        updateHeaderContent();
    }

    /**
     * Update headers content depending on {@link #mCategoryPath}
     */
    private void updateHeaderContent() {
        if (mCategoryPath.size() == 0) {
            header.setHeaderString(getString(R.string.new_raport));
            header.setBackShown(false);
        } else if (mCategoryPath.size() == 1) {
            header.setHeaderString(mCategoryPath.get(0));
            header.setBackShown(true);
        } else {
            header.setHeaderString(CommonOperations.getReadableReportString(mCategoryPath));
            header.setBackShown(true);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList(SAVE_PATH, mCategoryPath);
    }

    /**
     * Calculates the difference between new and old list
     */
    private class DiffUtilCallback extends DiffUtil.Callback {
        final ArrayList<ReportCategory> newItems;

        private DiffUtilCallback(ArrayList<ReportCategory> newItems) {
            this.newItems = newItems;
        }

        @Override
        public int getOldListSize() {
            return categoryItems.size();
        }

        @Override
        public int getNewListSize() {
            return newItems.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldItemPosition == newItemPosition;
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return false;
        }
    }

}

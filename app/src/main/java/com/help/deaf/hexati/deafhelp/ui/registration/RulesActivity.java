package com.help.deaf.hexati.deafhelp.ui.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.messages.SmsReceiver;
import com.help.deaf.hexati.deafhelp.ui.ChainedFormActivity;
import com.help.deaf.hexati.deafhelp.ui.MainActivity;
import com.help.deaf.hexati.deafhelp.ui.actions.UserRegistrationDataActions;



public class RulesActivity extends ChainedFormActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rules);

        Button tvAccept = (Button) findViewById(R.id.tv_accept);
        tvAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToNextActivityWithAction(UserDataActivity.class, UserRegistrationDataActions.ACTION_NEW_USER);
            }
        });

        Button btnDecline = (Button) findViewById(R.id.tv_decline);
        btnDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SmsReceiver.DEBUG){
                    Toast.makeText(RulesActivity.this, "DEBUG = TRUE, pomijam rejestrację", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(RulesActivity.this, MainActivity.class));
                    finish();
                }else
                    finish();
            }
        });
    }
}

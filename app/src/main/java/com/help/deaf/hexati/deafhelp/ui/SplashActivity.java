package com.help.deaf.hexati.deafhelp.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.help.deaf.hexati.deafhelp.ui.registration.RulesActivity;
import com.help.deaf.hexati.deafhelp.ui.registration.VerificationActivity;
import com.help.deaf.hexati.deafhelp.ui.security.SecurityActivity;
import com.help.deaf.hexati.deafhelp.utils.SharedPreferencesUtils;



public class SplashActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (SharedPreferencesUtils.getVerificationComplete(this)) {
            if (SharedPreferencesUtils.getPatternStatus(this)) {
                startActivity(new Intent(this, SecurityActivity.class));
            } else {
                startActivity(new Intent(this, MainActivity.class));
            }

        } else {
            if(SharedPreferencesUtils.getRegistrationComplete(this)) {
                startActivity(new Intent(this, VerificationActivity.class));
            } else {
                startActivity(new Intent(this, RulesActivity.class));
            }
        }
        finish();
    }
}

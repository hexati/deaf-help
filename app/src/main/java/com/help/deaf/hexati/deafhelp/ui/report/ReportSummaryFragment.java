package com.help.deaf.hexati.deafhelp.ui.report;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.ui.MainActivity;
import com.help.deaf.hexati.deafhelp.ui.TabbedFragment;
import com.help.deaf.hexati.deafhelp.utils.CommonOperations;

/**
 * Shows summary (category + address) of report, and shows extra input for details.
 */
public class ReportSummaryFragment extends TabbedFragment<MainActivity.TabHeader> {
    TextView txtCategory;
    TextView txtLocation;
    EditText etExtraDetails;

    private MainActivity activity;
    private Report report;

    /**
     * Summary for a report with details input
     *
     * @param report report with category, icon and location
     * @return new summary fragment
     */
    public static ReportSummaryFragment newInstance(Report report) {
        ReportSummaryFragment fragment = new ReportSummaryFragment();
        Bundle b = new Bundle(1);
        b.putParcelable(ReportWarningActivity.EXTRA_REPORT, report);
        fragment.setArguments(b);
        return fragment;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        report = getArguments().getParcelable(ReportWarningActivity.EXTRA_REPORT);
        activity.setReportSummaryFragment(this);
    }

    @Nullable
    @Override
    protected MainActivity.TabHeader onCreateHeader(LayoutInflater inflater, @Nullable Bundle savedInstanceState) {
        return new MainActivity.TabHeader(getString(R.string.raport_info), true);
    }

    @Override
    protected View onCreateContentView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_report_summary, container, false);
        txtCategory = (TextView) v.findViewById(R.id.txtReportSummaryCategory);
        txtLocation = (TextView) v.findViewById(R.id.txtReportSummaryLocation);
        etExtraDetails = (EditText) v.findViewById(R.id.etReportSummaryDetails);
        bindTextFromArgs();
        return v;
    }

    /**
     * Read fragment arguments and display them in text fields.
     */
    private void bindTextFromArgs() {
        if (report.getCategory() == null) {
            throw new IllegalArgumentException("ReportSummaryFragment must contain valid report!");
        }
        String category = CommonOperations.getReadableReportString(report.getCategory());


        txtCategory.setText(category);
        txtLocation.setText(report.getLocation());
    }

    @Override
    public boolean onBackPressed() {
        activity.setReportSummaryFragment(null);
        return super.onBackPressed();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        } else
            throw new ClassCastException("This can only be embedded in MainActivity");
    }

    /**
     * Called from activity when "done" floating action button is clicked.
     */
    public void onFabDoneClicked() {
        report.setDetails(etExtraDetails.getText().toString());
        Intent i = new Intent(getContext(), ReportWarningActivity.class);
        i.putExtra(ReportWarningActivity.EXTRA_REPORT, report);
        getActivity().startActivityForResult(i, ReportWarningActivity.REQUEST_CODE);
    }
}

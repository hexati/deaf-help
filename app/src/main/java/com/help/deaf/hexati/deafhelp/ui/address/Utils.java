package com.help.deaf.hexati.deafhelp.ui.address;

import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class Utils {
    public static boolean isZipCodeValid(String zipCode){
        String regex = "[0-9]{2}-[0-9]{3}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(zipCode);
        return matcher.matches();
    }


}

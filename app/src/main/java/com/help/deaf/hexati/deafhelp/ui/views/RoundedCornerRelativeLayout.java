package com.help.deaf.hexati.deafhelp.ui.views;



import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.RelativeLayout;

import com.help.deaf.hexati.deafhelp.R;


public class RoundedCornerRelativeLayout extends RelativeLayout {
    private final static float CORNER_RADIUS = 40.0f;

    private Bitmap maskBitmap;
    private float[] cornerRadius;
    private Path roundedPath = null;
    private int mOverlayColor;
    private Paint paint;

    public RoundedCornerRelativeLayout(Context context) {
        super(context);
        init(context, null, 0);
    }

    public RoundedCornerRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public RoundedCornerRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        int defaultOverlayColor = ContextCompat.getColor(getContext(), R.color.colorAccent);

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RoundedCornerRelativeLayout);
        float leftTopRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, a.getFloat(R.styleable.RoundedCornerRelativeLayout_leftTopRadius, 0), metrics);
        float leftBottomRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, a.getFloat(R.styleable.RoundedCornerRelativeLayout_leftBottomRadius, 0), metrics);
        float rightTopRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, a.getFloat(R.styleable.RoundedCornerRelativeLayout_rightTopRadius, 0), metrics);
        float rightBottomRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, a.getFloat(R.styleable.RoundedCornerRelativeLayout_rightBottomRadius, 0), metrics);
        this.setOverlayColor(a.getColor(R.styleable.RoundedCornerRelativeLayout_backgroundColor, defaultOverlayColor));


        cornerRadius = new float[]{leftTopRadius, leftTopRadius, rightTopRadius, rightTopRadius, rightBottomRadius, rightBottomRadius, leftBottomRadius, leftBottomRadius};

        setWillNotDraw(false);
        a.recycle();
    }

    public void setOverlayColor(int color) {
        this.mOverlayColor = color;
    }

    @Override
    public void onDraw(Canvas canvas) {

        if (roundedPath == null) {
            createMask(canvas.getWidth(), canvas.getHeight());
        }
        canvas.drawPath(roundedPath, paint);

        super.onDraw(canvas);
    }

    private void createMask(int width, int height) {
        RectF rect = (new RectF(0, 0, getWidth(), getHeight()));
        roundedPath = new Path();
        roundedPath.reset();
        roundedPath.addRoundRect(rect, cornerRadius, Path.Direction.CW);
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(mOverlayColor);
    }
}
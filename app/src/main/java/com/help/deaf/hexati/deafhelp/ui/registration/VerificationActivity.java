package com.help.deaf.hexati.deafhelp.ui.registration;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.messages.SmsReceiver;
import com.help.deaf.hexati.deafhelp.ui.address.AddressesActivity;
import com.help.deaf.hexati.deafhelp.ui.ChainedFormActivity;
import com.help.deaf.hexati.deafhelp.ui.views.TextViewWithFont;
import com.help.deaf.hexati.deafhelp.utils.PermissionUtils;
import com.help.deaf.hexati.deafhelp.utils.SharedPreferencesUtils;


public class VerificationActivity extends ChainedFormActivity {
    private static final int SMS_CODE_LENGTH = 4;
    private static final String VERIFICATION_USER_PREFIX = "Kod";
    private static final String EMAIL_SCHEME = "mailto";

    private EditText etSMSPassword;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.veryfication);
        initUI();
        PermissionUtils.requestReceiveSmsPermission(this);
    }

    @Override
    protected SmsReceiver onCreateSmsReceiver() {

        return SmsReceiver.createAndRegisterReceiveInstance(this, new SmsReceiver.SmsListener.Adapter() {
            @Override
            public void onMessageReceived(String messageText) {
                if(etSMSPassword!=null) {
                    etSMSPassword.setText(getCodeFromSms(messageText));
                }
            }
        });
    }

    private String getCodeFromSms(String messageText) {
        if(messageText.startsWith(VERIFICATION_USER_PREFIX)){
            return messageText.substring(messageText.length() - SMS_CODE_LENGTH, messageText.length());
        }
        return "";
    }

    private void initUI() {
        final String emailAddress = getResources().getString(R.string.email_address);


        etSMSPassword = (EditText) findViewById(R.id.etSMSPassword);

        TextViewWithFont tvEmailAddress = (TextViewWithFont) findViewById(R.id.tvEmailAddress);
        tvEmailAddress.setText(emailAddress);
        tvEmailAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        EMAIL_SCHEME, emailAddress, null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.email_title));
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(emailIntent, getString(R.string.send_email)));
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.floatingNext);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, getResources().getString(R.string.sending_verification), Snackbar.LENGTH_LONG)
                        .show();
                verifySMSPassword();
            }
        });
    }

    private void verifySMSPassword() {
        String userPassword = etSMSPassword.getText().toString();
        if (userPassword.equals(getString(R.string.verification_code)))
            passwordAccepted();
        else
            wrongPassword();
    }

    private void passwordAccepted() {
        SharedPreferencesUtils.saveVerificationComplete(this, true);
        finishRegistrationChain();
        startActivity(new Intent(this, AddressesActivity.class));
    }

    private void wrongPassword() {
        new MaterialDialog.Builder(this)
                .title(R.string.error)
                .content(R.string.verification_wrongPassword)
                .positiveText(R.string.ok)
                .show();
    }


}

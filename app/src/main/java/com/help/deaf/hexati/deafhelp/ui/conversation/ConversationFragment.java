package com.help.deaf.hexati.deafhelp.ui.conversation;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Telephony;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.help.deaf.hexati.deafhelp.R;
import com.help.deaf.hexati.deafhelp.messages.SmsSender;
import com.help.deaf.hexati.deafhelp.ui.MainActivity;
import com.help.deaf.hexati.deafhelp.ui.TabbedFragment;
import com.help.deaf.hexati.deafhelp.ui.history.HistoryEntry;
import com.help.deaf.hexati.deafhelp.ui.history.HistorySQL;
import com.help.deaf.hexati.deafhelp.utils.PermissionUtils;

import java.util.ArrayList;

/**
 * Fragment hosting ongoing conversation or from history. Uses content provider to read and live update displayed messages.
 */
public class ConversationFragment extends TabbedFragment<MainActivity.TabHeader> {
    private static final String ARG_CONVERSATION_ID = "ARG_CONVERSATION_ID";
    private static final String ARG_ISREADONLY = "ARG_ISREADONLY";
    /**
     * Time period for updating time in each conversation entry.
     */
    private final long UPDATE_TIMER_PERIOD_MS = 5000;

    /**
     * Columns read from {@link Telephony.Sms#CONTENT_URI}.
     */
    private final static String[] CONTENT_PROVIDER_PROJECTION = new String[]{Telephony.Sms._ID, Telephony.Sms.DATE, Telephony.Sms.TYPE, Telephony.Sms.BODY};

    /**
     * Flag that enables ongoing conversation with help center - if <code>false</code> user can only receive messages and finish ongoing report.
     */
    private final static boolean ONGOING_CONVERSATION_ENABLED = false;

    /**
     * Casted activity this fragment is in.
     */
    private MainActivity activity;

    /**
     * History entry this conversation is operating on. Can be missing endId.
     */
    private HistoryEntry historyEntry;
    /**
     * If set this is conversation from history and is read-only
     */
    private boolean isReadOnly;
    /**
     * If set then user wants to finish ongoing conversation and next call to {@link #onBackPressed()} will close it.
     */
    private boolean mIsCloseOngoingAccepted;

    /**
     * Show conversation between user and help
     *
     * @param conversationId ID of report conversation is about
     * @param readOnly       true if this is old report and no more input will come
     * @return new fragment
     */
    public static ConversationFragment newInstance(long conversationId, boolean readOnly) {
        Bundle args = new Bundle();
        args.putLong(ARG_CONVERSATION_ID, conversationId);
        args.putBoolean(ARG_ISREADONLY, readOnly);

        ConversationFragment fragment = new ConversationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private RecyclerView recRecycler;

    private ArrayList<ConversationEntry> items;
    private ConversationEntryAdapter mAdapter;
    private Handler mTimeUpdater;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        historyEntry = new HistorySQL(getContext()).getOne(getArguments().getLong(ARG_CONVERSATION_ID));
        if (historyEntry == null) {
            Log.e("Conversation", "onCreate: no history entry, did new report forget to insert it?");
        }
        isReadOnly = getArguments().getBoolean(ARG_ISREADONLY);

        mAdapter = new ConversationEntryAdapter();
        items = new ArrayList<>();
        mAdapter.setItems(items);
        mTimeUpdater = new Handler();
        mTimeUpdater.postDelayed(mUpdateTimeRunnable, UPDATE_TIMER_PERIOD_MS);

        if (!PermissionUtils.checkReadSmsPermission(getActivity())) {
            items.add(new ConversationEntry(0, ConversationEntry.TYPE_SYSTEM, getString(R.string.no_read_sms_permission)));
        } else {
            //start cursor loader using conversationID to load the SMS history...
            if (isReadOnly && historyEntry.getEndTime() > 0) {
                getLoaderManager().initLoader(0, null, new HistoryDataLoader());
            } else {
                getLoaderManager().initLoader(0, null, new OngoingDataLoader());
            }
            //if this is not read only (not from history) and settings flag is raised, enable conversation
            if (!isReadOnly && ONGOING_CONVERSATION_ENABLED)
                activity.setOngoingConversationFragment(this);
        }
    }

    @Nullable
    @Override
    protected MainActivity.TabHeader onCreateHeader(LayoutInflater inflater, @Nullable Bundle savedInstanceState) {
        return new MainActivity.TabHeader(getString(isReadOnly ? R.string.history_title : R.string.chat_title), true);
    }

    @Nullable
    @Override
    protected View onCreateContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_conversation, container, false);
        recRecycler = (RecyclerView) v.findViewById(R.id.recConversationRecycler);
        recRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        recRecycler.setAdapter(mAdapter);
        MainActivity.attachRecyclerToSharedRecyclePool(recRecycler);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        recRecycler = null;
    }

    /**
     * Whenever message should be added to the recycler view (send out or received SMS)
     *
     * @param entry entry to add and show
     */
    public void onTextReceived(ConversationEntry entry) {
        items.add(entry);

        //fragment has no view alive, ignore the call
        if (recRecycler == null)
            return;
        mAdapter.notifyItemInserted(items.size());
        if (!isReadOnly) {
            recRecycler.smoothScrollToPosition(items.size() - 1);
            //ugly cast, let's do it fast
            ((MainActivity) getActivity()).onNewMessageReceived();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimeUpdater.removeCallbacks(mUpdateTimeRunnable);
        mTimeUpdater = null;
    }

    @Override
    public boolean onBackPressed() {
        if (!isReadOnly && !mIsCloseOngoingAccepted) {
            //show finish dialog?
            new MaterialDialog.Builder(getContext())
                    .content(R.string.finish_chat)
                    .positiveText(android.R.string.ok)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            onAcceptCloseOngoingConversation();
                        }
                    })
                    .negativeText(android.R.string.cancel)
                    .show();
            return true;
        }
        //this fragment will get detached soon, remove it from activity
        if (!isReadOnly)
            activity.setOngoingConversationFragment(null);
        return super.onBackPressed();
    }

    /**
     * User pressed OK button in warning dialog, finish ongoing conversation.
     */
    void onAcceptCloseOngoingConversation() {
        new HistorySQL(getContext()).finishOngoingReport(System.currentTimeMillis());

        //send terminate help message and ignore the result
        SmsSender.sendTerminateHelpConversationMessage(getContext());

        //close this fragment
        mIsCloseOngoingAccepted = true;
        ((TabbedFragment) getParentFragment()).onBackPressed();
    }

    /**
     * See if user can write in this conversation
     *
     * @return true if he can, false if this is history view
     */
    public boolean isReadOnly() {
        return isReadOnly;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        } else
            throw new ClassCastException("This can only be embedded in MainActivity");
    }

    /**
     * Notifies adapter to update time on all viewholders.
     */
    private final Runnable mUpdateTimeRunnable = new Runnable() {
        @Override
        public void run() {
            if (mAdapter != null)
                mAdapter.notifyTimeUpdate();
            if (mTimeUpdater != null)
                mTimeUpdater.postDelayed(this, UPDATE_TIMER_PERIOD_MS);
        }
    };

    /**
     * Abstract for loading sms data from content provider. Keep projection order in check for valid loadFinished read.
     */
    private abstract class DataLoader implements LoaderManager.LoaderCallbacks<Cursor> {
        private Cursor c;

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            c = data;
            c.moveToPosition(items.size() - 1);

            while (c.moveToNext()) {
                long id = c.getLong(0);
                int smsType = resolveSmsType(c.getInt(2));
                onTextReceived(new ConversationEntry(id, c.getLong(1), smsType, c.getString(3)));
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            if (c != null)
                c.close();
        }

        /**
         * Convert type read from content provider into type for conversation entry.
         *
         * @param type type read from content
         * @return type for conversation entry
         */
        int resolveSmsType(int type) {
            switch (type) {
                case Telephony.Sms.MESSAGE_TYPE_SENT:
                    return ConversationEntry.TYPE_USER;
                case Telephony.Sms.MESSAGE_TYPE_INBOX:
                    return ConversationEntry.TYPE_HELP;
                default:
                    return ConversationEntry.TYPE_SYSTEM;
            }
        }
    }

    /**
     * Data loader listening for ongoing conversation.
     */
    private class OngoingDataLoader extends DataLoader {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(getContext(),
                    Telephony.Sms.CONTENT_URI,
                    CONTENT_PROVIDER_PROJECTION,
                    Telephony.Sms.ADDRESS + " =? AND " + Telephony.Sms.DATE + " >=?",
                    new String[]{getString(R.string.emergency_number), String.valueOf(historyEntry.getTime())},
                    Telephony.Sms.DATE + " ASC");
        }
    }

    /**
     * Data loader listening for ongoing conversation.
     */
    private class HistoryDataLoader extends DataLoader {
        boolean addedEndFooter = false;

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(getContext(),
                    Telephony.Sms.CONTENT_URI,
                    CONTENT_PROVIDER_PROJECTION,
                    Telephony.Sms.ADDRESS + " =? AND " + Telephony.Sms.DATE + " >=? AND " + Telephony.Sms.DATE + " <=?",
                    new String[]{getString(R.string.emergency_number), String.valueOf(historyEntry.getTime()), String.valueOf(historyEntry.getEndTime())},
                    Telephony.Sms.DATE + " ASC");
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            super.onLoadFinished(loader, data);
            if (!addedEndFooter) {
                if (items.size() == 0)
                    onTextReceived(new ConversationEntry(-1, 0, ConversationEntry.TYPE_SYSTEM, getString(R.string.chat_end_empty)));
                onTextReceived(new ConversationEntry(-2, historyEntry.getEndTime(), ConversationEntry.TYPE_SYSTEM, getString(R.string.chat_end)));
                addedEndFooter = true;
            }
        }
    }
}
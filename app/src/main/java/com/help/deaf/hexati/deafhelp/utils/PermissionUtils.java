package com.help.deaf.hexati.deafhelp.utils;

import android.Manifest;
import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Class with static method used to check and gather permissions needed by application
 */

public class PermissionUtils {

    private static String TAG = "PermissionUtils";
    public static final int PERMISSIONS_REQUEST_LOCATION = 765;
    public static final int PERMISSIONS_REQUEST_SMS_RECEIVE = 7652;
    public static final int PERMISSIONS_REQUEST_SMS_SEND = 7653;

    /**
     * Request permission for receiving sms only when permission is not granted
     * @param activity activity to check permission for
     */
    public static void requestReceiveSmsPermission(Activity activity) {
        if(!checkReceiveSmsPermission(activity)) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.RECEIVE_SMS},
                    PERMISSIONS_REQUEST_SMS_RECEIVE);
        }
    }

    public static void requestSendSmsPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                                          new String[]{Manifest.permission.SEND_SMS},
                PERMISSIONS_REQUEST_SMS_SEND);
    }

    public static boolean checkSendSmsPermission(Activity activity) {
        int permissionCheck = ContextCompat.checkSelfPermission(activity,
                                                                Manifest.permission.SEND_SMS);

        if (permissionCheck == android.content.pm.PackageManager.PERMISSION_GRANTED) return true;
        else {
            requestSendSmsPermission(activity);
            return false;
        }
    }

    /**
     * Check sms read permission without requesting it.
     * @param activity activity to check permission for
     * @return true if we can read sms, otherwise false
     */
    public static boolean checkReadSmsPermission(Activity activity) {
        int permissionCheck = ContextCompat.checkSelfPermission(activity,
                                                                Manifest.permission.READ_SMS);
        return permissionCheck == android.content.pm.PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Check sms recive permission without requesting it.
     * @param activity activity to check permission for
     * @return true if we can read sms, otherwise false
     */
    public static boolean checkReceiveSmsPermission(Activity activity) {
        int permissionCheck = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.RECEIVE_SMS);
        return permissionCheck == android.content.pm.PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Method requests for permission used to track user location
     * @param activity activity to request permission for
     */
    public static void requestGPSPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSIONS_REQUEST_LOCATION);
    }

    /**
     * Check GPS location permission without requesting it.
     * @param activity activity to check permission for
     * @return true if we can access location, otherwise false
     */
    public static boolean checkGpsPermission(Activity activity) {
        int permissionCheck = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionCheck == android.content.pm.PackageManager.PERMISSION_GRANTED;
    }
}

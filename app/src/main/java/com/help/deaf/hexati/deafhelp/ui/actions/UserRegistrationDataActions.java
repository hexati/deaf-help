package com.help.deaf.hexati.deafhelp.ui.actions;


public class UserRegistrationDataActions {
    public static final String ACTION_NEW_USER = UserRegistrationDataActions.class.getSimpleName() + ".new_user";
    public static final String ACTION_CHANGE_USER_DATA = UserRegistrationDataActions.class.getSimpleName() + ".change_user_data";
}

package com.help.deaf.hexati.deafhelp.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.help.deaf.hexati.deafhelp.R;



public class EditTextWithFont extends android.support.v7.widget.AppCompatEditText {
    private Typeface typeface;

    public EditTextWithFont(Context context) {
        super(context);
        init(context, null);
    }

    public EditTextWithFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public EditTextWithFont(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.RobotoTextView,
                0, 0);
        int font = a.getInt(R.styleable.RobotoTextView_font, 0);
        a.recycle();
        switch (font) {
            case 0: //Roboto Thin
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoThin.ttf");
                break;
            case 1:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoLight.ttf");
                break;
            case 2:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensedLight.ttf");
                break;
            case 3:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoRegular.ttf");
                break;
            case 4:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoMedium.ttf");
                break;
            case 5:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/hans.otf");
                break;
            case 6:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Befolk.ttf");
                break;
            case 7:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Thin.ttf");
                break;
            case 8:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
                break;
            case 9:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Medium.ttf");
                break;
            case 10:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Bold.ttf");
                break;
            case 11:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Black.ttf");
                break;
            case 12:
                typeface = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoBold.ttf");
                break;

        }
        if (typeface != null) {
            setTypeface(typeface);

        }
    }
}

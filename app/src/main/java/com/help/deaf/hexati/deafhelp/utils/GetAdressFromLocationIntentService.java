package com.help.deaf.hexati.deafhelp.utils;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.os.ResultReceiver;
//import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.help.deaf.hexati.deafhelp.R;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Class GetAddressFromLocationIntentService send request to reverse geocode API provided by Google
 * in background thread.
 */

public class GetAdressFromLocationIntentService extends IntentService{
    private LatLng latLng;
    private static final String SERVICE_NAME = ".utils.GetAdressFromLocationIntentService";
    private static final String TAG = "get_address_service";
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String RESULT_DATA_KEY = "RESULT_KEY";
    public static final String STREET_DATA_KEY = "STREET_KEY";
    public static final String NUMBER_DATA_KEY = "NUMBER_KEY";
    public static final String CITY_DATA_KEY = "CITY_KEY";
    public static final String POSTALCODE_DATA_KEY = "POSTAL_KEY";
    public static final String LATLNG_EXTRA_KEY = "LATLNG_DATA_EXTRA";
    public static final String RECIVER_KEY = "ADDRESS_RECEIVER_KEY";

    protected ResultReceiver mReceiver;


    public GetAdressFromLocationIntentService() {
        super(SERVICE_NAME);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String result = "";

        latLng = intent.getParcelableExtra(LATLNG_EXTRA_KEY);
        mReceiver = intent.getParcelableExtra(RECIVER_KEY);

        // Class Address from Android API not application!
        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        } catch (IOException ioException) {
            result = getString(R.string.geocoder_not_availabe);
//            Log.e(TAG, result);
        } catch (IllegalArgumentException illegalArgumentException) {

            result = getString(R.string.lat_lng_incorrect);
        }

        if (addresses == null || addresses.size() == 0) {
            result += getString(R.string.geocoder_not_availabe);
            returnResultAddressLine(FAILURE_RESULT, result, "", "", "", "");
        } else {
            Address address = addresses.get(0);

            String street = address.getThoroughfare();
            String number = address.getSubThoroughfare();
            String nrLokalu = address.getPremises();
            if (nrLokalu != null && !nrLokalu.isEmpty())
                number += "/" + nrLokalu;

            String city = address.getLocality();
            String dzielnica = address.getSubLocality();
            if (dzielnica != null && !dzielnica.isEmpty())
                city += " " + dzielnica;

            String postalCode = address.getPostalCode();

            returnResultAddressLine(SUCCESS_RESULT, result, street, number, city, postalCode);
        }
    }

    @SuppressWarnings("RestrictedApi")
    private void returnResultAddressLine(int resultCode, String result, String street, String number, String city, String postalCode) {
        Bundle bundle = new Bundle();
        bundle.putString(RESULT_DATA_KEY, result);
        bundle.putString(STREET_DATA_KEY, street);
        bundle.putString(NUMBER_DATA_KEY, number);
        bundle.putString(CITY_DATA_KEY, city);
        bundle.putString(POSTALCODE_DATA_KEY, postalCode);
        bundle.putParcelable(LATLNG_EXTRA_KEY, latLng);
        mReceiver.send(resultCode, bundle);
    }

}

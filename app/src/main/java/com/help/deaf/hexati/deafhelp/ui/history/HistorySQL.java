package com.help.deaf.hexati.deafhelp.ui.history;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Stores report history.
 */
public class HistorySQL extends SQLiteOpenHelper {
    private static final String DB_NAME = "History.db";
    private static final int VERSION = 3;

    public HistorySQL(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    /**
     * Read entire history (from newest to oldest), excluding ongoing conversation.
     *
     * @return full history of reports
     */
    ArrayList<HistoryEntry> getAll() {
        ArrayList<HistoryEntry> historyEntries = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(Table.NAME, null, null, null, null, null, Table.COLUMN_TIME + " DESC");
        try {
            while (c.moveToNext()) {
                if (c.isNull(2)) {
                    //if end time is not set conversation is still ongoing.
                    continue;
                }
                HistoryEntry historyEntry = new HistoryEntry(c.getLong(0),
                        c.getString(1),
                        c.getLong(2));
                historyEntries.add(historyEntry);
            }
        } finally {
            c.close();
            db.close();
        }
        return historyEntries;
    }

    /**
     * Read one history entry
     *
     * @param id id of history
     * @return history entry with given ID or null if missing,
     */
    public HistoryEntry getOne(long id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query(Table.NAME, null, Table.COLUMN_TIME + " >= ?", new String[]{String.valueOf(id)}, null, null, Table.COLUMN_TIME + " ASC", "1");
        HistoryEntry entry = null;
        try {
            //we're only going to read 1 element
            if (c.moveToFirst()) {
                long endId;
                if (c.isNull(2)) {
                    //if end time is not set conversation is still ongoing.
                    endId = 0;
                } else
                    endId = c.getLong(2);
                entry = new HistoryEntry(c.getLong(0), c.getString(1), endId);
            }
        } finally {
            c.close();
            db.close();
        }

        return entry;
    }

    /**
     * Read the id (timestamp) of last entry in the history.
     *
     * @return id of ongoing conversation or 0 if database is empty or conversation was terminated
     */
    public long getOngoingReportId() {
        try (SQLiteDatabase db = getReadableDatabase()) {
            return internalGetOngoingReportId(db);
        }
    }

    /**
     * Read the id (timestamp) of last entry in the history.
     *
     * @return id of ongoing conversation or 0 if database is empty or conversation was terminated
     */
    private long internalGetOngoingReportId(SQLiteDatabase db) {
        try (Cursor c = db.query(Table.NAME,
                null,
                null,
                null,
                null,
                null,
                Table.COLUMN_TIME + " DESC", "1")) {
            if (c.moveToFirst()) {
                if (c.isNull(2))
                    return c.getLong(0);
            }
            return 0;
        }
    }

    /**
     * Put timestamp as end of the ongoing (latest) conversation.
     *
     * @param terminationTimestamp value to put as the end of conversation
     * @return true if update succeeded
     */
    public boolean finishOngoingReport(long terminationTimestamp) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.beginTransaction();
            long ongoingId = internalGetOngoingReportId(db);
            if (ongoingId > 0) {
                ContentValues idUpdate = new ContentValues(1);
                idUpdate.put(Table.COLUMN_END_TIME, terminationTimestamp);
                String[] idUpdateArg = new String[]{String.valueOf(ongoingId)};
                int updatedRows = db.update(Table.NAME, idUpdate, Table.COLUMN_TIME + " = ?", idUpdateArg);
                if (updatedRows > 0)
                    db.setTransactionSuccessful();
                return updatedRows > 0;
            }
        } finally {
            db.endTransaction();
            db.close();
        }
        return false;
    }

    /**
     * Store new report in the history. This also puts this sms id-1 as end of previous report if it was not set.
     *
     * @param reportType         report type
     * @param newReportTimestamp time of the report
     * @return true if insert succeeded, false if something went wrong
     */
    public boolean saveNewReport(String reportType, long newReportTimestamp) {
        SQLiteDatabase db = getWritableDatabase();
        String[] idProjection = new String[]{Table.COLUMN_TIME, Table.COLUMN_END_TIME};
        Cursor c = db.query(Table.NAME, idProjection, null, null, null, null, Table.COLUMN_TIME + " DESC", "1");
        try {
            db.beginTransaction();

            //first see if last item in db has end time. If not then set it
            if (c.moveToFirst()) {
                if (c.isNull(2)) {
                    ContentValues idUpdate = new ContentValues(1);
                    idUpdate.put(Table.COLUMN_END_TIME, newReportTimestamp - 1);
                    String[] idUpdateArg = new String[]{c.getString(0)};
                    db.update(Table.NAME, idUpdate, Table.COLUMN_TIME + " = ?", idUpdateArg);
                }
            }//else this is very first report in history

            ContentValues insertValues = new ContentValues(2);
            insertValues.put(Table.COLUMN_TIME, newReportTimestamp);
            insertValues.put(Table.COLUMN_TYPE, reportType);
            db.insert(Table.NAME, null, insertValues);
            db.setTransactionSuccessful();
        } finally {
            c.close();
            db.endTransaction();
            db.close();
        }
        return true;
    }

    /**
     * Clear entire database (drop and recreate empty table).
     * It will not be recoverable, use this only when user is deleting his account.
     */
    public void clearDatabase() {
        SQLiteDatabase db = getWritableDatabase();
        clearAndCreateDatabase(db);
        db.close();
    }

    private void clearAndCreateDatabase(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + Table.NAME);
        onCreate(db);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(Table.CREATE_COMMAND);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        clearAndCreateDatabase(sqLiteDatabase);
    }

    private abstract static class Table {
        private final static String NAME = "HISTORY_TABLE";
        /**
         * Time also serves as an ID.
         */
        private final static String COLUMN_TIME = "TIME";
        /**
         * Type of user report.
         */
        private final static String COLUMN_TYPE = "TYPE";
        /**
         * Might be null until next record is inserted.
         */
        private final static String COLUMN_END_TIME = "END_TIME";

        private final static String CREATE_COMMAND =
                "CREATE TABLE " + NAME + " ( " +
                        COLUMN_TIME + " integer primary key not null, " +
                        COLUMN_TYPE + " text not null, " +
                        COLUMN_END_TIME + " integer)";
    }
}

package com.help.deaf.hexati.deafhelp.ui.conversation;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for conversation.
 */
class ConversationEntryAdapter extends RecyclerView.Adapter<ConversationEntry.ConversationEntryViewHolder> {
    private ArrayList<ConversationEntry> items;

    ConversationEntryAdapter() {
        setHasStableIds(true);
    }

    /**
     * Change list of items shown.
     * @param items new list
     */
    public void setItems(ArrayList<ConversationEntry> items) {
        this.items = items;
    }

    /**
     * Notify all adapters to update the time (relative to now)
     */
    public void notifyTimeUpdate() {
        notifyItemRangeChanged(0, getItemCount(), ConversationEntry.Payload.Time);
    }

    @Override
    public ConversationEntry.ConversationEntryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ConversationEntry.ConversationEntryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ConversationEntry.ConversationEntryViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public void onBindViewHolder(ConversationEntry.ConversationEntryViewHolder holder, int position, List<Object> payloads) {
        if (payloads.size() != 1) {
            onBindViewHolder(holder, position);
        } else  //only partial update if 1 payload item exists
            holder.update(items.get(position), payloads.get(0));
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).get_ID();
    }
}

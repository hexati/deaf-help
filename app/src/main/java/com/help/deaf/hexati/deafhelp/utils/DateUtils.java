package com.help.deaf.hexati.deafhelp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class DateUtils {
    private static final String DATE_FORMAT = "dd.MM.yyyy";
    /**
     * Minimum user age which can use application
     */
    public static final int MIN_USER_AGE = 7;

    /**
     * Format values from DatePicker to date
     *
     * @param year  - year
     * @param month - month of year (January = 0)
     * @param day   - day of month
     * @return date in specific format (dd.MM.yyyy)
     */
    public static String formatToDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

    public static boolean checkBirthDate(String date, int userAge) {
        Calendar currentDateCalendar = Calendar.getInstance();
        Calendar userBirthDateCalendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        userBirthDateCalendar.setTime(convertedDate);
        int age = currentDateCalendar.get(Calendar.YEAR) - userBirthDateCalendar.get(Calendar.YEAR);

        if (currentDateCalendar.get(Calendar.DAY_OF_YEAR) < userBirthDateCalendar.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        return age >= userAge;

    }
}

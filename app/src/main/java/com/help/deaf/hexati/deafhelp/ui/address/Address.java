package com.help.deaf.hexati.deafhelp.ui.address;


import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.help.deaf.hexati.deafhelp.R;


public class Address {
    public enum AddressIcon {
        Home,
        Work,
        Other,
        MapLocation
    }

    private String name;
    private String street;
    private String number;
    private String city;
    private String postCode;
    private AddressIcon icon;
    private Long dbId;
    public Address(String name, String addressLine, AddressIcon icon) {
        this.name = name;
        this.street = addressLine;
        this.icon = icon;
    }
    public Address(String name, String street, String number, String city, String postCode, AddressIcon icon) {
        this.name = name;
        this.street = street;
        this.number = number;
        this.city = city;
        this.postCode = postCode;
        this.icon = icon;
    }
    public Address(Long id, String name, String street, String number, String city, String postCode, int type) {
        this.dbId = id;
        this.name = name;
        this.street = street;
        this.number = number;
        this.city = city;
        this.postCode = postCode;


        switch (type) {
            case 1:
                this.icon = AddressIcon.Home;
                return;

            case 2:
                this.icon = AddressIcon.Work;
                return;

            default:
                this.icon = AddressIcon.Other;
        }
    }

    public void setTypeInt(int type) {
        switch (type) {
            case 1:
                this.icon = AddressIcon.Home;
                return;

            case 2:
                this.icon = AddressIcon.Work;
                return;

            default:
                this.icon = AddressIcon.Other;
        }
    }

    public void setIcon(AddressIcon icon) {
        this.icon = icon;
    }

    public Address.AddressIcon getIcon() {
        return this.icon;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return (street != null) ? street : "";
    }


    public String getNumber() {
        return (number !=null) ? number : "";
    }

    public String getCity() {
        return (city!=null) ? city : "";
    }

    public String getPostCode() {
        return (postCode!=null) ? postCode : "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getAddressLine(){
        String ret = (street != null && !street.isEmpty()) ? street : "";

        if (number != null && !number.isEmpty())
            ret += " " + number;

        if (city != null && !city.isEmpty()) {
            if(postCode != null && !postCode.isEmpty())
                ret += ", " + postCode + " " + city;
            else
                ret += ", " + city;
        } else if (postCode != null && !postCode.isEmpty()) {
            ret += ", " + postCode;
        }

        return ret;
    }

    public void setId(Long id) {
        this.dbId = id;
    }
    public Long getId() {
        return this.dbId;
    }
    public int getTypeInt() {
        switch (this.icon) {
            case Home:
                return 1;

            case Work:
                return 2;

            default:
                return 0;
        }
    }

    public int getIconColor(Context context) {
        switch (this.icon) {
            case Home:
                return ContextCompat.getColor(context, R.color.green);
            case Work:
                return ContextCompat.getColor(context, R.color.blue);

            case Other:
            case MapLocation:
            default:
                 return ContextCompat.getColor(context, R.color.gray);
        }

    }

    public int getIconImageId() {
        switch (this.icon) {
            case Home:
                return R.drawable.loc_home;
            case Work:
                return R.drawable.loc_work;
            case MapLocation:
                return R.drawable.loc_pin;

            default:
                return R.drawable.loc_a;
        }
    }
}

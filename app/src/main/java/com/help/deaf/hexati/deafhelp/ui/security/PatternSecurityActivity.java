package com.help.deaf.hexati.deafhelp.ui.security;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.help.deaf.hexati.deafhelp.R;

import java.util.List;

public abstract class PatternSecurityActivity extends AppCompatActivity implements PatternLockViewListener {


    private final String TAG = PatternSecurityActivity.class.getSimpleName();
    private PatternLockView patternLockView;



    private TextView helpText;
    private TextView messageText;
    private Button forgottenPasswordButton;
    private ImageView patternLockIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security);

        patternLockIcon = (ImageView) findViewById(R.id.pattern_lock_icon);
        forgottenPasswordButton = (Button) findViewById(R.id.pattern_lock_forgotten_password);
        patternLockView = (PatternLockView) findViewById(R.id.pattern_lock_view);
        helpText = (TextView) findViewById(R.id.pattern_lock_help_text);
        messageText = (TextView) findViewById(R.id.pattern_lock_help_message_text);
        patternLockView.addPatternLockListener(this);

    }

    @Override
    public void onStarted() {

    }

    @Override
    public void onProgress(List<PatternLockView.Dot> progressPattern) {

    }

    @Override
    public void onComplete(List<PatternLockView.Dot> pattern) {
        patternLockView.postDelayed(new Runnable() {
            @Override
            public void run() {
                patternLockView.clearPattern();
            }
        },200);

    }


    @Override
    public void onCleared() {

    }


    public PatternLockView getPatternLockView(){
        return patternLockView;
    }

    public TextView getHelpText() {
        return helpText;
    }

    public TextView getMessageText() {
        return messageText;
    }

    public Button getForgottenPasswordButton() {return forgottenPasswordButton;}

    public ImageView getPatternLockIcon() {
        return patternLockIcon;
    }
}

package com.help.deaf.hexati.deafhelp.ui.address;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;



public class AddressDBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "AddressDB.db";
    private static final String ADDRESS_TABLE_NAME = "addresses";
    private static final String ADDRESS_COLUMN_ID = "id";
    private static final String ADDRESS_COLUMN_NAME = "name";
    private static final String ADDRESS_COLUMN_ADDRESS_STREET = "street";
    private static final String ADDRESS_COLUMN_ADDRESS_NUMBER = "h_number";
    private static final String ADDRESS_COLUMN_ADDRESS_CITY = "city";
    private static final String ADDRESS_COLUMN_ADDRESS_POST_CODE = "post_code";
    private static final String ADDRESS_TYPE = "type";

    public AddressDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + ADDRESS_TABLE_NAME
                + " (" + ADDRESS_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + ADDRESS_COLUMN_NAME + " TEXT NOT NULL, "
                + ADDRESS_COLUMN_ADDRESS_STREET + " TEXT NOT NULL, "
                + ADDRESS_COLUMN_ADDRESS_NUMBER + " TEXT NOT NULL, "
                + ADDRESS_COLUMN_ADDRESS_CITY + " TEXT NOT NULL, "
                + ADDRESS_COLUMN_ADDRESS_POST_CODE + " TEXT NOT NULL, "
                + ADDRESS_TYPE + " INTEGER DEFAULT 0)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ADDRESS_TABLE_NAME);
        onCreate(db);
    }

    public boolean insertAddress(Address address) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ADDRESS_COLUMN_NAME, address.getName());
        contentValues.put(ADDRESS_COLUMN_ADDRESS_STREET, address.getStreet());
        contentValues.put(ADDRESS_COLUMN_ADDRESS_NUMBER, address.getNumber());
        contentValues.put(ADDRESS_COLUMN_ADDRESS_CITY, address.getCity());
        contentValues.put(ADDRESS_COLUMN_ADDRESS_POST_CODE, address.getPostCode());
        contentValues.put(ADDRESS_TYPE, address.getTypeInt());
        Long id = db.insert(ADDRESS_TABLE_NAME, null, contentValues);
        address.setId(id);

        if (db.isOpen()) {
            db.close();
        }
        return true;
    }

    public Long numberOfAddresses() {
        SQLiteDatabase db = this.getReadableDatabase();
        Long numRows = DatabaseUtils.queryNumEntries(db, ADDRESS_TABLE_NAME);
        if (db.isOpen()) {
            db.close();
        }
        return numRows;
    }

    public void deleteAddress(Address address) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(ADDRESS_TABLE_NAME,
                ADDRESS_COLUMN_ID + " = ? ",
                new String[]{Long.toString(address.getId())});
        if (db.isOpen()) {
            db.close();
        }
    }

    public boolean updateAddress(Address address) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ADDRESS_COLUMN_NAME, address.getName());
        contentValues.put(ADDRESS_COLUMN_ADDRESS_STREET, address.getStreet());
        contentValues.put(ADDRESS_COLUMN_ADDRESS_NUMBER, address.getNumber());
        contentValues.put(ADDRESS_COLUMN_ADDRESS_CITY, address.getCity());
        contentValues.put(ADDRESS_COLUMN_ADDRESS_POST_CODE, address.getPostCode());
        contentValues.put(ADDRESS_TYPE, address.getTypeInt());
        db.update(ADDRESS_TABLE_NAME, contentValues, "id = ? ", new String[]{Long.toString(address.getId())});

        if (db.isOpen()) {
            db.close();
        }
        return true;
    }

    public ArrayList<Address> loadAllAddresses() {
        ArrayList<Address> addresses = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + ADDRESS_TABLE_NAME, null);
        res.moveToFirst();


        int indexID = res.getColumnIndex(ADDRESS_COLUMN_ID);
        int indexName = res.getColumnIndex(ADDRESS_COLUMN_NAME);
        int indexStreetName = res.getColumnIndex(ADDRESS_COLUMN_ADDRESS_STREET);
        int indexNumberName = res.getColumnIndex(ADDRESS_COLUMN_ADDRESS_NUMBER);
        int indexCityName = res.getColumnIndex(ADDRESS_COLUMN_ADDRESS_CITY);
        int indexPostCodeName = res.getColumnIndex(ADDRESS_COLUMN_ADDRESS_POST_CODE);
        int indexType = res.getColumnIndex(ADDRESS_TYPE);

        while (!res.isAfterLast()) {
            Address address = new Address(res.getLong(indexID),
                    res.getString(indexName),
                    res.getString(indexStreetName),
                    res.getString(indexNumberName),
                    res.getString(indexCityName),
                    res.getString(indexPostCodeName),
                    res.getInt(indexType)
                    );

            addresses.add(address);
            res.moveToNext();
        }

        if (!res.isClosed()) {
            res.close();
        }

        if (db.isOpen()) {
            db.close();
        }
        return addresses;
    }
}
